package be.glever.xmlers.domain.util

import be.glever.xmlers.AbstractPkiEnabledTest
import be.glever.xmlers.domain.HashTreeTestUtil
import be.glever.xmlers.domain.model.DigestMethod
import org.bouncycastle.cms.CMSSignedData
import org.bouncycastle.tsp.TimeStampToken
import org.junit.Assert.assertArrayEquals
import org.junit.Test
import java.nio.charset.Charset
import java.security.MessageDigest

@Suppress("UNCHECKED_CAST")
class ArchiveTimestampBuilderTest : AbstractPkiEnabledTest() {

    @Test
    fun generateArchiveTimestamp() {
        val digestMethod = DigestMethod.SHA_256
        val messageDigest = MessageDigest.getInstance(digestMethod.javaName)

        val digests = arrayOf("1").map {
            messageDigest.digest(it.toByteArray(Charset.defaultCharset()))
        }.toMutableList()
        val hashTree = HashTreeTestUtil.generateHashTree(digestMethod = digestMethod, byteArrays = digests)
        val archiveTimestamp = ArchiveTimestampBuilder(
                timestamper = timestamper,
                digestMethod = digestMethod
        ).generateArchiveTimestamp(hashTree)

        val timeStampToken = TimeStampToken(CMSSignedData(archiveTimestamp.timestamp.timestamp))
        val messageImprintDigest = timeStampToken.timeStampInfo.messageImprintDigest
        assertArrayEquals(hashTree.getRootHash(), messageImprintDigest)
    }
}
