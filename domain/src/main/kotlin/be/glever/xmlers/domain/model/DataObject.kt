/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.domain.model

class DataObject(val id: Long? = null, val digests: Map<DigestMethod, ByteArray>, val referencedObject: Any? = null) {
	constructor(digestMethod: DigestMethod, hash: ByteArray)
			: this(null, mapOf(Pair(digestMethod, hash)))

	fun getHash(digestMethod: DigestMethod): ByteArray {
		return digests[digestMethod] ?: throw IllegalArgumentException("No hash found for $digestMethod")
	}
}
