/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.web.application

import be.glever.xmlers.BusinessConfig
import be.glever.xmlers.web.endpoint.JerseySetup
import org.glassfish.jersey.servlet.ServletContainer
import org.glassfish.jersey.servlet.ServletProperties
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import


@SpringBootApplication
@ComponentScan(basePackageClasses = arrayOf(JerseySetup::class))
@Import(BusinessConfig::class, JerseySetup::class)
class XmlErsApplication {
    companion object {
        private val log = LoggerFactory.getLogger(XmlErsApplication::class.java)
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(XmlErsApplication::class.java)
        }
    }


    @Bean
    fun jerseyServlet(): ServletRegistrationBean<ServletContainer> {
        val registration = ServletRegistrationBean(ServletContainer(), "/rest/*")
        registration.addInitParameter(ServletProperties.JAXRS_APPLICATION_CLASS, JerseySetup::class.java.name)
        return registration
    }

    @Bean
    fun run() = CommandLineRunner {}

    @Autowired
    lateinit var jpaConfig: BusinessConfig

}
