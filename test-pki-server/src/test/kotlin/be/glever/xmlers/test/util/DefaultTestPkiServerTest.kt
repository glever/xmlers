package be.glever.xmlers.test.util

import eu.europa.esig.dss.DigestAlgorithm
import eu.europa.esig.dss.client.SecureRandomNonceSource
import eu.europa.esig.dss.client.http.commons.TimestampDataLoader
import eu.europa.esig.dss.client.tsp.OnlineTSPSource
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cms.DefaultCMSSignatureAlgorithmNameGenerator
import org.bouncycastle.cms.SignerInformationVerifier
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider
import org.bouncycastle.operator.bc.BcRSAContentVerifierProviderBuilder
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.security.MessageDigest
import java.security.cert.*
import java.util.*


/**
 * Even testutils need tests...
 */
class DefaultTestPkiServerTest {
    lateinit var server: DefaultTestPkiServer
    @Before
    fun before() {
        server = DefaultTestPkiServer()
    }

    @After
    fun after() {
        server.stop()
    }


    @Test
    fun testCertChain() {
        verifyCert(certs = server.getChain(server.tsaCert()), trustAnchor = server.tsaRootCert())
    }

    @Test
    fun testTsa() {
        val testPKIServer = TestPKIServer()
        val tsaUrl = testPKIServer.tsaUrl()


        val tspSource = OnlineTSPSource(tsaUrl).apply {
            setNonceSource(SecureRandomNonceSource())
            setDataLoader(TimestampDataLoader().apply {
                timeoutConnection = 1500000
                timeoutSocket = 1500000
            })
        }

        val digest = MessageDigest.getInstance(DigestAlgorithm.SHA256.javaName).digest("I am so smrt".toByteArray())
        val timeStampResponse = tspSource.getTimeStampResponse(DigestAlgorithm.SHA256, digest)

        timeStampResponse.validate(
                SignerInformationVerifier(
                        DefaultCMSSignatureAlgorithmNameGenerator(),
                        DefaultSignatureAlgorithmIdentifierFinder(),
                        BcRSAContentVerifierProviderBuilder(DefaultDigestAlgorithmIdentifierFinder()).build(X509CertificateHolder(testPKIServer.tsaCert.encoded)),
                        BcDigestCalculatorProvider()
                )
        )
        val message = Base64.getEncoder().encodeToString(timeStampResponse.encoded)
        println(message)

    }


    fun verifyCert(certs: List<X509Certificate>, trustAnchor: X509Certificate) {
        val cf = CertificateFactory.getInstance("X.509")
        val mylist = certs.toList()

        val cp = cf.generateCertPath(mylist)

        val anchor = TrustAnchor(trustAnchor, null)
        val params = PKIXParameters(Collections.singleton(anchor))
        params.isRevocationEnabled = false // todo add crl / ocsp to testpkiserver
        val cpv = CertPathValidator.getInstance("PKIX")
        val result = cpv.validate(cp, params) as PKIXCertPathValidatorResult
        println(result)
    }

}
