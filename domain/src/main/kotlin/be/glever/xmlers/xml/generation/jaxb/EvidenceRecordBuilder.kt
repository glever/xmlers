/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.xml.generation.jaxb

import be.glever.xmlers.domain.model.ArchiveTimestamp
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.model.ReducedHashTree
import be.glever.xmlers.xml.mapper.HashTreeXmlMapper
import ietf.params.xml.ns.ers.*
import java.math.BigDecimal
import java.util.*
import javax.xml.crypto.dsig.CanonicalizationMethod

class EvidenceRecordBuilder {

    private val evidenceRecordType: EvidenceRecordType

    init {
        evidenceRecordType = ObjectFactory().createEvidenceRecordType()
        evidenceRecordType.version = BigDecimal.valueOf(1.0)
    }

    fun withArchiveTimestampSequence(seq: ArchiveTimeStampSequenceType): EvidenceRecordBuilder {
        evidenceRecordType.archiveTimeStampSequence = seq
        return this
    }

    fun withEncryptionInformation(encryptionInfo: EncryptionInfo): EvidenceRecordBuilder {
        evidenceRecordType.encryptionInformation = encryptionInfo
        return this
    }

    fun withSupportingInformationList(supportingInformation: SupportingInformationType): EvidenceRecordBuilder {
        evidenceRecordType.supportingInformationList = supportingInformation
        return this
    }

    fun build(): EvidenceRecordType {
        return this.evidenceRecordType
    }

    /**
     * Used for initial ReducedHashTree generation.
     */

    fun fromInitialTimestamp(reducedHashTree: ReducedHashTree? = null, archiveTimestamp: ArchiveTimestamp, digestMethodType: DigestMethod): EvidenceRecordType =
            EvidenceRecordType().apply {
                version = BigDecimal.ONE
                archiveTimeStampSequence = ArchiveTimeStampSequenceType().apply {
                    archiveTimeStampChain.add(ArchiveTimeStampSequenceType.ArchiveTimeStampChain().apply {
                        order = 0
                        canonicalizationMethod = CanonicalizationMethodType().apply {
                            algorithm = CanonicalizationMethod.INCLUSIVE
//                        content.add(null)
                        }
                        digestMethod = DigestMethodType().apply {
                            algorithm = digestMethodType.uri
                        }
                        archiveTimeStamp.add(ArchiveTimeStampType().apply {
                            order = 0
                            attributes = Attributes().apply {
                                attribute.add(Attributes.Attribute().apply {
                                    order = 0
//                                content.add(null)
                                    type = null

                                })
                            }
                            hashTree = reducedHashTree?.let { HashTreeXmlMapper.from(reducedHashTree) }
                            timeStamp = TimeStampType().apply {
                                timeStampToken = TimeStampType.TimeStampToken().apply {
                                    type = "RFC3161"
                                    content.add(Base64.getEncoder().encodeToString(archiveTimestamp.timestamp.timestamp))
                                }
                                cryptographicInformationList = CryptographicInformationType().apply {
                                    cryptographicInformation.add(CryptographicInformationType.CryptographicInformation().apply {
                                        //                                    content.add(null)
                                        order = 0
                                        type = null
                                    })
                                }
                            }
                        })

                    })

                }
                encryptionInformation = null
                supportingInformationList = null
            }


}
