#!/usr/bin/env bash
docker stop xmlers-postgres
docker rm xmlers-postgres
docker run --name xmlers-postgres -p 5432:5432 -e POSTGRES_PASSWORD=xmlers -e POSTGRES_USER=xmlers -d postgres