/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.entity

import java.util.*
import javax.persistence.*

@Table(name="ATTRIBUTE")
@Entity
data class AttributeEntity(
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	val id: Long? = null,

	@Column(name = "TYPE")
	val type: String,

	@Column(name = "VALUE")
	val value: ByteArray,

	@Column(name = "SORT_ORDER")
	val order: Int) : XmlErsEntity {

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as AttributeEntity

		if (id != other.id) return false
		if (type != other.type) return false
		if (!Arrays.equals(value, other.value)) return false
		if (order != other.order) return false

		return true
	}

	override fun hashCode(): Int {
		var result = id?.hashCode() ?: 0
		result = 31 * result + type.hashCode()
		result = 31 * result + Arrays.hashCode(value)
		result = 31 * result + order
		return result
	}


}
