package be.glever.xmlers.xml.validation

import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.util.ByteArrayComparator
import be.glever.xmlers.domain.util.MessageDigestUtil
import ietf.params.xml.ns.ers.ArchiveTimeStampSequenceType
import ietf.params.xml.ns.ers.ArchiveTimeStampType
import ietf.params.xml.ns.ers.EvidenceRecordType
import ietf.params.xml.ns.ers.HashTreeType
import org.bouncycastle.cms.CMSSignedData
import org.bouncycastle.tsp.TimeStampToken
import java.util.*

class XMLERSValidator(val evidenceRecordType: EvidenceRecordType, val digestToVerify: ByteArray) {
    // TODO these members make no sense when working with multiple sequences / chains. refactor when needed
    private val firstChain: ArchiveTimeStampSequenceType.ArchiveTimeStampChain
    private val firstArchiveTimeStamp: ArchiveTimeStampType
    private val firstTimestampToken: TimeStampToken

    init {
        this.firstChain = this.evidenceRecordType
                .archiveTimeStampSequence.archiveTimeStampChain
                .reduce { firstChain, archiveTimeStampChain -> if (firstChain.order < archiveTimeStampChain.order) firstChain else archiveTimeStampChain }
        this.firstArchiveTimeStamp = firstChain
                .archiveTimeStamp.reduce { firstArchiveTimestamp, archiveTimeStamp -> if (firstArchiveTimestamp.order < archiveTimeStamp.order) firstArchiveTimestamp else archiveTimeStamp }
        this.firstTimestampToken = firstArchiveTimeStamp
                .timeStamp.timeStampToken.content[0]
                .let { contentB64 -> Base64.getDecoder().decode(contentB64 as String) }
                .let { tokenDER -> TimeStampToken(CMSSignedData(tokenDER)) }
    }

    fun validate(): XMLERSValidationResult {
        val result = chainsToRootHash(XMLERSValidationResult())

        return result;
    }

    private fun chainsToRootHash(xmlersValidationResult: XMLERSValidationResult): XMLERSValidationResult {
        val timestampHash = firstTimestampToken.timeStampInfo.messageImprintDigest
        val digestJavaName = this.firstChain.digestMethod.algorithm.let { uri ->
            DigestMethod.fromUri(uri)?.javaName ?: throw IllegalArgumentException("Unknown digest identifier: $uri")
        }
        val rootHashCalculated = if (firstArchiveTimeStamp.hashTree != null) calculateRootHash(firstArchiveTimeStamp.hashTree, digestJavaName) else digestToVerify

        return xmlersValidationResult.copy(chainsToRootHash = Arrays.equals(timestampHash, rootHashCalculated))
    }

    private fun calculateRootHash(hashTreeType: HashTreeType, digestAlgo: String): ByteArray =
            hashTreeType.sequence
                    .sortedBy { it.order }
                    .fold(this.digestToVerify) { previousHash, sequence ->
                        listOf<ByteArray>().plus(sequence.digestValue).plus(previousHash)
                                .sortedWith(ByteArrayComparator())
                                .let { sortedDigests -> MessageDigestUtil.generateDigest(digestAlgo, sortedDigests) }
                    }


}
