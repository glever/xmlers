package be.glever.xmlers

import be.glever.xmlers.pkix.TimeStamper
import be.glever.xmlers.test.util.DefaultTestPkiServer
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass

abstract class AbstractPkiEnabledTest {
    lateinit var timestamper: TimeStamper

    companion object{
        lateinit var server: DefaultTestPkiServer

        @BeforeClass
        @JvmStatic
        fun beforeClass() {
            server = DefaultTestPkiServer()
        }

        @AfterClass
        @JvmStatic
        fun afterClass() {
            server.stop()
        }
    }

    @Before
    fun setup(){
        server.server.reset()
        timestamper = TimeStamper(server.tsaUrl(), server.tsaCert(), server.getChain(server.tsaCert()), server.tsaRootCert())
    }



}
