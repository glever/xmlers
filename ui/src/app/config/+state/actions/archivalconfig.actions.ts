import { Action } from '@ngrx/store';
import { ArchivalConfig } from '@xmlers/generated/xmlers-api';

export enum ArchivalConfigActionTypes {
  Search = 'archivalconfig/search',
  SearchSuccess = 'archivalconfig/searchSuccess',
  SearchFailure = 'archivalconfig/searchFailure',
  Select = 'archivalconfig/select',
}

export class SearchArchivalConfigAction implements Action {
  readonly type = ArchivalConfigActionTypes.Search;
}

export class SearchArchivalConfigSuccessAction implements Action {
  readonly type = ArchivalConfigActionTypes.SearchSuccess;

  constructor(public archivalConfigs: ArchivalConfig[]) {
  }
}

export class SearchArchivalConfigActionFailure implements Action {
  readonly type = ArchivalConfigActionTypes.SearchFailure;

  constructor(public payload: any) {
  };
}

export class SelectArchivalConfigAction implements Action {
  readonly type = ArchivalConfigActionTypes.Select;

  constructor(public id: string) {
  };
}

export type ArchivalConfigAction =
  SearchArchivalConfigAction
  | SearchArchivalConfigSuccessAction
  | SearchArchivalConfigActionFailure;
