package be.glever.xmlers.rest.client

import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.Response

class EvidenceRecordRestClient(val target: WebTarget) {

	fun getEvidenceRecord(archivalConfiguUniqueId: String, archiveObjectUniqueId: String): String =
			target.path(archivalConfiguUniqueId)
					.path(archiveObjectUniqueId)
					.request()
					.get()
					.readResponse(Response.Status.OK, GenericType(String::class.java))
}