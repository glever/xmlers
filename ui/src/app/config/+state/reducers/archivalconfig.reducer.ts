import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { ArchivalConfig } from '@xmlers/generated/xmlers-api';
import { ArchivalConfigAction, ArchivalConfigActionTypes } from '@xmlers/config/+state/actions/archivalconfig.actions';

export interface State extends EntityState<ArchivalConfig> {
  selectedArchivalConfigId: string | null;
  error?: any
}

export const adapter: EntityAdapter<ArchivalConfig> = createEntityAdapter({
  selectId: (archivalConfig: ArchivalConfig) => archivalConfig.uniqueId,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedArchivalConfigId: null
});

export function reducer(state = initialState, action: ArchivalConfigAction): State {
  switch (action.type) {
    case ArchivalConfigActionTypes.SearchSuccess:
      return adapter.addMany(action.archivalConfigs, state);
    case ArchivalConfigActionTypes.SearchFailure:
      return {
        ...state,
        error: action.payload
      };
    case ArchivalConfigActionTypes.Search:
      return state;

    default:
      return state;
  }
}

export const getSelectedId = (state: State) => state.selectedArchivalConfigId;
