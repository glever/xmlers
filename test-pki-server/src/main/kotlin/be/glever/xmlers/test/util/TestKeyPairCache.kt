package be.glever.xmlers.test.util

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.SecureRandom


class TestKeyPairCache {
    private val sr: SecureRandom = SecureRandom()

    private val usedKeyPairs: MutableSet<CachedKeyPair> = mutableSetOf()
    private val availableKeyPairs: MutableSet<CachedKeyPair> = mutableSetOf()

    fun getKeyPair(algorithm: String, keySize: Int): KeyPair {
        val cachedKeyPair: CachedKeyPair = availableKeyPairs.filter { it.algorithm.equals(algorithm) && it.keySize.equals(keySize) }
                .firstOrNull()
                ?: CachedKeyPair(algorithm, keySize, generateKeyPair(algorithm, keySize))
        usedKeyPairs.add(cachedKeyPair)
        availableKeyPairs.remove(cachedKeyPair)
        return cachedKeyPair.keyPair
    }

    private fun generateKeyPair(algorithm: String, keySize: Int): KeyPair =
            KeyPairGenerator.getInstance(algorithm)
                    .apply { initialize(keySize, sr) }
                    .generateKeyPair()

    fun release(keyPair: KeyPair) =
            usedKeyPairs.first { it.keyPair == keyPair }
                    .also { usedKeyPairs.remove(it) }
                    .also { availableKeyPairs.add(it) }
                    .let { null }

    fun freeAll() {
        availableKeyPairs.addAll(usedKeyPairs).also { usedKeyPairs.clear() }
    }

    inner class CachedKeyPair(val algorithm: String, val keySize: Int, val keyPair: KeyPair) {}

}
