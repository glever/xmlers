package be.glever.xmlers.domain.model

/**
 *3.  Archive Time-Stamp

An Archive Time-Stamp is a Time-Stamp with additional artifacts that
allow the verification of the existence of several data objects at a
certain time.

The process of construction of an ATS must support evidence on a
long-term basis and prove that the archive object existed and was
identical, at the time of the Time-Stamp, to the currently present
archive object (at the time of verification).  To achieve this, an
ATS MUST be renewed before it becomes invalid (which may happen for
several reasons such as, e.g., weakening used cryptographic
algorithms, invalidation of digital certificate, or a TSA terminating
its business or ceasing its service).

3.1.  Structure

An Archive Time-Stamp contains a Time-Stamp Token, with useful data
for its validation (cryptographic information), such as the
certificate chain or Certificate Revocation Lists, an optional
ordered set of ordered lists of hash values (a hash tree) that were
protected with the Time-Stamp Token and optional information
describing the renewal steps (<Attributes> element).  A hash tree may
be used to store data needed to bind the Time-Stamped value with
protected objects by the Archive Time-Stamp.  If a hash tree is not
present, the ATS simply refers to a single object, either input data
object or a previous TS.
 */
class ArchiveTimestamp(val hashTree: HashTree?, val timestamp: Timestamp, val digestMethod: DigestMethod) {

}
