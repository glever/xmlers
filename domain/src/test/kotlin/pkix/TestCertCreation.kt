/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix

import java.math.BigInteger
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.SecureRandom
import java.security.Security
import java.util.Base64
import java.util.Date
import java.util.Random

import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.ContentSigner
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder

object TestCertCreation {
    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        Security.addProvider(BouncyCastleProvider())

        val keyPairGenerator = KeyPairGenerator.getInstance("RSA", "BC")
        keyPairGenerator.initialize(1024, SecureRandom())
        val keyPair = keyPairGenerator.generateKeyPair()

        val startDate = Date() // time from which certificate is valid
        val expiryDate = Date(startDate.time +  60 * 60 * 24 * 365)
        val serialNumber = BigInteger.valueOf(Math.abs(Random().nextLong()))
        val subjectName = X500Name("CN=Test V3 Certificate")
        val sPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.public
                .encoded)

        val sigGen = JcaContentSignerBuilder("SHA1withRSA").build(keyPair.private)

        val generatedCert = X509v3CertificateBuilder(subjectName, serialNumber, startDate,
                expiryDate, subjectName, sPubKeyInfo)
                .build(sigGen)
        println(Base64.getEncoder()
                .encodeToString(generatedCert.encoded))

    }
}
