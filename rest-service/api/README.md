Because the [jaxrs-to-gradle plugin](https://github.com/mulesoft-labs/raml-for-jax-rs/blob/master/raml-to-jaxrs/raml-to-jaxrs-gradle-plugin/README.md) 
is currently not available from the maven repos (see [this issue](https://github.com/mulesoft-labs/raml-for-jax-rs/issues/287)),
The generated sources are placed under version control and this project is not part of the parent project.
If you want to build this yourself, clone the  [raml-for-jax-rs project](https://github.com/mulesoft-labs/raml-for-jax-rs) and install in your local repo using
mvn clean install -DskipTests.