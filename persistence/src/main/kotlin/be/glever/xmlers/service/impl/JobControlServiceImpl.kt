/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.service.impl

import be.glever.xmlers.business.jpa.dao.ArchiveObjectDao
import be.glever.xmlers.business.jpa.dao.HashTreeDao
import be.glever.xmlers.business.jpa.dao.TimeStampDao
import be.glever.xmlers.business.jpa.entity.ArchivalConfigEntity
import be.glever.xmlers.business.jpa.entity.HashTreeEntity
import be.glever.xmlers.business.jpa.entity.TimeStampEntity
import be.glever.xmlers.business.jpa.entity.TimeStampTokenEmbeddableEntity
import be.glever.xmlers.domain.model.ArchiveObject
import be.glever.xmlers.domain.model.DataObject
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.model.HashTree
import be.glever.xmlers.pkix.TimeStamper
import be.glever.xmlers.service.ArchivalConfigService
import be.glever.xmlers.service.JobControlService
import org.springframework.stereotype.Component
import java.security.cert.X509Certificate
import java.time.LocalDateTime
import javax.transaction.Transactional

@Transactional
@Component
class JobControlServiceImpl(
        val archivalConfigService: ArchivalConfigService,
        val archiveObjectDao: ArchiveObjectDao,
        val hashTreeDao: HashTreeDao,
        val timestampDao: TimeStampDao
) : JobControlService {

    companion object {
        val DEFAULT_ARITY: Short = 2
    }

    override fun runJob(archivalConfigId: String) {
        val archivalConfig = archivalConfigService.findByUniqueId(archivalConfigId)

        // preserve order
        createHashTreeForNewArchiveObjects(archivalConfig)
//		renewHashTreesIfNeeded(archivalConfig)
//		reTimestampHashTreesIfNeeded(archivalConfig)
    }

    /*
     * Picks up any new (not yet covered by ER) AOs and covers them by a HashTree.
     */
    private fun createHashTreeForNewArchiveObjects(archivalConfig: ArchivalConfigEntity) {
        val digestMethodEntity = archivalConfig.digestMethodToUse(LocalDateTime.now())
        val digestMethod = digestMethodEntity.let { DigestMethod(javaName = it.javaName, oid = it.oid, uri = it.uri) }
        val aoEntities = archiveObjectDao.findNew(archivalConfig.id!!, digestMethod.oid)

        val hashTree = HashTree(arity = DEFAULT_ARITY,
                digestMethod = digestMethod,
                archiveObjects = aoEntities.map { aoEntity ->
                    ArchiveObject(digestMethod, aoEntity.dataObjects.map { doEntity ->
                        DataObject(digestMethod, doEntity.getDigest(digestMethod.oid)!!)
                    }.toMutableList())
                }
        )

        // TODO need to provide trustanchor for tsa.
        var dummyRoot: X509Certificate? = null
        val timeStampToken: ByteArray = TimeStamper(archivalConfig.tsaConfig.tsaUrl, archivalConfig.tsaConfig.tsaCert,listOf(), dummyRoot!!).timestamp(digestMethod, hashTree.getRootHash()).encoded

        val hashTreeEntity = hashTreeDao.save(HashTreeEntity(digestMethod = digestMethodEntity, archiveObjects = aoEntities, arity = hashTree.arity, rootHash = hashTree.getRootHash()))


        val timestampEntity = timestampDao.save(
                TimeStampEntity(
                        HashTree = hashTreeEntity,
                        attributes = listOf(),
                        cryptographicInformations = listOf(),
                        timeStampToken = TimeStampTokenEmbeddableEntity(value = timeStampToken, type = TimeStampTokenEmbeddableEntity.Type.RFC3161)
                )
        )


    }

    /*
     * Searches for ArchiveObjects covered by an ER based on outdated digestmethod and covers them by a new evidencerecord.
     * TODO Merge trees? (seems easy enough), respect maximum tree size?
     */
    private fun renewHashTreesIfNeeded(archivalConfig: ArchivalConfigEntity) {
        TODO("not implemented")
    }

    /*
     * Retimestamp all impacted hashtrees whenever condition x has been met (change TSA, new TSA cert with validity date further in future).
     * TODO Merge trees? (effectively renew hash trees with same digestmethod) if certain conditions are met (there are "too much" smaller trees, group all by year / month / day, or just "always" if retimestamping periods are long apart)
     */
    private fun reTimestampHashTreesIfNeeded(archivalConfig: ArchivalConfigEntity) {
        TODO("not implemented")
    }

}
