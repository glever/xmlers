/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.domain

import be.glever.xmlers.domain.model.ArchiveObject
import be.glever.xmlers.domain.model.DataObject
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.util.ByteArrayComparator
import be.glever.xmlers.domain.util.MessageDigestUtil
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import org.junit.rules.RuleChain
import java.lang.IllegalArgumentException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class ArchiveObjectTest {
	private var expection: ExpectedException = ExpectedException.none()
	@Rule
	fun rules() = RuleChain.outerRule(expection)

	@Test
	fun emptyDataObjectsShouldThrowIllegalArgumentException() {
		expection.expect(IllegalArgumentException::class.java)
        ArchiveObject(digestMethod = DigestMethod.SHA_256)
	}

	@Test
	@Throws(NoSuchAlgorithmException::class)
	fun digestShouldBeDigestOfSortedChildren() {
		val hashAlgo = DigestMethod.SHA_512
		val s1 = "one"
		val s2 = "two"
		val s3 = "three"
		val s1Hash = getHash(s1, hashAlgo) // [5, -9, 3, 65, 7, -118, ...
		val s2Hash = getHash(s2, hashAlgo) // [-110, -115, 80, -47,...
		val s3Hash = getHash(s3, hashAlgo) // [98, 117, -114, 74, 87,...

		val hashArr = listOf(s1Hash, s2Hash, s3Hash).sortedWith(ByteArrayComparator())
		val expectedHash = MessageDigestUtil.generateDigest(hashAlgo.javaName, hashArr)

		val ao = ArchiveObject(digestMethod = hashAlgo, dataObjects = mutableListOf(DataObject(hashAlgo, s1Hash), DataObject(hashAlgo, s2Hash),
                DataObject(hashAlgo, s3Hash)))

		Assert.assertArrayEquals(expectedHash, ao.getDigest())

	}

	@Throws(NoSuchAlgorithmException::class)
	private fun getHash(s1: String, hashAlgo: DigestMethod): ByteArray {
		return MessageDigest.getInstance(hashAlgo.javaName).digest(s1.toByteArray())
	}

}
