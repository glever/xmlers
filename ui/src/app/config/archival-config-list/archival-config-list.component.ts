import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ArchivalConfig } from '@xmlers/generated/xmlers-api';

@Component({
  selector: 'app-archival-config-list',
  template: `
    <table mat-table [dataSource]="archivalConfigs">
      <ng-container matColumnDef="id">
        <th mat-header-cell *matHeaderCellDef>Id</th>
        <td mat-cell *matCellDef="let archivalConfig">{{archivalConfig.uniqueId}}</td>
      </ng-container>
      <ng-container matColumnDef="policies">
        <th mat-header-cell *matHeaderCellDef>Policy</th>
        <td mat-cell *matCellDef="let archivalConfig">{{archivalConfig.digestMethodPolicies}}</td>
      </ng-container>
      <ng-container matColumnDef="c14nMethod">
        <th mat-header-cell *matHeaderCellDef>C14nMethod</th>
        <td mat-cell *matCellDef="let archivalConfig">{{archivalConfig.c14nMethod}}</td>
      </ng-container>
      <ng-container matColumnDef="tsaUrl">
        <th mat-header-cell *matHeaderCellDef>tsa url</th>
        <td mat-cell *matCellDef="let archivalConfig">{{archivalConfig.tsaUrl}}</td>
      </ng-container>
      <tr mat-header-row *matHeaderRowDef="columnsToDisplay"></tr>
      <tr mat-row *matRowDef="let myRowData; columns: columnsToDisplay"></tr>
    </table>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArchivalConfigListComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  @Input()
  archivalConfigs: ArchivalConfig[];

  columnsToDisplay = ['id', 'policies', 'c14nMethod', 'tsaUrl'];
}
