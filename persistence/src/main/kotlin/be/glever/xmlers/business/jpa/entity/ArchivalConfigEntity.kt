/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(
        name = "ARCHIVAL_CONFIG",
        uniqueConstraints = [UniqueConstraint(columnNames = ["UNIQUE_ID"])]
)
class ArchivalConfigEntity(
        id: Long?,
        @Column(name = "UNIQUE_ID", nullable = false)
        val uniqueId: String,

        @ManyToOne
        @JoinColumn(name = "C14N_METHOD", nullable = false)
        val c14nMethod: C14nMethodEntity,

        @ManyToOne(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinColumn(name = "TSA_CONFIG", nullable = false)
        val tsaConfig: TsaConfigEntity,

        @OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinColumn(name = "ARCHIVAL_CONFIG_ID", nullable = false)
        val digestMethodPolicyEntities: Set<DigestMethodPolicyEntity>
) : AbstractXmlErsEntity(id = id) {

    fun copy(id: Long? = this.id, uniqueId: String = this.uniqueId, c14nMethod: C14nMethodEntity = this.c14nMethod, tsaConfig: TsaConfigEntity = this.tsaConfig, digestMethodPolicyEntities: Set<DigestMethodPolicyEntity> = this.digestMethodPolicyEntities): ArchivalConfigEntity =
            ArchivalConfigEntity(id = id, uniqueId = uniqueId, c14nMethod = c14nMethod, tsaConfig = tsaConfig, digestMethodPolicyEntities = digestMethodPolicyEntities)

    fun digestMethodToUse(date: LocalDateTime): DigestMethodEntity =
            digestMethodPolicyEntities
                    .filter { it.period.validFrom.toLocalDateTime().isBefore(date) && it.period.validUntil.toLocalDateTime().isAfter(date) }
                    .sortedBy { it.period.validUntil }
                    .reversed()
                    .first().digestMethod

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArchivalConfigEntity

        if (uniqueId != other.uniqueId) return false

        return true
    }

    override fun hashCode(): Int {
        return uniqueId.hashCode()
    }


}
