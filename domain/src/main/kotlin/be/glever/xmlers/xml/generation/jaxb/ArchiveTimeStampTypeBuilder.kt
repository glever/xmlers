/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.*

class ArchiveTimeStampTypeBuilder {

	private val archiveTimeStampType: ArchiveTimeStampType

	init {
		this.archiveTimeStampType = ObjectFactory().createArchiveTimeStampType()
	}

	fun withAttributes(attributes: Attributes): ArchiveTimeStampTypeBuilder {
		this.archiveTimeStampType.attributes = attributes
		return this
	}

	fun withHashTree(hashTreeType: HashTreeType): ArchiveTimeStampTypeBuilder {
		this.archiveTimeStampType.hashTree = hashTreeType
		return this
	}

	fun withOrder(order: Int): ArchiveTimeStampTypeBuilder {
		this.archiveTimeStampType.order = order
		return this
	}

	fun withTimeStamp(timestamptype: TimeStampType): ArchiveTimeStampTypeBuilder {
		this.archiveTimeStampType.timeStamp = timestamptype
		return this
	}

	fun build(): ArchiveTimeStampType {
		return this.archiveTimeStampType
	}

}
