import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivalConfigListComponent } from "@xmlers/config/archival-config-list/archival-config-list.component";

describe('ArchivalConfigListComponent', () => {
  let component: ArchivalConfigListComponent;
  let fixture: ComponentFixture<ArchivalConfigListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArchivalConfigListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivalConfigListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
