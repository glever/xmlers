/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.dao

import be.glever.xmlers.business.jpa.entity.ArchiveObjectEntity
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface ArchiveObjectDao : CrudRepository<ArchiveObjectEntity, Long> {

	@EntityGraph(attributePaths = ["dataObjects.digests" ])
	fun findByArchivalConfigIdAndId(archivalConfigId: Long, archiveObjectId: Long): ArchiveObjectEntity

	@Query("""
		from ArchiveObjectEntity as ao
		where ao.uniqueId = :uniqueId
		and ao.archivalConfig.uniqueId = :archivalConfigUniqueId
		""")
	@EntityGraph(attributePaths = ["dataObjects.digests" ])
	fun findByArchivalConfigUniqueIdAndUniqueId(archivalConfigUniqueId: String, uniqueId: String): ArchiveObjectEntity

	fun findByArchivalConfigId(archivalConfigId: Long): List<ArchiveObjectEntity>

	fun findByArchivalConfigIdAndIdIn(archivalConfigId: Long, archiveObjectIds: List<Long>): List<ArchiveObjectEntity>

	@Query("""
		from ArchiveObjectEntity as ao
		 where ao.archivalConfig.id = :archivalConfigId
		 and hashTree is null
		 and not exists (
			from DataObjectEntity as do where not exists (
				from DataObjectDigestEntity where dataObject = do and digestMethod.oid = :digestMethodOid
			)
		)
		""")
	fun findNew(archivalConfigId: Long, digestMethodOid: String): List<ArchiveObjectEntity>
}