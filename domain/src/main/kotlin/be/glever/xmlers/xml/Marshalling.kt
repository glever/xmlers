package be.glever.xmlers.xml

import be.glever.xmlers.xml.ContextHolder.jaxbContext
import ietf.params.xml.ns.ers.EvidenceRecordType
import ietf.params.xml.ns.ers.ObjectFactory
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import java.nio.charset.Charset
import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller

object ContextHolder {
    val jaxbContext = JAXBContext.newInstance(EvidenceRecordType::class.java)
}

fun marshall(evidenceRecordType: EvidenceRecordType, outputStream: OutputStream) {
    val evidenceRecord = ObjectFactory().createEvidenceRecord(evidenceRecordType)
    val marshaller = jaxbContext.createMarshaller()
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, java.lang.Boolean.FALSE)
    marshaller.marshal(evidenceRecord, outputStream)
}

fun marshallToString(evidenceRecordType: EvidenceRecordType): String =
        ByteArrayOutputStream()
                .apply { marshall(evidenceRecordType, this) }
                .let { String(it.toByteArray(), Charset.defaultCharset()) }
