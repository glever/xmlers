package be.glever.xmlers.test.util

import org.bouncycastle.asn1.ASN1EncodableVector
import org.bouncycastle.asn1.DERIA5String
import org.bouncycastle.asn1.DERSequence
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.*
import org.bouncycastle.asn1.x509.GeneralName.uniformResourceIdentifier
import org.bouncycastle.cert.X509v1CertificateBuilder
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.crypto.util.PrivateKeyFactory
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder
import java.math.BigInteger
import java.security.KeyPair
import java.security.KeyStore
import java.security.PrivateKey
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.*


fun generateV1Certificate(certificateKeys: KeyPair, validUntil: Date, dn: String, algorithm: String): X509Certificate {

    val subPubKeyInfo = SubjectPublicKeyInfo.getInstance(certificateKeys.public.encoded)
    val name = X500Name(dn)
    val from = Date()
    val serialNumber = generateSerialNumber()


    val privateKeyAsymKeyParam = PrivateKeyFactory.createKey(certificateKeys.private.encoded)
    val sigAlgId = DefaultSignatureAlgorithmIdentifierFinder().find(algorithm)
    val digAlgId = DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId)
    val signer = BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(privateKeyAsymKeyParam)


    val certificateBuilder = X509v1CertificateBuilder(name, serialNumber, from, validUntil, name, subPubKeyInfo)
    val certificateHolder = certificateBuilder.build(signer)

    return JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder)

}

private fun generateSerialNumber() = BigInteger(64, SecureRandom())

// https://cipherious.wordpress.com/2013/05/20/constructing-an-x-509-certificate-using-bouncy-castle/
fun generateV3Certificate(issuer: KeyStore.PrivateKeyEntry, keyPair: KeyPair, algorithm: String, pkiServerUrl: String, certSpec: CertificateSpec, crlUrl: String, ocspUrl: String, caIssuerUrl: String): X509Certificate {
    val issuerCert = issuer.certificate as X509Certificate
    val issuerPrivKey = issuer.privateKey as PrivateKey

    // basic properties
    val issuerName = X500Name(issuerCert.subjectDN.name)
    val subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.public.encoded)
    val serialNumber = generateSerialNumber()
    val validFrom = Date()
    val subjectName = X500Name(certSpec.subjectDn)

    val certificateBuilder = X509v3CertificateBuilder(issuerName, serialNumber, validFrom, certSpec.validUntil, subjectName, subPubKeyInfo)

    // add crl
    val crlSequence = DERSequence(DistributionPoint(DistributionPointName(GeneralNames(GeneralName(uniformResourceIdentifier, DERIA5String("$crlUrl/${certSpec.issuerSerialNr}.crl")))), null, null))
    certificateBuilder.addExtension(Extension.cRLDistributionPoints, false, crlSequence)

    //AIA info such as issuer cert location and ocsp info
    val caIssuers = AccessDescription(AccessDescription.id_ad_caIssuers,
            GeneralName(uniformResourceIdentifier, DERIA5String(caIssuerUrl)))
    val ocsp = AccessDescription(AccessDescription.id_ad_ocsp,
            GeneralName(uniformResourceIdentifier, DERIA5String(ocspUrl)))
    val aiaAsn = ASN1EncodableVector()
    aiaAsn.add(caIssuers)
    aiaAsn.add(ocsp)
    certificateBuilder.addExtension(Extension.authorityInfoAccess, false, DERSequence(aiaAsn))

    // CA specific data
    when {
        certSpec.isCa -> certificateBuilder.addExtension(Extension.basicConstraints, false, BasicConstraints(true))
    }
    if(certSpec.keyPurposes.isNotEmpty()){
        certificateBuilder.addExtension(Extension.extendedKeyUsage, true, ExtendedKeyUsage(certSpec.keyPurposes.toTypedArray()))
    }

    // CA signing data
    val sigAlgId = DefaultSignatureAlgorithmIdentifierFinder().find(algorithm)
    val digAlgId = DefaultDigestAlgorithmIdentifierFinder().find(sigAlgId)
    val privateKeyAsymKeyParam = PrivateKeyFactory.createKey(issuerPrivKey.encoded)
    val signer = BcRSAContentSignerBuilder(sigAlgId, digAlgId).build(privateKeyAsymKeyParam)

    // assemble and sign
    val certificateHolder = certificateBuilder.build(signer)

    /*
     if (keyPurposes == null ||
                        !keyPurposes.contains(KP_OCSP_SIGNING_OID)) {
                        throw new CertPathValidatorException(
                            "Responder's certificate not valid for signing " +
                            "OCSP responses");
                    }
     */


    return JcaX509CertificateConverter().setProvider("BC").getCertificate(certificateHolder)
}

fun getPem(x509Certificate: X509Certificate): String =
        Base64.getEncoder().encodeToString(x509Certificate.encoded)
                .let { strEncoded ->
                    StringBuilder("-----BEGIN CERTIFICATE-----")
                            .also { sb ->
                                strEncoded.toCharArray().forEachIndexed { index, c ->
                                    if (index % 64 == 0) {
                                        sb.append("\n")
                                    }
                                    sb.append(c)
                                }
                            }
                            .append("\n-----END CERTIFICATE-----")
                            .toString()
                }
