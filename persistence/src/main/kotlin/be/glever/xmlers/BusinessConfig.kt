/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers

import be.glever.xmlers.business.jpa.entity.EvidenceRecordEntity
import be.glever.xmlers.service.ArchiveObjectService
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.service.HashTreeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories
@ComponentScan
@EntityScan(basePackageClasses = arrayOf(EvidenceRecordEntity::class))
class BusinessConfig {
	@Autowired lateinit var hashTreeService: HashTreeService
	@Autowired lateinit var archiveObjectService: ArchiveObjectService
	@Autowired lateinit var digestMethodService: DigestMethodService
}