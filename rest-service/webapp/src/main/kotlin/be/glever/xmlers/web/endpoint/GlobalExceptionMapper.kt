/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.web.endpoint

import be.glever.xmlers.service.exception.XmlErsBusinessException
import mu.KotlinLogging
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper

private val LOG = KotlinLogging.logger {}

class GlobalExceptionMapper : ExceptionMapper<Throwable> {

	override fun toResponse(throwable: Throwable): Response {
		LOG.warn(throwable, { "Mapping ${throwable.message}" })

		return when (throwable) {
			is WebApplicationException -> throwable.response
			is XmlErsBusinessException -> {
				LOG.info(throwable) { "Business Exception occurred" }
				Response.status(Response.Status.BAD_REQUEST).entity(throwable.message ?: "Bad Request.").build()
			}
			else -> {
				LOG.error(throwable) { "Mapping Exception ${throwable.message} to Response" }
				Response.serverError().entity(throwable.message ?: "Internal server error").build()
			}
		}

	}
}