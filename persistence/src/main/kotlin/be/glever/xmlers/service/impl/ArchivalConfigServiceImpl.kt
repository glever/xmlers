/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.service.impl

import be.glever.xmlers.business.jpa.dao.ArchivalConfigDao
import be.glever.xmlers.business.jpa.entity.ArchivalConfigEntity
import be.glever.xmlers.service.ArchivalConfigService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Transactional
@Repository
class ArchivalConfigServiceImpl(private val archivalConfigDao: ArchivalConfigDao) : ArchivalConfigService {
	override fun findAll(): List<ArchivalConfigEntity> {
		return archivalConfigDao.findAll().toList().also {
			it.count()
			it.forEach {
				it.digestMethodPolicyEntities.count()
			}
		}
	}

	override fun findById(id: Long): ArchivalConfigEntity = archivalConfigDao.findById(id).orElse(null)

	override fun findByUniqueId(uniqueKey: String): ArchivalConfigEntity = archivalConfigDao.findByUniqueId(uniqueKey)

	override fun create(entity: ArchivalConfigEntity): ArchivalConfigEntity =
			when (entity.id) {
				null -> archivalConfigDao.save(entity)
				else -> throw XmlErsBusinessException(message = "Id was null when updating ArchivalConfig")
			}

	override fun delete(entity: ArchivalConfigEntity): Unit = archivalConfigDao.delete(entity)

	override fun update(entity: ArchivalConfigEntity): ArchivalConfigEntity =
			when (entity.id) {
				null -> throw XmlErsBusinessException(message = "Id must not be null when updating ArchivalConfig")
				else -> archivalConfigDao.save(entity)
			}


}

