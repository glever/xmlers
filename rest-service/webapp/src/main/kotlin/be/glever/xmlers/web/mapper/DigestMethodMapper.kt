/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.DigestMethodEntity
import be.glever.xmlers.rest.api.generated.model.DigestMethod

class DigestMethodMapper {
	fun map(entity: DigestMethodEntity): DigestMethod =
			DigestMethod().let {
				it.oid = entity.oid
				it.javaName = entity.javaName
				it.uri = entity.uri
				return it
			}

	fun map(digestMethod: DigestMethod): DigestMethodEntity =
			DigestMethodEntity(uri = digestMethod.uri, javaName = digestMethod.javaName, oid = digestMethod.oid)
}
