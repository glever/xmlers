/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain.model

import java.util.*

/**
 * Data holder for a Reduced Hash extracted from a @see HashTreeKotlin
 */
data class ReducedHashTree(val id: Long? = null, val reducedHash: ByteArray, val rootHash: ByteArray, val digestMethod: DigestMethod, val reducedHashes: List<List<ByteArray>>) {
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as ReducedHashTree

		if (!Arrays.equals(reducedHash, other.reducedHash)) return false
		if (!Arrays.equals(rootHash, other.rootHash)) return false
		if (digestMethod != other.digestMethod) return false
		if (reducedHashes != other.reducedHashes) return false

		return true
	}

	override fun hashCode(): Int {
		var result = Arrays.hashCode(reducedHash)
		result = 31 * result + Arrays.hashCode(rootHash)
		result = 31 * result + digestMethod.hashCode()
		result = 31 * result + reducedHashes.hashCode()
		return result
	}

}
