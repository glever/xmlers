/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.rest.client

import be.glever.xmlers.rest.api.generated.model.ArchiveObject
import be.glever.xmlers.rest.api.generated.model.DataObject
import be.glever.xmlers.rest.api.generated.model.DataObjectList
import java.math.BigDecimal
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class ArchiveObjectRestClient(private val target: WebTarget) {

	private fun archivalConfigPath(archivalConfigUniqueId: String): WebTarget = target
			.path(archivalConfigUniqueId)

	private fun archiveObjectPath(archivalConfigUniqueId: String, archiveObjectUniqueId: String): WebTarget =
			archivalConfigPath(archivalConfigUniqueId)
					.path(archiveObjectUniqueId)


	fun createArchiveObject(archivalConfigUniqueId: String, archiveObject: ArchiveObject) =
			archivalConfigPath(archivalConfigUniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.put(Entity.entity(archiveObject, MediaType.APPLICATION_JSON_TYPE))
					.readResponse(expectedStatus = Response.Status.CREATED, expectedType = object : GenericType<BigDecimal>() {})

	fun findAllArchiveObjects(archivalConfigUniqueId: String): List<ArchiveObject> =
			archivalConfigPath(archivalConfigUniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get()
					.readResponse(expectedStatus = Response.Status.OK, expectedType = object : GenericType<List<ArchiveObject>>() {})

	fun findArchiveObjectByUniqueId(archivalConfigUniqueId: String, archiveObjectUniqueId: String): ArchiveObject =
			archiveObjectPath(archivalConfigUniqueId, archiveObjectUniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get()
					.readResponse(expectedStatus = Response.Status.OK, expectedType = object : GenericType<ArchiveObject>() {})

	fun deleteArchiveObject(archivalConfigUniqueId: String, archiveObjectUniqueId: String) =
			archivalConfigPath(archivalConfigUniqueId)
					.path(archiveObjectUniqueId.toString())
					.request(MediaType.APPLICATION_JSON_TYPE)
					.delete()
					.assertStatus(Response.Status.NO_CONTENT)

	fun updateDataObjects(archivalConfigUniqueId: String, archiveObjectUniqueId: String, dataObjects: List<DataObject>): Response =
			archiveObjectPath(archivalConfigUniqueId, archiveObjectUniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(DataObjectList().apply { this.dataObjects = dataObjects } , MediaType.APPLICATION_JSON_TYPE))
					.assertStatus(Response.Status.NO_CONTENT)
}