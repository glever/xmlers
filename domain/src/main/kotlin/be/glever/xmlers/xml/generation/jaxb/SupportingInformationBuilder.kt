/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.ObjectFactory
import ietf.params.xml.ns.ers.SupportingInformationType.SupportingInformation

class SupportingInformationBuilder {

	private val supportingInformation: SupportingInformation

	init {
		this.supportingInformation = ObjectFactory().createSupportingInformationTypeSupportingInformation()
	}

	fun withType(type: String?): SupportingInformationBuilder {
		this.supportingInformation.type = type
		return this
	}

	fun withContent(content: Any?): SupportingInformationBuilder {
		this.supportingInformation.content.add(content)
		return this
	}

	fun build(): SupportingInformation {
		return this.supportingInformation
	}

}
