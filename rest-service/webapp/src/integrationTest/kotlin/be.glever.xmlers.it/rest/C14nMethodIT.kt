/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest

import be.glever.xmlers.rest.api.generated.model.C14nMethod
import be.glever.xmlers.rest.client.C14nMethodRestClient
import be.glever.xmlers.rest.client.XmlersRestClient
import be.glever.xmlers.rest.client.exception.XmlErsRestClientException
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import java.util.*
import javax.ws.rs.core.Response
import javax.xml.crypto.dsig.CanonicalizationMethod

class C14nMethodIT : AbstractRestIntegrationIT() {

	companion object {
		fun defaultC14nMethod(): C14nMethod = C14nMethod().apply { uri = CanonicalizationMethod.INCLUSIVE }
		fun randomC14nMethod(): C14nMethod = C14nMethod().apply { uri = UUID.randomUUID().toString() }

		fun createDefaultC14nMethodIfNeeded(restClient: XmlersRestClient): C14nMethod =
				with(defaultC14nMethod()) {
					restClient.c14nMethodRestClient.findAll()
							.filter { c14nMethod -> c14nMethod.uri == this.uri }
							.getOrElse(0, {
								restClient.c14nMethodRestClient.create(this)
								return this
							})
				}

	}

	fun restClient(): C14nMethodRestClient = super.xmlErsRestClient().c14nMethodRestClient

	@Test
	fun addC14nMethodShouldInsert() {
		val initialSize = restClient().findAll().size
		val insertC14nMethod = randomC14nMethod().also { restClient().create(it) }


		restClient().findAll().let { allC14nMethods ->
			Assert.assertEquals(initialSize + 1, allC14nMethods.size)
			Assert.assertEquals(allC14nMethods.first { it.uri == insertC14nMethod.uri }.uri, insertC14nMethod.uri)
		}
	}

	@Test
	fun addDuplicateC14nMethodShouldThrow409() {
		randomC14nMethod().run {
			restClient().create(this)
			try {
				restClient().create(this)
				fail("Should not be able to createArchivalConfig duplicate C14nMethod")
			} catch (xmlErsClientException: XmlErsRestClientException) {
				assertEquals(Response.Status.CONFLICT.statusCode, xmlErsClientException.statusCode)
			}
		}
	}

	@Test
	fun deleteNonExistingC14nMethodReturnNotFound() {
		try {
			restClient().delete("-1")
			Assert.fail("Should not be able to delete non-existing id")
		} catch (e: XmlErsRestClientException) {
			Assert.assertEquals(e.statusCode, Response.Status.NOT_FOUND.statusCode)
		}
	}

	@Test
	fun deleteExistingC14nMethodShouldWork() {
		val initialSize = restClient().findAll().size
		val existingId = randomC14nMethod().also { restClient().create(it) }.uri
		Assert.assertEquals(initialSize + 1, restClient().findAll().size)
		restClient().delete(existingId)
		Assert.assertEquals(initialSize, restClient().findAll().size)
	}

}