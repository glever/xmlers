/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest

import be.glever.xmlers.it.rest.util._isEqual
import be.glever.xmlers.rest.api.generated.model.ArchivalConfig
import be.glever.xmlers.rest.api.generated.model.DigestMethodPolicy
import be.glever.xmlers.rest.api.generated.model.Period
import be.glever.xmlers.rest.api.generated.model.TsaConfig
import be.glever.xmlers.rest.client.ArchivalConfigRestClient
import be.glever.xmlers.rest.client.XmlersRestClient
import be.glever.xmlers.rest.client.exception.XmlErsRestClientException
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test
import java.util.*
import javax.ws.rs.core.Response

class ArchivalConfigIT : AbstractRestIntegrationIT() {

    companion object {

        fun defaultArchivalConfig(restClient: XmlersRestClient): ArchivalConfig =
                ArchivalConfig().apply {
                    this.uniqueId = UUID.randomUUID().toString()

                    this.tsaConfig = TsaConfig().apply {
                        tsaUrl = server.tsaUrl()
                        tsaCert = server.tsaCert().encoded
                    }

                    this.c14nMethod = C14nMethodIT.createDefaultC14nMethodIfNeeded(restClient)
                    this.digestMethodPolicies = listOf(DigestMethodPolicy().apply {
                        this.period = Period().apply {}
                        this.digestMethodOid = DigestMethodIT.createDefaultDigestMethodIfNeeded(restClient).oid
                    })
                }

        fun createDefaultArchivalConfig(xmlErsRestClient: XmlersRestClient) =
                xmlErsRestClient.archivalConfigRestClient.createArchivalConfig(defaultArchivalConfig(xmlErsRestClient))

    }

    private fun restClient(): ArchivalConfigRestClient = super.xmlErsRestClient().archivalConfigRestClient

    @Test
    fun createValidReturnsOk() {
        val initialSize = restClient().findAll().size

        val createdArchivalConfig = restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        Assert.assertEquals(initialSize + 1, restClient().findAll().size)

        val fetched = restClient().getByUniqueId(createdArchivalConfig.uniqueId)
        Assert.assertTrue(createdArchivalConfig._isEqual(fetched))
    }


    @Test
    fun createDuplicateNotAllowed() {
        val archivalConfig = defaultArchivalConfig(xmlErsRestClient())
        restClient().createArchivalConfig(archivalConfig)
        try {
            restClient().createArchivalConfig(archivalConfig)
            fail("Should not be able to createArchivalConfig identical ArchivalConfig")
        } catch (e: XmlErsRestClientException) {
            assertEquals(Response.Status.BAD_REQUEST.statusCode, e.statusCode)
        }
    }

    @Test
    fun createArchivalConfigWithNonExistingDigestMethodReturnsBadRequest() {
        val ac = defaultArchivalConfig(xmlErsRestClient())
        ac.digestMethodPolicies[0].digestMethodOid = "non-existing"

        try {
            restClient().createArchivalConfig(ac)
            fail("Expected bad request")
        } catch (e: XmlErsRestClientException) {
            Assert.assertEquals(Response.Status.BAD_REQUEST.statusCode, e.statusCode)
        }

    }

    @Test
    fun createWithNonExistingC14nMethodReturnsBadRequest() {
        val ac = defaultArchivalConfig(xmlErsRestClient())
        ac.c14nMethod.uri = "non-existing"

        try {
            restClient().createArchivalConfig(ac)
            fail("Expected bad request")
        } catch (e: XmlErsRestClientException) {
            Assert.assertEquals(Response.Status.BAD_REQUEST.statusCode, e.statusCode)
        }
    }


    @Test
    fun updateExistingArchivalConfigReturnsOk() {
        val archivalConfig = restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        val archivalConfigToUpdate = restClient().getByUniqueId(archivalConfig.uniqueId)
        archivalConfigToUpdate.tsaConfig.tsaUrl = "http://updated.tsaUrl"

        restClient().updateDataObjects(archivalConfigToUpdate)
        val updatedAc = restClient().getByUniqueId(archivalConfigToUpdate.uniqueId)
        assertFalse(archivalConfig._isEqual(updatedAc))
        assertTrue(archivalConfigToUpdate._isEqual(updatedAc))
    }

    @Test
    fun updateNonExistingArchivalConfigReturnsNotFound() {
        val ac = restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        val acToUpdate = restClient().getByUniqueId(ac.uniqueId)
        acToUpdate.tsaConfig.tsaUrl = "http://updated.tsaUrl"
        acToUpdate.uniqueId = UUID.randomUUID().toString()

        try {
            restClient().updateDataObjects(acToUpdate)
            fail("Expected 404")
        } catch (e: XmlErsRestClientException) {
            assertEquals(Response.Status.NOT_FOUND.statusCode, e.statusCode)
        }
    }

    @Test
    fun deleteExistingArchivalConfigReturnsOk() {
        val initialSize = restClient().findAll().size
        restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient())).let { restClient().deleteArchivalConfig(it.uniqueId) }
        assertEquals(initialSize, restClient().findAll().size)
    }

    @Test
    fun deleteNonExistingArchivalConfigReturnsNotFound() {
        val expected = Response.Status.NOT_FOUND.statusCode
        try {
            restClient().deleteArchivalConfig("NON-EXISTING-ID")
            fail("$expected expected")
        } catch (e: XmlErsRestClientException) {
            assertEquals(expected, e.statusCode)
        }
    }

    @Test
    fun listAllArchivalConfigReturnsAll() {
        val initialSize = restClient().findAll().size

        restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))

        assertEquals(initialSize + 3, restClient().findAll().size)
    }

    @Test
    fun findExistingByIdReturnsExisting() {
        val created = restClient().createArchivalConfig(defaultArchivalConfig(xmlErsRestClient()))
        val found = restClient().getByUniqueId(created.uniqueId)

        assertTrue(created._isEqual(found))
    }

    @Test
    fun findNonExistingByIdReturnsNotFound() {
        val expected = Response.Status.NOT_FOUND.statusCode
        try {
            restClient().getByUniqueId("NON-EXISTING")
            fail("$expected expected")
        } catch (e: XmlErsRestClientException) {
            assertEquals(expected, e.statusCode)
        }
    }

    @Test
    fun digestMethodPoliciesChainMustNotBeBroken() {
        val defaultArchivalConfig = defaultArchivalConfig(xmlErsRestClient())
        defaultArchivalConfig.digestMethodPolicies

    }

    @Test
    fun thereShouldAlwaysBeDigestMethodPoliciesForCurrentDateUntilEndOfTime() {

    }

    @Test
    fun digestMethodMayOccurMultipleTimesInChain() {
        // Admins prerogative, I don't want to limit flexibility.
    }

    @Test
    fun digestMethodsMayOverlapInTime() {
        // allows for migration of archiveobjects that are ready instead
    }

    @Test
    @Ignore
    fun overlappingDigestMethodsAreUsedAccordingToPriority() {
        TODO("Implement when hashtree renewal is done")

        /*
        dm1 priority 1
        dm2 priority 2
        ao1 with all do's having hashes of dm1 and dm2
        ao2 with all do's only having dm1
        ao3 with all do's only having dm2
        ao4 with full dm1 and partial dm2
        ao5 with partial dm1 and full dm2
        -----
        start LTA
        ==>
        ao1 RHT in dm1
        ao2 RHT in dm1
        ao3 RHT in dm2
        ao4 RHT in dm1
        ao5 RHT in dm2
         */
    }

    @Test
    @Ignore
    fun overlappingDigestMethodPoliciesMayNotHaveSamePriority() {
        TODO("Implement when hashtree renewal is done")
    }

    @Test
    @Ignore
    fun highestAvailableDigestMethodPriorityIsUsed() {
        TODO("Implement when hashtree renewal is done")
        /*
        dm1 prio 1
        dm2 prio 2
        dm1.period == dm2.period
        ao dm1 and dm2
        ---
        TLA
        =>
        RHT in dm1
         */
    }

    @Test
    @Ignore
    fun canonicalisationChangeTriggersHashtreeRenewal() {
        /* c14n is a property of archivetsmtpchain,
        so change in c14n results in new chain added to sequence
         */
    }
}
