import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';

import { metaReducers, ROOT_REDUCERS } from '@xmlers/app.reducers';
import { AppRoutingModule } from '@xmlers/app-routing.module';
import { AppComponent } from '@xmlers/app.component';
import { ApiModule, BASE_PATH, Configuration } from '@xmlers/generated/xmlers-api';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    ApiModule.forRoot(() => new Configuration()),
    AppRoutingModule,

    // ngrx
    StoreModule.forRoot(ROOT_REDUCERS, { metaReducers }),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      name: 'Xmlers'
    }),
    EffectsModule.forRoot([]),
  ],
  providers: [{ provide: BASE_PATH, useValue: 'http://localhost:4200/api' }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
