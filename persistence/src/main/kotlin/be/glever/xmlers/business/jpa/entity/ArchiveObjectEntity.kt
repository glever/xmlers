/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.entity

import javax.persistence.*

@Entity(name = "ArchiveObjectEntity")
@Table(name = "ARCHIVE_OBJECT",
		uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("ARCHIVAL_CONFIG_ID", "UNIQUE_ID")))
)
class ArchiveObjectEntity(

	id: Long? = null,

	@OneToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
	@JoinColumn(name = "ARCHIVE_OBJECT_ID")
	val dataObjects: Set<DataObjectEntity> = setOf(),

	@ManyToOne
	@JoinColumn(name = "HASH_TREE_ID")
	val hashTree: HashTreeEntity? = null,

	@ManyToOne
	@JoinColumn(name = "ARCHIVAL_CONFIG_ID", nullable = false)
	val archivalConfig: ArchivalConfigEntity,

	@Column(name = "UNIQUE_ID", nullable = false)
	val uniqueId: String

) : AbstractXmlErsEntity(id)