/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package tests

import eu.europa.esig.dss.DigestAlgorithm
import eu.europa.esig.dss.client.SecureRandomNonceSource
import eu.europa.esig.dss.client.http.commons.TimestampDataLoader
import eu.europa.esig.dss.client.tsp.OnlineTSPSource
import org.apache.commons.io.IOUtils
import org.bouncycastle.asn1.ASN1Boolean
import org.bouncycastle.asn1.ASN1Integer
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.tsp.MessageImprint
import org.bouncycastle.asn1.tsp.TimeStampReq
import org.bouncycastle.asn1.x509.AlgorithmIdentifier
import org.bouncycastle.tsp.TSPException
import org.bouncycastle.tsp.TimeStampToken

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.Base64

object Timestamp_Main {
    //	 private static final String TSA_URL = "http://freetsa.org/tsr";
    // private static final String TSA_URL = "http://tsa.safecreative.org/";
    private val TSA_URL = "http://tsa.belgium.be/connect"

    @Throws(NoSuchAlgorithmException::class, IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        // java doesnt seem to have the capacity to perform timestamping, use bouncycastle

        val tspSource = OnlineTSPSource(TSA_URL)
        tspSource.setNonceSource(SecureRandomNonceSource())
        tspSource.setDataLoader(TimestampDataLoader())

        val digest = MessageDigest.getInstance(DigestAlgorithm.SHA256.javaName).digest("I am so smart".toByteArray())
        val timeStampResponse = tspSource.getTimeStampResponse(DigestAlgorithm.SHA256, digest)
        println(Base64.getEncoder().encodeToString(timeStampResponse.encoded))

        //		final DigestMethod hashAlgo = DigestMethod.Companion.getSHA_512();
        //		final String message = "I am so smrt";
        //		final byte[] hash = MessageDigest.getInstance(hashAlgo.getJavaName()).digest(message.getBytes());
        //		final long nonce = new SecureRandom().nextLong();
        //		final boolean requireCert = true;
        //		final String timestampPolicy = null;
        //
        //		final TimeStampReq tsaReq = createTimestampReq(hash, nonce, requireCert, hashAlgo.getOid(), timestampPolicy);
        //
        //		final byte[] dataToSend = tsaReq.getEncoded();
        //		final byte[] response = sendTsaRequest(dataToSend);
        //		System.out.println(Base64.getEncoder().encodeToString(response));
        //		final TimeStampResponse tsaResp = new TimeStampResponse(response);
        //		tsaResp.toString();

    }

    @Throws(IOException::class)
    private fun sendTsaRequest(tsaReq: ByteArray): ByteArray {
        val url = URL(TSA_URL)
        val con = url.openConnection() as HttpURLConnection
        con.doOutput = true
        con.doInput = true
        con.requestMethod = "POST"
        con.setRequestProperty("Content-Type", "application/timestamp-query")
        val os = con.outputStream
        os.write(tsaReq)
        os.flush()
        os.close()

        val `is` = con.inputStream
        val baos = ByteArrayOutputStream()
        IOUtils.copy(`is`, baos)

        return baos.toByteArray()
    }

    private fun createTimestampReq(hash: ByteArray, nonce: Long, requireCert: Boolean, oid: String,
                                   timestampPolicy: String?): TimeStampReq {
        val messageImprint = MessageImprint(AlgorithmIdentifier(ASN1ObjectIdentifier(oid)),
                hash)

        val tsaPolicy = if (timestampPolicy ==
                null)
            null
        else
            ASN1ObjectIdentifier(timestampPolicy)
        return TimeStampReq(messageImprint, tsaPolicy, ASN1Integer(nonce), ASN1Boolean.getInstance(requireCert), null)
    }

}
