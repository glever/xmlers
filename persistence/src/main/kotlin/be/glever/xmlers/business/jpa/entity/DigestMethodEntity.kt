/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "DIGEST_METHOD")
class DigestMethodEntity(

	@Column(name = "OID", unique = true, nullable = false)
	val oid: String,

	@Column(name = "JAVANAME", unique = true, nullable = false)
	val javaName: String,

	@Column(name = "URI", unique = true, nullable = false)
	val uri: String

) : AbstractXmlErsEntity(){

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as DigestMethodEntity

		if (oid != other.oid) return false
		if (javaName != other.javaName) return false
		if (uri != other.uri) return false

		return true
	}

	override fun hashCode(): Int {
		var result = oid.hashCode()
		result = 31 * result + javaName.hashCode()
		result = 31 * result + uri.hashCode()
		return result
	}
}
