package be.glever.xmlers.test.util

import org.bouncycastle.asn1.ocsp.CertID
import org.bouncycastle.asn1.ocsp.OCSPRequest
import org.bouncycastle.asn1.ocsp.TBSRequest
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.CRLReason
import org.bouncycastle.asn1.x509.KeyPurposeId
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.X509v2CRLBuilder
import org.bouncycastle.cert.jcajce.JcaX509CRLConverter
import org.bouncycastle.cert.ocsp.*
import org.bouncycastle.cert.ocsp.jcajce.JcaBasicOCSPRespBuilder
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder
import org.bouncycastle.tsp.TimeStampRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.Request
import spark.Service
import spark.Service.ignite
import java.math.BigInteger
import java.security.KeyStore
import java.security.PrivateKey
import java.security.Security
import java.security.cert.X509CRL
import java.security.cert.X509Certificate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*
import javax.naming.ldap.LdapName
import org.bouncycastle.jce.provider.BouncyCastleProvider.PROVIDER_NAME as BC


/**
 * Helper class for Tests. Simulates needed PKI operations such as management  of certificates (create, revoke,...), chains, http methods for
 * CRL, OCSP, postTsa simulations....
 */
class TestPKIServer {
    companion object {
        private val LOG: Logger = LoggerFactory.getLogger(TestPKIServer::class.java)
    }

    init {
        Security.addProvider(BouncyCastleProvider())
    }

    var ocspActive = true
    var crlActive = true
    lateinit var rootCert: X509Certificate
    lateinit var interCert: X509Certificate
    lateinit var tsaCert: X509Certificate
    private val keyStore = KeyStore.getInstance("jks").also { it.load(null) };

    private val service: Service
    private val keyPairCache = TestKeyPairCache()
    private val passwordProtection = KeyStore.PasswordProtection("irrelevant".toCharArray())

    private val tsaPath = "/tsa"
    private val ocspPath = "/ocsp"
    private val crlPath = "/crl"

    private val rootDn = "CN = root.glever.be, O = Glever Inc, L = location, ST = FL.BR., C = BE"
    private val interDn = "CN = inter.glever.be, O = Glever Inc, L = location, ST = FL.BR., C = BE"
    private val tsaDn = "CN = tsa.glever.be, O = Glever Inc, L = location, ST = FL.BR., C = BE"

    private val sha256WithRsa = "SHA256WithRSAEncryption"
    private val sha512WithRSA = "SHA512withRSA"

    init {
        Security.addProvider(BouncyCastleProvider())

        service = ignite().port(0)
        service.post(tsaPath) { req, _ -> postTsa(req) }
        service.post(ocspPath) { req, _ -> postOcsp(req) }
        service.get("$crlPath/:crlName") { req, _ -> getCrl(req) }
        service.init()
        service.awaitInitialization()
        reset()
    }

    fun reset() {

        keyPairCache.freeAll()
        keyStore.aliases().toList().forEach { keyStore.deleteEntry(it) }
        ocspActive = true
        crlActive = true
        keyStore.load(null)
        rootCert = createRootCertificate()
        interCert = createIntermediateTsaCertificate()
        tsaCert = createTsaCertificate()
    }

    fun stop() {
        service.stop()
    }

    fun tsaUrl(): String = "${serverUrl()}$tsaPath"

    fun getChain(cert: X509Certificate): List<X509Certificate> = this.keyStore.getCertificateChain(cert.serialNumber.toString())
            .filterIsInstance<X509Certificate>()
            .filter { !it.equals(cert) && !it.equals(rootCert) }

    private fun serverUrl(): String = "http://localhost:${service.port()}"
    private fun ocspUrl(): String = "${serverUrl()}$ocspPath"
    private fun crlUrl(): String = "${serverUrl()}$crlPath"


    private fun createCertificate(certSpec: CertificateSpec): X509Certificate {
        val certKeyPair = generateKeyPair(certSpec.keyAlgorithm, certSpec.keySize)

        var issuer: KeyStore.PrivateKeyEntry? = null
        val cert = if (certSpec.issuerSerialNr == null) {
            generateV1Certificate(certKeyPair, certSpec.validUntil, certSpec.subjectDn, sha512WithRSA)
        } else {
            issuer = this.keyStore.getEntry(certSpec.issuerSerialNr.toString(), this.passwordProtection) as KeyStore.PrivateKeyEntry
            generateV3Certificate(issuer = issuer, keyPair = certKeyPair, algorithm = sha512WithRSA, pkiServerUrl = serverUrl(), certSpec = certSpec, crlUrl = crlUrl(), ocspUrl = ocspUrl(), caIssuerUrl = "${serverUrl()}/cert/${certSpec.issuerSerialNr}.cert")
        }

        val chain = if (issuer == null) arrayOf(cert) else arrayOf(cert, *issuer.certificateChain)
        this.keyStore.setEntry(cert.serialNumber.toString(), KeyStore.PrivateKeyEntry(certKeyPair.private, chain), passwordProtection)
        return cert
    }


    private fun postTsa(req: Request): ByteArray {
        val reqBytes = req.bodyAsBytes()
        val timeStampRequest = TimeStampRequest(reqBytes)
        val response = TspResponseGenerator(getKey(tsaDn), getCert(tsaDn)).generateResponse(timeStampRequest)
        return response.encoded
    }

    private fun postOcsp(req: Request?): ByteArray =
            if (this.ocspActive)
                ocsResponse(OCSPRequest.getInstance(req!!.bodyAsBytes())).encoded
            else throw IllegalStateException("OCSP not active")

    private fun getCrl(req: Request) =
            if (crlActive)
                req.params("crlName").substringBefore(".crl").toBigInteger()
                        .let { generateCrl(ca = getCert(it), caPrivateKey = getKey(it)).encoded }
            else throw IllegalStateException("CRL not active")

    private fun createRootCertificate() =
            createCertificate(
                    CertificateSpec(
                            subjectDn = rootDn,
                            validUntil = Date.from(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant())
                    )
            )


    private fun createIntermediateTsaCertificate() =
            createCertificate(
                    CertificateSpec(
                            keyPurposes = listOf(KeyPurposeId.id_kp_OCSPSigning),
                            subjectDn = interDn,
                            issuerSerialNr = rootCert.serialNumber,
                            validUntil = rootCert.notAfter!!))


    private fun createTsaCertificate() =
            createCertificate(
                    CertificateSpec(
                            keyPurposes = listOf(KeyPurposeId.id_kp_timeStamping),
                            subjectDn = tsaDn,
                            issuerSerialNr = interCert.serialNumber,
                            validUntil = interCert.notAfter!!))


    private fun generateKeyPair(keyAlgorithm: String, keySize: Int) = keyPairCache.getKeyPair(keyAlgorithm, keySize)
    private fun getKey(dn: String): PrivateKey = getPrivateKeyEntry(dn).privateKey

    // todo should filter on certID.issuerNameHash and .issuerKeyHash
    private fun getPrivateKeyEntry(serialNumber: BigInteger): KeyStore.PrivateKeyEntry = keyStore.getEntry(serialNumber.toString(), passwordProtection) as KeyStore.PrivateKeyEntry

    private fun getKey(serialNumber: BigInteger): PrivateKey = getPrivateKeyEntry(serialNumber).privateKey
    private fun getCert(serialNumber: BigInteger): X509Certificate = getPrivateKeyEntry(serialNumber).certificate as X509Certificate

    private fun getCert(certID: CertID): X509Certificate = getCert(certID.serialNumber.value)

    private fun getPrivateKeyEntry(dn: String) =
            this.keyStore.aliases().toList()
                    .map { alias -> this.keyStore.getEntry(alias, passwordProtection) as KeyStore.PrivateKeyEntry }
                    .filter { entry -> (entry.certificate as X509Certificate).subjectDN.name.let { LdapName(it).equals(LdapName(dn)) } }
                    .first()

    private fun getCert(dn: String): X509Certificate =
            getPrivateKeyEntry(dn).certificate as X509Certificate

    private fun ocsResponse(ocspRequest: OCSPRequest): OCSPResp {
        val tbsRequest = TBSRequest.getInstance(ocspRequest.tbsRequest)
        val ocspReq = org.bouncycastle.asn1.ocsp.Request.getInstance(tbsRequest.requestList.getObjectAt(0))
        val certID = ocspReq.reqCert
        val cert = getCert(certID)

        return ocspResponse(getCert(cert.issuerDN.name), certID)
    }


    private fun ocspResponse(cert: X509Certificate, certID: CertID): OCSPResp {
        val x509CertificateHolder = X509CertificateHolder(cert.encoded)
        val digCalcProv = JcaDigestCalculatorProviderBuilder().setProvider(BC).build()
        val certificateID = CertificateID(certID)
        return OCSPRespBuilder().build(OCSPRespBuilder.SUCCESSFUL,
                JcaBasicOCSPRespBuilder(cert.publicKey, digCalcProv.get(RespID.HASH_SHA1))
                        .addResponse(certificateID, CertificateStatus.GOOD, toDate(LocalDateTime.now()), toDate(LocalDateTime.now().plusHours(1)))
                        .build(JcaContentSignerBuilder(sha256WithRsa)
                                .setProvider(BC)
                                .build(getKey(cert.serialNumber)), arrayOf(x509CertificateHolder), Date()))
    }

    private fun toDate(localDateTime: LocalDateTime): Date {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())
    }

    private fun generateCrl(ca: X509Certificate, caPrivateKey: PrivateKey, vararg revoked: X509Certificate): X509CRL {
        val builder = X509v2CRLBuilder(
                X500Name(ca.subjectDN.name),
                Date()
        ).setNextUpdate(Date(System.currentTimeMillis() + 30758400000))

        for (certificate in revoked) {
            builder.addCRLEntry(certificate.serialNumber, Date(), CRLReason.privilegeWithdrawn)
        }

        val contentSignerBuilder = JcaContentSignerBuilder(sha256WithRsa)

        contentSignerBuilder.setProvider("BC")

        val crlHolder = builder.build(contentSignerBuilder.build(caPrivateKey))

        val converter = JcaX509CRLConverter()

        converter.setProvider("BC")

        return converter.getCRL(crlHolder)
    }

}
