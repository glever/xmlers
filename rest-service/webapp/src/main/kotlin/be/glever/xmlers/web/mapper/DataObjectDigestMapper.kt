/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.DataObjectDigestEntity
import be.glever.xmlers.rest.api.generated.model.Digest
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import java.util.*

class DataObjectDigestMapper(
	private val digestMethodService: DigestMethodService) {
	fun map(entity: DataObjectDigestEntity): Digest =
			Digest().apply {
				this.digestMethodOid = entity.digestMethod.oid
				this.digestValue = Base64.getEncoder().encodeToString(entity.digestValue)
			}

	fun map(digest: Digest): DataObjectDigestEntity =
			DataObjectDigestEntity(
					digestMethod = digestMethodService.findByOid(digest.digestMethodOid)
							?: throw XmlErsBusinessException("DigestMethod wit OID ${digest.digestMethodOid} not found"),
					digestValue = Base64.getDecoder().decode(digest.digestValue),
					dataObject = null // circular dependency when called from DataObjectMapper
			)
}
