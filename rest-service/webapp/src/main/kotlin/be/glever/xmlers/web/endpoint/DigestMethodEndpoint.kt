/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.web.endpoint

import be.glever.xmlers.rest.api.generated.api.DigestMethodApi
import be.glever.xmlers.rest.api.generated.model.DigestMethod
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.web.mapper.DigestMethodMapper
import org.springframework.beans.factory.annotation.Autowired
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status.*
import javax.ws.rs.core.SecurityContext


class DigestMethodEndpoint : DigestMethodApi {
	@Autowired
	lateinit var digestMethodRepository: DigestMethodService

	private fun mapper(): DigestMethodMapper = DigestMethodMapper()

	override fun addDigestMethod(digestMethod: DigestMethod, securityContext: SecurityContext): Response {
		val existingDigestMethods = digestMethodRepository.search(oids = listOf(digestMethod.oid), javaNames = listOf(digestMethod.javaName), uris = listOf(digestMethod.uri))

		if (!existingDigestMethods.isEmpty()) {
			throw WebApplicationException(Response.status(CONFLICT).entity("DigestMethod with at least one of the given oid, javaName or uri already exists.").build())
		}

		val savedDigestMethod = digestMethodRepository.save(mapper().map(digestMethod))
		return Response.status(CREATED)
				.entity(mapper().map(savedDigestMethod))
				.build()
	}

	override fun searchDigestMethods(oids: MutableList<String>?, javaNames: MutableList<String>?, uris: MutableList<String>?, securityContext: SecurityContext?): Response =
			digestMethodRepository.search(
					oids = oids?.toList() ?: emptyList()
					, uris = uris?.toList() ?: emptyList()
					, javaNames = javaNames?.toList() ?: emptyList()
			).let { digestMethods ->
				Response.status(OK)
						.entity(digestMethods.map { mapper().map(it) })
						.build()
			}

	override fun getDigestMethodByOid(oid: String, securityContext: SecurityContext): Response =
			digestMethodRepository.findByOid(oid)
					?.let { Response.status(OK).entity(DigestMethodMapper().map(it)).build() }
					?: throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("No DigestMethod with oid $oid found").build())

	override fun deleteDigestMethod(oid: String, securityContext: SecurityContext?): Response =
			this.digestMethodRepository.findByOid(oid)
					?.let { this.digestMethodRepository.delete(it).let { Response.status(NO_CONTENT).build() } }
					?: throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("No DigestMethod with oid $oid found").build())


}