/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.service.impl

import be.glever.xmlers.business.jpa.dao.C14nMethodDao
import be.glever.xmlers.business.jpa.entity.C14nMethodEntity
import be.glever.xmlers.service.C14nMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Transactional
@Repository
class C14nMethodServiceImpl(val c14nMethodDao: C14nMethodDao) : C14nMethodService {
	override fun findByUri(uri: String): C14nMethodEntity? =
			try {
				c14nMethodDao.findByUri(uri)
			} catch (emptyResultDataAccessException: EmptyResultDataAccessException) {
				throw XmlErsBusinessException()
			}

	override fun findAll(): List<C14nMethodEntity> =
			this.c14nMethodDao.findAll().toList()

	override fun add(c14nMethod: C14nMethodEntity): C14nMethodEntity =
			this.c14nMethodDao.save(c14nMethod)

	override fun delete(uri: String) =
			try {
				this.c14nMethodDao.delete(this.c14nMethodDao.getByUri(uri))
			} catch (e: EmptyResultDataAccessException) {
				throw XmlErsBusinessException()
			}


}

