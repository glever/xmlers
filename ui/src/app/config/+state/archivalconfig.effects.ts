import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap } from 'rxjs/operators';
import { ArchivalConfig, ArchivalConfigService } from '@xmlers/generated/xmlers-api';
import {
  ArchivalConfigActionTypes,
  SearchArchivalConfigActionFailure,
  SearchArchivalConfigSuccessAction
} from '@xmlers/config/+state/actions/archivalconfig.actions';
import { of } from 'rxjs';

@Injectable()
export class ArchivalconfigEffects {

  constructor(
    private actions$: Actions,
    private service: ArchivalConfigService
  ) {
  }

  @Effect()
  searchArchivalConfigs =
    this.actions$.pipe(
      ofType(ArchivalConfigActionTypes.Search),
      switchMap(() =>
        this.service.searchArchivalConfig().pipe(
          map((archivalConfigs: Array<ArchivalConfig>) =>
            new SearchArchivalConfigSuccessAction(archivalConfigs))
          , catchError(error => of(new SearchArchivalConfigActionFailure(error)))
        ))
    );

}
