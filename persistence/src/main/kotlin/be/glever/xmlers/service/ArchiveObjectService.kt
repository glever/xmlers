/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.service

import be.glever.xmlers.business.jpa.entity.ArchiveObjectEntity
import be.glever.xmlers.business.jpa.entity.DataObjectEntity


interface ArchiveObjectService {
	fun insert(archiveObject: ArchiveObjectEntity): ArchiveObjectEntity
	fun findByArchivalConfigIdAndArchiveObjectId(archivalConfigId: Long, archiveObjectId: Long): ArchiveObjectEntity
	fun findByArchivalConfigUniqueIdAndArchiveObjectUniqueId(archivalConfigUniqueId: String, archiveObjectUniqueId: String): ArchiveObjectEntity
	/**
	 * Returns a list of [ArchiveObjectEntity]s with specified ids belonging to the given ArchivalConfigEntity, or
	 * all [ArchiveObjectEntity]s from that ArchivalConfigEntity if archiveObjectIds is empty.
	 */
	fun findAllByArchivalConfigIdAndArchiveObjectIds(archivalConfigId: Long, archiveObjectIds: List<Long>): List<ArchiveObjectEntity>

	fun updateArchiveObjectDigests(archivalConfigUniqueId: String, archiveObjectUniqueId: String, dataObjects: List<DataObjectEntity>)
}