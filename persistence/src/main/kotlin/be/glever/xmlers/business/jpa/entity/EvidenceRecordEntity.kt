/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.business.jpa.entity

import javax.persistence.*

@Table(name = "EVIDENCE_RECORD")
@Entity
data class EvidenceRecordEntity(
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	val id: Long? = null,

	@OneToMany
	@JoinColumn(name = "ER_ID")
	val encryptionInfos: List<EncryptionInfoEntity>,

	@OneToMany
	@JoinColumn(name = "ER_ID")
	val supportingInfos: List<SupportingInfoEntity>,

	@OneToMany
	@JoinColumn(name = "ER_ID")
	val archiveTimestampSequences: List<ArchiveTimeStampSequenceEntity>) : XmlErsEntity
