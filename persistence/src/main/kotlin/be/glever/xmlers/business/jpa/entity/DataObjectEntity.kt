/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.business.jpa.entity

import javax.persistence.*

@Entity
@Table(name = "DATA_OBJECT", uniqueConstraints = [UniqueConstraint(columnNames = ["ARCHIVE_OBJECT_ID", "UNIQUE_ID"])])
class DataObjectEntity(

	id: Long?,

	@Column(name = "UNIQUE_ID")
	val uniqueId: String,

	@ManyToOne
	@JoinColumn(name = "ARCHIVE_OBJECT_ID")
	var archiveObjectEntity: ArchiveObjectEntity?,

	@OneToMany(cascade = [CascadeType.ALL], mappedBy = "dataObject")
	val digests: MutableSet<DataObjectDigestEntity> = mutableSetOf()

) : AbstractXmlErsEntity(id) {
	fun getDigest(oid: String): ByteArray? = digests.firstOrNull { it.digestMethod.oid == oid }?.digestValue

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as DataObjectEntity

		if (uniqueId != other.uniqueId) return false
		if (archiveObjectEntity != other.archiveObjectEntity) return false

		return true
	}

	override fun hashCode(): Int {
		var result = uniqueId.hashCode()
		result = 31 * result + (archiveObjectEntity?.hashCode() ?: 0)
		return result
	}


}