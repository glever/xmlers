/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest

import be.glever.xmlers.domain.util.MessageDigestUtil
import be.glever.xmlers.rest.api.generated.model.ArchivalConfig
import be.glever.xmlers.rest.api.generated.model.ArchiveObject
import be.glever.xmlers.rest.api.generated.model.DataObject
import be.glever.xmlers.rest.api.generated.model.Digest
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.util.*


class EvidenceRecordIT : AbstractRestIntegrationIT() {
	var archivalConfig: ArchivalConfig? = null

	@Before
	fun before() {
		archivalConfig = ArchivalConfigIT.createDefaultArchivalConfig(super.xmlErsRestClient())
	}

	@Test
	fun createBasicEvidenceRecordShouldSucceed() {
		val defaultDigestMethod = DigestMethodIT.defaultDigestMethod()
		val archiveObject = ArchiveObject().apply {
			this.uniqueId = UUID.randomUUID().toString()
			this.dataObjects = mutableListOf(DataObject().apply {
				this.uniqueId = UUID.randomUUID().toString()
				this.digests = mutableListOf(Digest().apply {
					this.digestMethodOid = defaultDigestMethod.oid
					this.digestValue = MessageDigestUtil.toDigestBase64(defaultDigestMethod.javaName, UUID.randomUUID().toString())
				})
			})
		}

		archiveObjectRestClient().createArchiveObject(
				archivalConfigUniqueId = archivalConfig!!.uniqueId,
				archiveObject = archiveObject)

//		TODO("continue")
//		jobControlRestClient().launchArchivalJob(archivalConfig!!.uniqueId)
//        evidenceRecordRestClient().getEvidenceRecord(archivalConfig!!.uniqueId, archiveObject.uniqueId)
	}

	@Test
	@Ignore
	fun runArchivalConfigJobCreatesNewEvidenceRecords() {
		// inject archive objects

		// run archivalConfig job

		// get evidencerecords and validate
	}

	@Test
	@Ignore
	fun getEvidenceRecordBeforeArchivalConfigJobReturnsNotFound() {

	}

	@Test
	@Ignore
	fun evidenceRecordShouldNotBeCreatedUntilArchiveObjectContainsAllDigests() {
		// createArchivalConfig ao with missing digests
		// run job
		// get evidencerecord => not found
		// complete archive object
		// run job
		// get evidencerecord => ok
	}

	@Test
	@Ignore
	fun onlyNewArchiveObjectsMayBeAddedToNewEvidenceRecord() {
	}

	@Test
	fun timestampRenewalRenewsTimestampOfAllEvidenceRecordsAcrossMultipleHashTrees() {
		//
	}

	@Test
	fun hashTreeRenewalIgnoresIncompleteArchiveObjects() {

	}

	@Test
	@Ignore
	fun hashTreeRenewalMayMergeMultipleHashTrees() {
		// nice to have. algorithm for rehashing generates new hashtree over AO + its' old EvidenceRecord
	}
}
