/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest

import be.glever.xmlers.pkix.TimeStamper
import be.glever.xmlers.rest.client.*
import be.glever.xmlers.test.util.DefaultTestPkiServer
import be.glever.xmlers.web.application.XmlErsApplication
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.glassfish.jersey.client.ClientConfig
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.junit4.SpringRunner
import javax.ws.rs.client.ClientBuilder

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = arrayOf(XmlErsApplication::class))
abstract class AbstractRestIntegrationIT {

    companion object {

        lateinit var server: DefaultTestPkiServer
        lateinit private var timestamper: TimeStamper

        @BeforeClass
        @JvmStatic
        fun before() {
            server = DefaultTestPkiServer()
            timestamper = TimeStamper(server.tsaUrl(), server.tsaCert(), server.getChain(server.tsaCert()), server.tsaRootCert())
        }

        @AfterClass
        @JvmStatic
        fun after() {
            server.stop()
        }
    }


    @LocalServerPort()
    var localServerPort: String? = null


    fun xmlErsRestClient(): XmlersRestClient {
        val clientConfig = ClientConfig()
        clientConfig.register(JacksonObjectMapper::class.java)
        clientConfig.register(JavaTimeModule::class.java)

        return XmlersRestClient(ClientBuilder.newClient(), "http://localhost:$localServerPort/rest")
    }

    fun digestMethodRestClient(): DigestMethodRestClient = xmlErsRestClient().digestMethodRestClient
    fun c14nMethodRestClient(): C14nMethodRestClient = xmlErsRestClient().c14nMethodRestClient
    fun archivalConfigRestClient(): ArchivalConfigRestClient = xmlErsRestClient().archivalConfigRestClient
    fun archiveObjectRestClient(): ArchiveObjectRestClient = xmlErsRestClient().archiveObjectRestClient
    fun jobControlRestClient(): JobControlRestClient = xmlErsRestClient().jobControlRestClient
    fun evidenceRecordRestClient(): EvidenceRecordRestClient = xmlErsRestClient().evidenceRecordRestClient
}
