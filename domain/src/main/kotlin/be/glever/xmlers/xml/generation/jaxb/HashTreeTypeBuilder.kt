/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.HashTreeType
import ietf.params.xml.ns.ers.HashTreeType.Sequence
import ietf.params.xml.ns.ers.ObjectFactory

/**
 * Creates an xmlers [HashTreeType] of a reduced hashtree for a given
 *
 * @author glen
 */
class HashTreeTypeBuilder {

	private val hashTreeType: HashTreeType

	init {
		this.hashTreeType = ObjectFactory().createHashTreeType()
	}

	fun withSequence(from: List<Sequence>): HashTreeTypeBuilder {
		this.hashTreeType.sequence.addAll(from)
		return this
	}

	fun build(): HashTreeType {
		return this.hashTreeType
	}

}
