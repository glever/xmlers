/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.domain.model

import be.glever.xmlers.domain.util.ByteArrayComparator
import java.security.MessageDigest

/**
 * Container for closely-related DataObjects (such as a signed document and its' certificate info).
 */
class ArchiveObject(val digestMethod: DigestMethod, val dataObjects: MutableList<DataObject> = mutableListOf()) {

	constructor(digestMethod: DigestMethod, dataObject: DataObject) : this(digestMethod, mutableListOf(dataObject))

	init {
		if (dataObjects.size == 0) {
			throw IllegalArgumentException("Cannot pass empty or null dataObject list.")
		}
	}

	/**
	 * Calculates the Archive Objects' digest over its' children or when only 1 child returns the digest of this child, according to XMLERS spec.
	 */
	fun getDigest(): ByteArray {
		val md = MessageDigest.getInstance(digestMethod.javaName)
		if (dataObjects.size == 1) {
			val dataObject = dataObjects[0]
			return dataObject.digests[digestMethod] ?: throw IllegalArgumentException("$dataObject does not contain a digest of type $digestMethod")
		}

		dataObjects.map { dataObject ->
			dataObject.digests[digestMethod] ?: throw IllegalArgumentException("$dataObject does not contain a digest of type $digestMethod")
		}.sortedWith(
                ByteArrayComparator()
		).forEach { hash ->
			md.update(hash)
		}
		return md.digest()
	}
}
