/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.ArchivalConfigEntity
import be.glever.xmlers.business.jpa.entity.ArchiveObjectEntity
import be.glever.xmlers.rest.api.generated.model.ArchiveObject
import be.glever.xmlers.service.DigestMethodService

class ArchiveObjectMapper(
	private val digestMethodService: DigestMethodService){
	fun map(entity: ArchiveObjectEntity): ArchiveObject =
			ArchiveObject().apply {
				this.uniqueId = entity.uniqueId
				this.dataObjects = entity.dataObjects.map { DataObjectMapper(digestMethodService).map(it) }
			}

	fun map(archiveObject: ArchiveObject, archivalConfigEntity: ArchivalConfigEntity): ArchiveObjectEntity =
			ArchiveObjectEntity(
					dataObjects = archiveObject.dataObjects.map { DataObjectMapper(digestMethodService).map(it) }.toMutableSet(),
					uniqueId = archiveObject.uniqueId,
					hashTree = null,
					archivalConfig = archivalConfigEntity
			).apply {
				dataObjects.forEach { it.archiveObjectEntity = this }
			}
}
