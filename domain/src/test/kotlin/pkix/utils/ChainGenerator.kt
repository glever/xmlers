/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix.utils

import java.io.ByteArrayInputStream
import java.io.IOException
import java.math.BigInteger
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.SecureRandom
import java.security.Security
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.cert.CertificateFactory
import java.util.ArrayList
import java.util.Random

import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.ContentSigner
import org.bouncycastle.operator.OperatorCreationException
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder

class ChainGenerator {

    /**
     * Creates a keychain of specs.length. If specs[i] is not null, this spec is
     * used, else a default spec is used. In other words: generate(new
     * CertificateSpec[3] will create keychain with 3 certificates, each with
     * default values.
     *
     * @param specs
     * @return
     */
    @Throws(OperatorCreationException::class, CertificateException::class, NoSuchProviderException::class, NoSuchAlgorithmException::class, IOException::class)
    fun generate(vararg specs: CertificateSpec): List<Certificate>? {
        val ret = ArrayList<Certificate>()
        var caCert: Certificate? = null
        for (spec in specs) {
            val cert = generateCertificate(caCert, spec)
            ret.add(cert)
            caCert = cert
        }

        return ret
    }

    @Throws(OperatorCreationException::class, NoSuchProviderException::class, NoSuchAlgorithmException::class, IOException::class, CertificateException::class)
    private fun generateCertificate(caCert: Certificate?, spec: CertificateSpec): Certificate {

        if (caCert == null) {
            // generate self signed cert

        }
        // generate cert with caCert as issuer
        return createCert(spec)
    }

    @Throws(OperatorCreationException::class, IOException::class, NoSuchAlgorithmException::class, CertificateException::class)
    fun createCert(spec: CertificateSpec): Certificate {
        Security.addProvider(BouncyCastleProvider())
        val keyPairGenerator = KeyPairGenerator.getInstance(spec.kpAlgo)
        keyPairGenerator.initialize(spec.kpKeySize, SecureRandom())
        val keyPair = keyPairGenerator.generateKeyPair()

        val serialNumber = BigInteger.valueOf(Math.abs(Random().nextLong()))

        val subjectName = X500Name(spec.dirName)
        val sPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.public
                .encoded)

        val sigGen = JcaContentSignerBuilder(spec.signatureAlgo).build(keyPair.private)

        val generatedCert = X509v3CertificateBuilder(subjectName, serialNumber,
                spec.startDate, spec.expiryDate, subjectName, sPubKeyInfo)
                .build(sigGen)
        return CertificateFactory.getInstance("X509")
                .generateCertificate(ByteArrayInputStream(generatedCert.encoded))
    }
}
