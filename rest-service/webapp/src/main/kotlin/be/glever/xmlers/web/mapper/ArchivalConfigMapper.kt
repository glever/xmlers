/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.ArchivalConfigEntity
import be.glever.xmlers.rest.api.generated.model.ArchivalConfig
import be.glever.xmlers.service.C14nMethodService
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException

class ArchivalConfigMapper(val digestMethodService: DigestMethodService, val c14nMethodService: C14nMethodService) {

    fun map(entity: ArchivalConfigEntity): ArchivalConfig =
            ArchivalConfig().apply {
                digestMethodPolicies = entity.digestMethodPolicyEntities.map { DigestMethodPolicyMapper(digestMethodService).map(it) }
                c14nMethod = C14nMethodMapper().map(entity.c14nMethod)
                tsaConfig = TsaConfigMapper().map(entity.tsaConfig)
                uniqueId = entity.uniqueId
            }


    fun map(archivalConfig: ArchivalConfig): ArchivalConfigEntity =
            ArchivalConfigEntity(
                    id = null,
                    c14nMethod = c14nMethodService.findByUri(archivalConfig.c14nMethod.uri)
                            ?: throw XmlErsBusinessException("C14nMethod with uri ${archivalConfig.c14nMethod.uri} not found"),
                    digestMethodPolicyEntities = archivalConfig.digestMethodPolicies.map { DigestMethodPolicyMapper(digestMethodService).map(it) }.toMutableSet(),
                    tsaConfig = TsaConfigMapper().map(archivalConfig.tsaConfig),
                    uniqueId = archivalConfig.uniqueId
            )
}
