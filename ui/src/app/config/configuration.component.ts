import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ArchivalConfig } from '@xmlers/generated/xmlers-api';
import { Observable } from 'rxjs';
import * as fromArchivalConfigs from '@xmlers/config/+state/reducers';
import { getAllArchivalConfigs } from '@xmlers/config/+state/reducers';
import { SearchArchivalConfigAction } from '@xmlers/config/+state/actions/archivalconfig.actions';

@Component({
  selector: 'app-configuration',
  template: `
    <h1>ArchivalConfigs</h1>
    <app-archival-config-list [archivalConfigs]="archivalConfigs$ | async"></app-archival-config-list>
  `,
  styles: []
})
export class ConfigurationComponent implements OnInit {
  archivalConfigs$: Observable<ArchivalConfig[]>;

  constructor(private store: Store<fromArchivalConfigs.State>) {
    this.archivalConfigs$ = store.pipe(
      select(getAllArchivalConfigs)
    );
  }

  ngOnInit(): void {
    this.store.dispatch(new SearchArchivalConfigAction());
  }

}
