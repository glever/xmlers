package be.glever.xmlers.business.jpa.entity

import be.glever.xmlers.business.jpa.converter.X509CertificateConverter
import java.security.cert.X509Certificate
import javax.persistence.Column
import javax.persistence.Convert
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(
        name = "TSA_CONFIG"
)
class TsaConfigEntity(
        id: Long? = null,

        @Column(name = "TSA_URL", nullable = false)
        val tsaUrl: String,

        @Column(name = "TSA_CERT", nullable = false)
        @Convert(converter = X509CertificateConverter::class)
        val tsaCert: X509Certificate

) : AbstractXmlErsEntity(id = id) {
}
