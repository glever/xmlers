/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.security

import java.security.cert.CertPathChecker
import java.security.cert.Certificate
import java.util.*

/**
 * Certpath Checker that performs ocsp/crl lookups for a given certificate.
 * Priority is given to ocsp. This checker accumulates any response encountered
 * for later retrieval.
 *
 * @author glenv
 */
class OcspCrlCertpathChecker : CertPathChecker {

    private val crls = HashMap<Certificate, ByteArray>()
    private val ocspResponses = HashMap<Certificate, ByteArray>()

    override fun init(forward: Boolean) {
        if (forward) {
            throw IllegalArgumentException("Forward checking not supported")
        }
    }

    override fun isForwardCheckingSupported(): Boolean {
        return false
    }

    override fun check(cert: Certificate) {
        // TODO Auto-generated method stub

    }

}
