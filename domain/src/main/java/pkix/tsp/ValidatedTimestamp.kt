/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix.tsp

import java.security.cert.CRL
import java.util.HashSet

import org.bouncycastle.cert.ocsp.OCSPResp
import org.bouncycastle.tsp.TimeStampResponse

class ValidatedTimestamp(val timestampResponse: TimeStampResponse) {
    val crls: Set<CRL> = HashSet()
    val ocspResponses: Set<OCSPResp> = HashSet()
}
