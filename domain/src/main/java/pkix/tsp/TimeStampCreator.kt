/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix.tsp

import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClientBuilder
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.tsp.TSPException
import org.bouncycastle.tsp.TimeStampRequest
import org.bouncycastle.tsp.TimeStampRequestGenerator
import org.bouncycastle.tsp.TimeStampResponse
import java.io.IOException
import java.math.BigInteger
import java.net.URISyntaxException
import java.security.SecureRandom
import java.security.cert.CertStore
import java.security.cert.TrustAnchor

/**
 * Constructor.
 *
 * @param trustAnchors
 * TrustAnchors for the TSA root certificate.
 * @param certStore
 * Optional [CertStore] for building a
 * [java.security.cert.CertPath] to any of the trustAnchors.
 */
class TimeStampCreator
(private val trustAnchors: Set<TrustAnchor>?, private val certStore: CertStore?) {

    @Throws(URISyntaxException::class, IOException::class, TSPException::class)
    fun create(imprint: ByteArray, hashAlgo: ASN1ObjectIdentifier, tsaUrl: String): ValidatedTimestamp {
        val tsr = TimeStampRequestGenerator()
        tsr.setCertReq(true)
        val nonce = BigInteger.valueOf(SecureRandom().nextLong())
        val timeStampRequest = tsr.generate(hashAlgo, imprint, nonce)

        val tsResp = callTsa(timeStampRequest, tsaUrl)
        return validate(tsResp)
    }

    private fun validate(tsResp: TimeStampResponse): ValidatedTimestamp {
        return ValidatedTimestamp(tsResp)
    }

    @Throws(IOException::class, TSPException::class)
    private fun callTsa(timeStampRequest: TimeStampRequest, tsaUrl: String): TimeStampResponse {
        val httpClient = HttpClientBuilder.create().build()
        val post = HttpPost(tsaUrl)
        val resp = httpClient.execute(post)
        return TimeStampResponse(IOUtils.toByteArray(resp.entity.content))
    }
}
