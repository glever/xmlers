# About
This module provides a server with PKI functionality needed for unit testing XMLERS.  
It is written in Kotlin, based on sparkjava so launches fast (could be faster because keys are regenerated on the fly).  
The logic is currently hard-coded and that won't change unless needed.

Implemented features:
* Http Server which launches on a random port.
* Hard coded timestamping certificate chain
* Timestamping endpoint on /tsa
* OCSP responder on /ocsp => everything status good

Next up to be implemented
* CRL endpoint on /crl/{certificateSerialNr}.crl => everything status good
* certificate download url on /cert/{certificateSerialNr}.crt , for AIA support

Probably to be implemented:
* ocsp response which includes the chain
* dynamic (re-)configuration of certificate chains
* certificate revocation 

# How to use
Not sure if you can use it as-is for your usecase, but the sources may be good for copy/paste inspiration.

This command will install to a local maven repo: `gradle build install`   

Check out:
* TestPkiServer
* DefaultTestPkiServerTest
* AbstractPkiEnabledTest and subclasses in module domain to see how to start as part of test.
