package be.glever.xmlers.business.jpa.converter

import java.io.ByteArrayInputStream
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class X509CertificateConverter : AttributeConverter<X509Certificate, ByteArray> {

    override fun convertToDatabaseColumn(attribute: X509Certificate?): ByteArray = attribute!!.encoded

    override fun convertToEntityAttribute(dbData: ByteArray?): X509Certificate =
            CertificateFactory.getInstance("X.509").generateCertificate(ByteArrayInputStream(dbData)) as X509Certificate


}
