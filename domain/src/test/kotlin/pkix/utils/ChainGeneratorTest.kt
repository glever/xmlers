/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix.utils

import java.io.IOException
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.util.Arrays

import org.bouncycastle.operator.OperatorCreationException
import org.junit.Test

class ChainGeneratorTest {
    @Test
    @Throws(OperatorCreationException::class, CertificateException::class, NoSuchProviderException::class, NoSuchAlgorithmException::class, IOException::class)
    fun testGenerateChain() {
//        val generate = ChainGenerator().generate(, null, null)
//        println(Arrays.toString(generate!!.toTypedArray()))
    }

}
