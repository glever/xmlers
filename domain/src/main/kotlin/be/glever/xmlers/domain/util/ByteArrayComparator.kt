/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.domain.util

/**
 * Core Comparator for all HashTree sorting tasks. Compares given ByteArray s in little endian fashion.
 * For example:
 * [0,1] <=> [0,1] ==> 0
 * [0,1] <=> [0,1,2] => -1
 * [0,1,2] <=> [0,1] =>  1
 */
class ByteArrayComparator : Comparator<ByteArray> {
	override fun compare(o1: ByteArray, o2: ByteArray): Int {
		val l1 = o1.size
		val l2 = o2.size

		// java is big-endian, most significant bit is lowest index
		var i = 0
		while (i < l1 && i < l2) {
			val diff = o1[i] - o2[i]
			if (diff != 0) {
				return diff
			}
			i++
		}

		// at least 1 array is equal to start of other
		return if (l1 < l2) {
			-1
		} else if (l1 > l2) {
			1
		} else {
			0
		}
	}

}
