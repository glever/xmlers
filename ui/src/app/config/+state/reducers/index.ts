import * as fromRoot from '@xmlers/app.reducers';
import * as fromArchivalConfigs from '@xmlers/config/+state/reducers/archivalconfig.reducer';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

export interface ArchivalConfigsState {
  archivalConfigs: fromArchivalConfigs.State;
}

export interface State extends fromRoot.State {
  archivalConfigs: ArchivalConfigsState;
}

export const reducers: ActionReducerMap<ArchivalConfigsState, any> = {
  archivalConfigs: fromArchivalConfigs.reducer
};

export const getArchivalConfigsState = createFeatureSelector<State, ArchivalConfigsState>('archivalConfigs');
export const getArchivalConfigEntitiesState = createSelector(getArchivalConfigsState, state => state.archivalConfigs);
export const getSelectedId = createSelector(getArchivalConfigEntitiesState, fromArchivalConfigs.getSelectedId);

export const {
  selectIds: selectArchivalConfigIds,
  selectEntities: selectArchivalConfigEntities,
  selectAll: selectAllArchivalConfigs,
  selectTotal: selectTotalArchivalConfigs
} = fromArchivalConfigs.adapter.getSelectors(getArchivalConfigEntitiesState);

export const getAllArchivalConfigs =
  createSelector(selectAllArchivalConfigs, archivalConfigs => archivalConfigs);
