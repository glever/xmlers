package be.glever.xmlers.xml.generation.stax

import be.glever.xmlers.domain.model.ArchiveTimestamp
import be.glever.xmlers.domain.model.ReducedHashTree
import java.util.*
import javax.xml.crypto.dsig.CanonicalizationMethod
import javax.xml.stream.XMLStreamWriter

fun writeAttributes(writer: XMLStreamWriter) {
    // todo implement
}

fun writeArchiveTimeStampChain(writer: XMLStreamWriter, reducedHashTree: ReducedHashTree?, archiveTimestamp: ArchiveTimestamp) {
    writer.writeStartElement("ArchiveTimeStampChain")

    writer.writeAttribute("Order", "1")
    writeDigestMethod(writer, archiveTimestamp.digestMethod.uri)
    writeCanonicalizationMethod(writer, CanonicalizationMethod.INCLUSIVE)
    writeArchiveTimeStamp(writer, archiveTimestamp, reducedHashTree)


    writer.writeEndElement()
}

fun writeArchiveTimeStamp(writer: XMLStreamWriter, archiveTimestamp: ArchiveTimestamp, reducedHashTree: ReducedHashTree?) {
    writer.writeStartElement("ArchiveTimeStamp")

    writer.writeAttribute("Order", "1")

    writeHashTree(writer, reducedHashTree)
    writeTimeStamp(writer, archiveTimestamp.timestamp.timestamp)
    writeAttributes(writer)

    writer.writeEndElement()
}

fun writeArchiveTimeStampSequence(writer: XMLStreamWriter, reducedHashTree: ReducedHashTree?, archiveTimestamp: ArchiveTimestamp) {
    writer.writeStartElement("ArchiveTimeStampSequence")

    writeArchiveTimeStampChain(writer, reducedHashTree, archiveTimestamp)

    writer.writeEndElement()
}

fun writeCanonicalizationMethod(writer: XMLStreamWriter, algorithm: String) {
    writer.writeStartElement("CanonicalizationMethod")
    writer.writeAttribute("Algorithm", algorithm)
    writer.writeCharacters("")
    writer.writeEndElement()
}

fun writeCryptographicInformationList(writer: XMLStreamWriter) {
    //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
}

fun writeDigestMethod(writer: XMLStreamWriter, uri: String) {
    writer.writeStartElement("DigestMethod")

    writer.writeAttribute("Algorithm", uri)

    writer.writeCharacters("")
    writer.writeEndElement()
}

fun writeEncryptionInformation(writer: XMLStreamWriter) {
    // TODO implement
    //        writer.writeStartElement("EncryptionInformation")
    // ..
    //        writer.writeEndElement()
}

fun writeHashTree(writer: XMLStreamWriter, reducedHashTree: ReducedHashTree?) {
    if (reducedHashTree != null) {
        writer.writeStartElement("HashTree")


        reducedHashTree.reducedHashes.forEachIndexed { index, hashes ->
            writer.writeStartElement("Sequence")

            writer.writeAttribute("Order", (index + 1).toString())
            hashes.forEach { hash ->
                writer.writeStartElement("DigestValue")
                writer.flush()
                writer.writeCharacters(Base64.getEncoder().encodeToString(hash))
                writer.writeEndElement()
            }
            writer.writeEndElement()
        }
        writer.writeEndElement()
    }
}

fun writeSupportingInformationList(writer: XMLStreamWriter) {
//        TODO implement
//        writer.writeStartElement("SupportingInformationList")
//                ...
//        writer.writeEndElement()
}

fun writeTimeStamp(writer: XMLStreamWriter, timestamp: ByteArray) {
    writer.writeStartElement("TimeStamp")

    writeCryptographicInformationList(writer)
    writeTimeStampToken(writer, timestamp)

    writer.writeEndElement()
}

fun writeTimeStampToken(writer: XMLStreamWriter, timestamp: ByteArray) {
    writer.writeStartElement("TimeStampToken")
    writer.writeAttribute("Type", "RFC3161")
    writer.writeCharacters(Base64.getEncoder().encodeToString(timestamp))
    writer.writeEndElement()
}
