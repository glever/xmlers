/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.rest.client

import be.glever.xmlers.rest.api.generated.model.DigestMethod
import java.util.*
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class DigestMethodRestClient(val target: WebTarget) {

	fun find(uris: List<String> = listOf(), javaNames: List<String> = listOf(), oids: List<String> = listOf()): List<DigestMethod> =
			target.queryParam("oids", *oids.toTypedArray())
					.queryParam("javaNames", *javaNames.toTypedArray())
					.queryParam("uris", *uris.toTypedArray())
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get().readResponse(expectedStatus = Response.Status.OK, expectedType = object : GenericType<ArrayList<DigestMethod>>() {})


	fun getByOid(oid: String): DigestMethod =
			target.path(oid)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get()
					.readResponse(expectedStatus = Response.Status.OK, expectedType = GenericType(DigestMethod::class.java))


	fun create(digestMethod: DigestMethod): DigestMethod =
			target.request(MediaType.APPLICATION_JSON_TYPE)
					.put(Entity.entity(digestMethod, MediaType.APPLICATION_JSON_TYPE))
					.readResponse(expectedStatus = Response.Status.CREATED, expectedType = GenericType(DigestMethod::class.java))

	fun deleteDigestMethod(digestMethod: DigestMethod) =
			target.path(digestMethod.oid)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.delete()
					.assertStatus(expectedStatus = Response.Status.NO_CONTENT)

}