/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest

import be.glever.xmlers.rest.api.generated.model.*
import be.glever.xmlers.rest.client.exception.XmlErsRestClientException
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.security.MessageDigest
import java.util.*
import javax.ws.rs.core.Response

class ArchiveObjectIT : AbstractRestIntegrationIT() {
	companion object {

		private fun defaultArchiveObject(digestMethod: DigestMethod): ArchiveObject =
				ArchiveObject().apply {
					this.uniqueId = UUID.randomUUID().toString()
					this.dataObjects = mutableListOf(DataObject().apply {
						this.uniqueId = UUID.randomUUID().toString()
						this.digests = mutableListOf(Digest().apply {
							this.digestMethodOid = digestMethod.oid
							this.digestValue = Base64.getEncoder().encodeToString(MessageDigest.getInstance(digestMethod.javaName).digest("toDigest".toByteArray()))
						})
					})
				}
	}

	private var defaultDigestMethod: DigestMethod? = null
	private var defaultC14nMethod: C14nMethod? = null
	private var defaultArchivalConfig: ArchivalConfig? = null
	private fun defaultArchiveObject(): ArchiveObject = defaultArchiveObject(defaultDigestMethod!!)

	@Before
	fun before() {
		defaultDigestMethod = DigestMethodIT.createDefaultDigestMethodIfNeeded(xmlErsRestClient())
		defaultC14nMethod = C14nMethodIT.createDefaultC14nMethodIfNeeded(xmlErsRestClient())
		defaultArchivalConfig = ArchivalConfigIT.createDefaultArchivalConfig(xmlErsRestClient())
	}


	@Test
	fun validArchiveObjectShouldBeInserted() {
		archiveObjectRestClient().createArchiveObject(defaultArchivalConfig!!.uniqueId, defaultArchiveObject())
	}

	@Test
	fun dataObjectUniqueKeyShouldBeUniqueWithinArchiveObject() {
		val defaultArchiveObject = defaultArchiveObject()
		defaultArchiveObject.dataObjects.add(DataObject().apply { this.uniqueId = defaultArchiveObject.dataObjects.first().uniqueId })

		try {
			archiveObjectRestClient().createArchiveObject(defaultArchivalConfig!!.uniqueId, defaultArchiveObject)
			fail("Should not be able to have AO containing DOs with identical unique keys")
		} catch (e: XmlErsRestClientException) {
			assertEquals(Response.Status.BAD_REQUEST.statusCode, e.statusCode)
		}
	}

	@Test
	fun canAddDigestsLater() {
		// setup: createArchivalConfig AO without digests
		val archivalConfigUniqueId = defaultArchivalConfig!!.uniqueId
		val digestMethod = defaultDigestMethod!!
		val defaultArchiveObject = defaultArchiveObject()
		val archiveObjectUniqueId = defaultArchiveObject.uniqueId
		defaultArchiveObject.dataObjects.forEach { it.digests = listOf() }

		archiveObjectRestClient().createArchiveObject(archivalConfigUniqueId, defaultArchiveObject)
		val originalAo = archiveObjectRestClient().findArchiveObjectByUniqueId(archivalConfigUniqueId, archiveObjectUniqueId)
		Assert.assertEquals(0, originalAo.dataObjects.map { it.digests.size }.sum())

		// action: add digests
		originalAo.dataObjects.forEach {
			it.digests = listOf(
					Digest().apply {
						this.digestMethodOid = digestMethod.oid
						this.digestValue = Base64.getEncoder().encodeToString(MessageDigest.getInstance(digestMethod.javaName).digest(UUID.randomUUID().toString().toByteArray()))
					}
			)
		}


		archiveObjectRestClient().updateDataObjects(archivalConfigUniqueId, originalAo.uniqueId, originalAo.dataObjects)
		val updatedAo = archiveObjectRestClient().findArchiveObjectByUniqueId(archivalConfigUniqueId, archiveObjectUniqueId)

		// test: validate digests updated correctly
		updatedAo.dataObjects.forEach { Assert.assertEquals(1, it.digests.size) }

		assertDigestsEqual(originalAo, updatedAo)

	}

	private fun assertDigestsEqual(originalAo: ArchiveObject, updatedAo: ArchiveObject) {
		Assert.assertEquals(updatedAo.dataObjects.map { it.digests.size }.sum(), originalAo.dataObjects.map { it.digests.size }.sum())

		updatedAo.dataObjects.forEach { updatedDo ->
			originalAo.dataObjects.first { originalDo ->
				updatedDo.uniqueId == originalDo.uniqueId
			}.digests.forEach { originalDigest ->
				if (updatedDo.digests.none { updatedDigest ->
							updatedDigest.digestValue == originalDigest.digestValue && updatedDigest.digestMethodOid == originalDigest.digestMethodOid
						}) Assert.fail("Updated digest not found")
			}
		}
	}


	@Test
	fun canOnlyAddDigestForDoIfDigestMethodNotYetUsed() {
		val defaultArchiveObject = defaultArchiveObject()
		defaultArchiveObject.dataObjects = mutableListOf()
		defaultArchiveObject.dataObjects.add(
				DataObject().apply {
					this.uniqueId = UUID.randomUUID().toString()
					this.digests = listOf(
							Digest().apply {
								this.digestMethodOid = defaultDigestMethod!!.oid
								this.digestValue = Base64.getEncoder().encodeToString(MessageDigest.getInstance(defaultDigestMethod!!.javaName).digest(UUID.randomUUID().toString().toByteArray()))
							},
							Digest().apply {
								this.digestMethodOid = defaultDigestMethod!!.oid
								this.digestValue = Base64.getEncoder().encodeToString(MessageDigest.getInstance(defaultDigestMethod!!.javaName).digest(UUID.randomUUID().toString().toByteArray()))
							})
				}
		)

		try {
			archiveObjectRestClient().createArchiveObject(defaultArchivalConfig!!.uniqueId, defaultArchiveObject)
			fail("DataObject cannot have multiple digests of same algorithm.")
		} catch (e: XmlErsRestClientException) {
			assertEquals(Response.Status.BAD_REQUEST.statusCode, e.statusCode)
		}
	}

	@Test
	@Ignore
	fun archiveObjectOnlyAddedToHashTreeOnceAllRequiredDigestsPresent() {
// todo
	}

	@Test
	@Ignore
	fun hashTreeRenewalOnlyDoneOnceAllRequiredDigestsPresent() {
		TODO()
	}

	// Future Features Tests

	@Test
	@Ignore
	fun xmlDataObjectsShouldUseArchivalConfigC14nMethod() {
		TODO("Implement when adding support for xml DataObjects")
	}

	@Test
	@Ignore
	fun createAoFromExistingReducedTreeXml() {
		TODO("Implement when deciding if/how to support")
		// could be interesting feature that when only a reduced tree (which is still valid) remains, it gets added
		// to a new hashtree. Should follow the standard hashtree renewal rules.
	}

}