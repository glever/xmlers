package be.glever.xmlers.test.util

import org.bouncycastle.asn1.x509.KeyPurposeId
import java.math.BigInteger
import java.time.LocalDate
import java.util.*

data class CertificateSpec(
        val keyAlgorithm: String = "RSA",
        val keySize: Int = 2048,
        val subjectDn: String,
        val issuerSerialNr: BigInteger? = null,
        val validFrom: Date = Date(),
        val validUntil: Date = Date(LocalDate.now().plusYears(1).toEpochDay()),
        val isEndEntity: Boolean = false,
        val isCa: Boolean = false,
        val keyPurposes: List<KeyPurposeId> = emptyList()

)
