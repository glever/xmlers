/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain.model

import be.glever.xmlers.domain.util.ByteArrayComparator
import java.security.MessageDigest
import kotlin.math.ceil
import kotlin.math.ln
import kotlin.math.pow

/**
 * A fully balanced hashtree.
 * Constructor creates a HashTree for given input elements.
 * For processing reasons the input list will be expanded with dummy hashes until it is fully balanced.
 * Worst-case scenario, a binary tree has the least amount of dummy nodes
 * ( n*2-1 vs n*3-1 for ternary tree or n*4-1 for ... quadrary...? tree)
 *
 * @arity How many children per parent (binary, ternary,....)
 * @digestMethod The java name for the hashing algorithm used.
 * @archiveObjects The input list
 */
class HashTree constructor(val digestMethod: DigestMethod, val arity: Short = 2, digests: List<ByteArray>) {
    constructor(arity: Short = 2, digestMethod: DigestMethod, archiveObjects: List<ArchiveObject>) : this(digestMethod, arity, archiveObjects.map { it.getDigest() }.toMutableList())


    val hashes: List<List<ByteArray>>

    init {
        if (arity < 2) throw IllegalArgumentException("Minimum tree arity is 2")
        if (digests.size < 1) throw IllegalArgumentException("Archive object list is empty")

        val digestsMutableList = digests.toMutableList()
        balanceAndSortInputList(digestMethod, arity, digestsMutableList)

        val treeDepth = calcTreeDepth(digests.size, arity)

        this.hashes = mutableListOf()
        hashes.add(digestsMutableList)

        for (i in 1 until treeDepth) {
            hashes.add(createParentHashes(hashes[i - 1]))
        }

    }


    /**
     * Generates the ReducedHashTree for the given hash. An IllegalArgumentException will be thrown if the hash is not present.
     */

    fun reduce(hash: ByteArray): ReducedHashTree {
        val hashIndex = getLeafIndex(hash)
        if (hashIndex < 0) throw IllegalArgumentException("Given hash is not present (or hash list is unsorted, which indicates a bug in the program)")
        val reducedHashes = mutableListOf<List<ByteArray>>()
        val reducedTreeDepth = this.hashes.size - 1 // root hash not part of hash list in Reduced Tree.
        for (i in 0 until reducedTreeDepth) {
            val curHashIdx = if (hashIndex == 0) hashIndex else (hashIndex / Math.pow(arity.toDouble(), i.toDouble())).toInt()
            reducedHashes.add(getSiblings(curHashIdx, i))
        }

        return ReducedHashTree(reducedHash = hash, rootHash = getRootHash(), digestMethod = digestMethod, reducedHashes = reducedHashes)
    }

    /**
     * Returns the root hash of this HashTree.
     */
    fun getRootHash(): ByteArray {
        return this.hashes.last().first()
    }


    private fun getLeafIndex(hashToReduce: ByteArray) =
            this.hashes[0].binarySearch(hashToReduce, ByteArrayComparator())


    fun getSiblings(hash: ByteArray): List<ByteArray> {
        return getSiblings(getLeafIndex(hash))
    }

    private fun getSiblings(hashIndex: Int, treeLevel: Int = 0): List<ByteArray> {
        val siblings = mutableListOf<ByteArray>()
        val currentTreeLevel = this.hashes[treeLevel]

        val start = (hashIndex / arity) * arity
        val end = start + arity

        for (i in start until end) {
            if (i != hashIndex) {
                siblings.add(currentTreeLevel[i])
            }
        }

        return siblings
    }

    private fun createParentHashes(leafHashes: List<ByteArray>): List<ByteArray> {
        val ret = mutableListOf<ByteArray>()
        val md = MessageDigest.getInstance(this.digestMethod.javaName)

        // for each group of arity length in input
        // sort group
        // create hash over group
        for (groupNum in 0 until leafHashes.size step arity.toInt()) {
            val group = leafHashes.subList(groupNum, groupNum + arity).toMutableList()
            group.sortWith(ByteArrayComparator())
            group.forEach { hash -> md.update(hash) }
            ret.add(md.digest())
        }

        return ret
    }


    companion object {
        private val dummyBytes: ByteArray = byteArrayOf(1)

        /**
         * Basically resolving n in (a = b ^ n) through n = ceil(ln(nrLeafs)/ln(arity)), with nrLeafs = 1 being a special case (ln(1)=0)
         */
        fun calcTreeDepth(nrLeafs: Int, arity: Short): Int =
                nrLeafs.let { listSize -> if (listSize < 2) 2 else listSize }
                        .let { correctedSize -> 1 + ceil(ln(correctedSize.toFloat()) / ln(arity.toFloat())) }
                        .toInt()

        /**
         * Adds dummy values to the inputlist so its' size becomes a factor of arity and then sorts inputList
         */
        fun balanceAndSortInputList(hashAlgo: DigestMethod, arity: Short, leafHashes: MutableList<ByteArray>) {
            val dummyhash = MessageDigest.getInstance(hashAlgo.javaName).digest(dummyBytes)
            val leafSize = calculateBalancedTreeLeafSize(leafHashes.size, arity.toInt())
            for (i in leafHashes.size until leafSize) {
                leafHashes.add(dummyhash)
            }
            leafHashes.sortWith(ByteArrayComparator())
        }

        /**
         * Basically get first exponent of arity where arity ^ exponent >= nrLeafs.
         */
        fun calculateBalancedTreeLeafSize(nrLeafs: Int, arity: Int): Int =
                nrLeafs.let { oldSize -> if (oldSize < 2) 2 else oldSize }
                        .let { correctedSize -> ceil(ln(correctedSize.toDouble()) / ln(arity.toDouble())) }
                        .let { exponent -> arity.toDouble().pow(exponent) }
                        .toInt()
    }
}
