/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.rest.client.exception

import javax.ws.rs.core.Response

open class XmlErsRestClientException(message: String, response: Response?, cause: Throwable? = null) : Exception(message, cause) {
	val statusCode: Int? = response?.status
	val responseMsg: String? = if (response != null && response.hasEntity()) response.readEntity(String::class.java) else null

	override fun toString(): String {
		return "${super.toString()} -- statusCode: [$statusCode] -- responseMsg: [$responseMsg]"
	}
}