/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.C14nMethodEntity
import be.glever.xmlers.rest.api.generated.model.C14nMethod

class C14nMethodMapper {
	fun map(entity: C14nMethodEntity): C14nMethod =
			C14nMethod().apply {
				this.uri = entity.uri
			}


	fun map(c14nMethod: C14nMethod): C14nMethodEntity =
			C14nMethodEntity(uri = c14nMethod.uri)

}
