/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.ArchiveTimeStampSequenceType
import ietf.params.xml.ns.ers.ArchiveTimeStampSequenceType.ArchiveTimeStampChain
import ietf.params.xml.ns.ers.ObjectFactory

class ArchiveTimestampSequenceBuilder {

	private val archiveTimestampSequenceType: ArchiveTimeStampSequenceType

	init {
		this.archiveTimestampSequenceType = ObjectFactory().createArchiveTimeStampSequenceType()

	}

	fun withArhiveTimestampChains(chain: List<ArchiveTimeStampChain>): ArchiveTimestampSequenceBuilder {
		this.archiveTimestampSequenceType.archiveTimeStampChain.addAll(chain)
		return this
	}

	fun withArchiveTimestampChain(chain: ArchiveTimeStampChain): ArchiveTimestampSequenceBuilder {
		this.archiveTimestampSequenceType.archiveTimeStampChain.add(chain)
		return this
	}

	fun build(): ArchiveTimeStampSequenceType {
		return this.archiveTimestampSequenceType
	}
}
