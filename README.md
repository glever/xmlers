# XMLERS
This project is a java/kotlin implementation of the XML Evidence Record Syntax standard ( [RFC 6283](https://tools.ietf.org/html/rfc6283) ).
Please note that the code is still in the very early stages of development and should be considered unusable.

The actual motivation for this project is educational as the spec lends itself to learn different aspects of java 
security and PKI.

This page will be updated once a milestone has been reached.


## Planning
First milestone is planned to contain:
* Swagger-driven rest api
* Definition and configuration of an Archival Configuration
* Upload of ArchiveObjects and their DataObjects
* ~~Creation of initial timestamped EvidenceRecord XML~~
* Validation Client: Validation of the generated EvidenceRecord XML structure.

## Out of scope:
This functionality is foreseen for later milestones:
* Re-timestamping
	* Detection of new certificate in TSA which triggers re-timestamping
* Re-hashing
	* Support for (planned) obsoletion of hash algorithm and replacement with more secure algo.
* Ingestion of signed data
	* XMLERS spec has no explicit support for signed data, but has foreseen the SupportingInformation elements 
	in which EvidenceRecords for the PKI artifacts can be provided.
	* Management of these PKI artifacts (trust lists, revocation info, etc) will likely be spun off into separate 
	project.
* Multiple TSA's (paranoid mode): Seems relatively simple to add multiple TSA's in re-timestamping system
* Multiple Simultaneous Hash Algorithms (paranoid mode):
	* Unlike Re-Hashing where the new hash tree is based upon the previous (which may then never evolve),
	this mode allows parallel HashTrees, each with own re-timestamping/re-hashing lifecycles.
* HashTree grouping
	* To minimize TSA calls: During Re-timestamping / re-hashing, combine multiple smaller hash trees into one.
	This system would be very similar to re-hashing where hash of Previous EvidenceRecord is added to ArchiveObject. 
	But hashing algo would just not change.
* Watchdog program
	* Periodic validation of protected data (and their EvidenceRecords)
	* Periodic validation of TSA certificate


## Module overview
The project consists of following modules:
* xml: generated java classes from xsd defined in RFC 6283. 
* test-utils: specific utilities used only for tests
* domain: Implementation of logic defined in RFC 6283.  
* business: persistence logic
* rest-service: Jax-RS REST endpoint

*note that the main difference between domain model and xml model is that domain focuses on the entire hashtree, whilst 
the xml specifies the reduced tree (= proof)


## Development notes
Main technologies are kotlin and gradle, usually latest version. 
You can find gradle version in gradle/wrapper/gradle-wrapper.properties and kotlin version in build.gradle -> ext.kotlin_version

Standard build (compile + test):	`gradle build`  
run integration tests (requires docker to spin up postgres db): `gradle IT`



## Bottlenecks
### Memory
To achieve fairly performant sorting and index calculation the Hash Tree is represented in memory as an 
Array(=tree order) of Array(=list of digests) of ByteArray(=digest).  
For easier processing, dummy digest values are added to make the tree fully balanced, this means the tree leaf node size is always a power of 2(
current implementation generates a binary tree).  

This makes for fairly easy estimation of memory requirements:  
Creating a Hash tree for 10M digests will create a tree with order 24 and 16.777.216 leaf nodes (2^24).  
Since every level of the tree has a size of 1/2 of the level below, memory consumption is:   `hash size * ((leafSize x2 )-1) + _JAVA_OVERHEAD_`  
LeafSize can be calculated by `2^ceil(ln(nrDigests)/ln(2)))`  
Calculating the memory requirements of 10M sha-512 digests results in: ` 512 * (16.777.216 * 2 -1) + _JAVA_OVERHEAD_
= 17179868672 bit = 2.147.483.584 bytes + _JAVA_OVERHEAD_`.  
Calculating the java overhead is based on trial and error, but a safe assumption would be about x2 
(default heap size of 4.2gb is -barely- able to generate a hash tree for 10M sha-512 digests)  

### CPU
The cpu bottleneck is **by far** xml generation.  
Again for the example of 10M digests, a hash tree can be generated in +- 10s, but generating an EvidenceRecord xml for all 
reduced trees takes +-2:50m with stax (brought down from +-5:50m with jaxb)  
Initial tests for canonicalization using a c14n library show a big slowdown, so current plans are to hard code the 
creation of canonical xml, with unit tests for validation.  
Hash size has a smaller impact: +-3:30 for sha-512 compared to +-2:50 for sha-256
