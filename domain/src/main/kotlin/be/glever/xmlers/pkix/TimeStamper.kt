/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.pkix

import be.glever.xmlers.domain.model.DigestMethod
import eu.europa.esig.dss.DigestAlgorithm
import eu.europa.esig.dss.client.SecureRandomNonceSource
import eu.europa.esig.dss.client.http.commons.TimestampDataLoader
import eu.europa.esig.dss.client.tsp.OnlineTSPSource
import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.cms.DefaultCMSSignatureAlgorithmNameGenerator
import org.bouncycastle.cms.SignerInformationVerifier
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider
import org.bouncycastle.operator.bc.BcRSAContentVerifierProviderBuilder
import org.bouncycastle.tsp.TimeStampToken
import java.security.cert.*


class TimeStamper(val tsaUrl: String, val tsaCert: X509Certificate, val intermediateCerts: List<X509Certificate>, val rootCA: X509Certificate) {

    fun timestamp(digestMethod: DigestMethod, digest: ByteArray): TimeStampToken {
        val tspSource = OnlineTSPSource(tsaUrl).apply {
            setNonceSource(SecureRandomNonceSource())
            setDataLoader(TimestampDataLoader().apply {
                timeoutConnection = 1500000
                timeoutSocket = 1500000
            })
        }
        val timeStampResponse = tspSource.getTimeStampResponse(DigestAlgorithm.forOID(digestMethod.oid), digest)

        timeStampResponse.validate(
                SignerInformationVerifier(
                        DefaultCMSSignatureAlgorithmNameGenerator(),
                        DefaultSignatureAlgorithmIdentifierFinder(),
                        BcRSAContentVerifierProviderBuilder(DefaultDigestAlgorithmIdentifierFinder()).build(X509CertificateHolder(tsaCert.encoded)),
                        BcDigestCalculatorProvider()
                )
        )

        tsaCert.checkValidity()

//        println(Base64.getEncoder().encodeToString(intermediateChain.encoded))
//Check the chain
        val cf = CertificateFactory.getInstance("X.509")
        val cp = cf.generateCertPath(intermediateCerts)
//        val cpv = CertPathValidator.getInstance(CertPathValidator.getDefaultType(),"BC")
        val cpv = CertPathValidator.getInstance(CertPathValidator.getDefaultType())

        val params = PKIXParameters(setOf(TrustAnchor(rootCA, null)))
//        params.setRevocationEnabled(false)
        params.addCertPathChecker(cpv.revocationChecker as PKIXCertPathChecker)
        val pkixCertPathValidatorResult = cpv.validate(cp, params) as PKIXCertPathValidatorResult

        return timeStampResponse
    }
}
