This module houses a copy of the xmlers xsd schema as provided on https://tools.ietf.org/html/rfc6283#page-30, and xjc generated classes from it.

The classes are on source control as they don't change.
If ever needed: remove src/generated/xjc and run
`gradle jaxb`
