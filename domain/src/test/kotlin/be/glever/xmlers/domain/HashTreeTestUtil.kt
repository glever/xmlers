/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.domain

import be.glever.xmlers.domain.model.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.util.*

object HashTreeTestUtil {
    val DEFAULT_ALGO: DigestMethod = DigestMethod.SHA_512

    /**
     * @param nrLeaves
     * : Nr Elements to generate
     * @param ratioDataObjectGroups
     * : Every nth dataobject will become a Group. So 4 means element
     * 0,4,8,... is a group.
     * @param dataObjectsPerGroup
     * : How many dataobjects per group.
     * @return
     */
    @Throws(NoSuchAlgorithmException::class)
    fun generateArchiveObjects(nrLeaves: Int = 100,
                               ratioDataObjectGroups: Int = 1,
                               dataObjectsPerGroup: Int = 1,
                               algo: DigestMethod = DEFAULT_ALGO): List<ArchiveObject> {

        val md = MessageDigest.getInstance(algo.javaName)
        val archiveObjects = ArrayList<ArchiveObject>()
        for (i in 0 until nrLeaves) {
            val toAdd: ArchiveObject

            // create dataobjectgroup here and there
            if (i > 1 && i % ratioDataObjectGroups == 0) {
                val dataObjects = mutableListOf<DataObject>()
                for (j in 0 until dataObjectsPerGroup) {
                    // use random to bypass cool bug: first 10 elements where
                    // always sorted low to high in groups of 2.
                    // fun times debugging binary tree...
                    val oid = "ao: " + i + "-" + j + "_" + SecureRandom().nextInt()
                    dataObjects.add(DataObject(algo, md.digest(oid.toByteArray())))
                }
                toAdd = ArchiveObject(digestMethod = algo, dataObjects = dataObjects)

            } else {
                val oid = "do: " + i
                val dataObject = DataObject(algo, md.digest(oid.toByteArray()))
                toAdd = ArchiveObject(algo, dataObject)
            }

            archiveObjects.add(toAdd)
        }
        return archiveObjects
    }

    /**
     * Returns a string representation of all the hashes in the hashtree.
     *
     * @param ht
     * @return
     */
    fun printHashTree(ht: HashTree) {
        val hashes = ht.hashes
        printHashTreeChildren(hashes, ht.arity, 0, 0)
    }

    private fun printHashTreeChildren(hashes: List<List<ByteArray>>, arity: Short, depth: Int,
                                      parentIdx: Int) {

        for (i in 0 until depth) {
            print('\t')
        }
        val hashListIdx = hashes.size - (1 + depth)
        val hashToPrint = hashes[hashListIdx][parentIdx]
        println(Arrays.toString(hashToPrint))

        if (depth < hashes.size - 1) {
            val children = hashes[hashListIdx - 1]
            val startPos = parentIdx * arity
            var i = startPos
            var j = 0
            while (j++ < arity && i < children.size) {
                printHashTreeChildren(hashes, arity, depth + 1, i)
                i++
            }
        }
    }

    fun printReducedTreeChildren(rht: ReducedHashTree) {
        val hashes = rht.reducedHashes

        for (depth in 0 until rht.reducedHashes.size) {
            val curLevel = hashes[hashes.size - 1 - depth]

            for (curHash in curLevel) {
                for (i in 0 until depth) {
                    print('\t')
                }
                println(Arrays.toString(curHash))
            }
        }
    }

    @Throws(NoSuchAlgorithmException::class)
    fun generateDefaultArchiveObjects(nrArchiveObjects: Int): List<ArchiveObject> {
        return generateArchiveObjects(nrArchiveObjects, Integer.MAX_VALUE, 1, DEFAULT_ALGO)
    }

    fun generateHashTree(digestMethod: DigestMethod = DEFAULT_ALGO, byteArrays: List<ByteArray>) =
            HashTree(arity = 2, digestMethod = digestMethod, digests = byteArrays)


}
