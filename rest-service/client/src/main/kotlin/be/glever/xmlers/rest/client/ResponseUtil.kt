/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.rest.client

import be.glever.xmlers.rest.client.exception.XmlErsRestClientException
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.Response

fun <T> Response.readResponse(expectedStatus: Response.Status = Response.Status.OK, expectedType: GenericType<T>): T {
	assertStatus(expectedStatus)
	return try {
		readEntity(expectedType)
	} catch (e: Exception) {
		throw XmlErsRestClientException("Error reading entity from response", null, e)
	}
}

fun Response.assertStatus(expectedStatus: Response.Status = Response.Status.OK): Response =
		if (this.status != expectedStatus.statusCode) throw XmlErsRestClientException("Unexpected response status", this)
		else this
