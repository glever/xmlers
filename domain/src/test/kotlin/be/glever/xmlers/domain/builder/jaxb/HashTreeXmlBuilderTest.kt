/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.xml.generation

import be.glever.xmlers.domain.HashTreeTestUtil.DEFAULT_ALGO
import be.glever.xmlers.domain.HashTreeTestUtil.generateArchiveObjects
import be.glever.xmlers.domain.HashTreeTestUtil.generateDefaultArchiveObjects
import be.glever.xmlers.domain.model.HashTree
import be.glever.xmlers.domain.model.ReducedHashTree
import be.glever.xmlers.domain.util.ByteArrayComparator
import be.glever.xmlers.xml.mapper.HashTreeXmlMapper
import ietf.params.xml.ns.ers.HashTreeType
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class HashTreeXmlBuilderTest {
    private val defaultTreeOrder = 3.toShort()

    @Test
    @Throws(Exception::class)
    fun sequenceOrderIncreases() {
        // setup
        val archiveObjects = generateArchiveObjects(10, 3, 4, DEFAULT_ALGO)
        val hashTree = HashTree(arity = defaultTreeOrder, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

        // action
        for (ao in archiveObjects) {
            val reducedHashTree = hashTree.reduce(ao.getDigest())
            val hashTreeType = HashTreeXmlMapper.from(reducedHashTree)

            // test
            var seqOrder = 1
            for (seq in hashTreeType.sequence) {
                Assert.assertEquals(seqOrder++.toLong(), seq.order.toLong())
            }
        }

    }

    @Test
    @Throws(NoSuchAlgorithmException::class)
    fun sequencesShouldContainAllAndOnlyHashesOfReducedHashTree() {
        val archiveObjects = generateDefaultArchiveObjects(10)
        val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

        for (ao in archiveObjects) {
            val rht = ht.reduce(ao.getDigest())
            val htt = HashTreeXmlMapper.from(rht)

            assertXmlSequencesMatchReducedHashTreeHashes(rht, htt)

        }

    }

    private fun assertXmlSequencesMatchReducedHashTreeHashes(rht: ReducedHashTree, htt: HashTreeType) {
        // validate number of hashlists
        Assert.assertEquals(rht.reducedHashes.size, htt.sequence.size)

        for (i in 0 until rht.reducedHashes.size) {
            val rhtHashList = rht.reducedHashes[i]
            val httHashes = htt.sequence[i].digestValue

            // Validate same number of hashes in each list
            Assert.assertEquals(rhtHashList.size.toLong(), httHashes.size.toLong())

            for (rhtHash in rhtHashList) {
                assertHashListContains(httHashes, rhtHash)
            }
        }
    }

    private fun assertHashListContains(httHashes: List<ByteArray>, rhtHash: ByteArray) {
        val bac = ByteArrayComparator()
        for (httHash in httHashes) {
            if (bac.compare(httHash, rhtHash) == 0) {
                return
            }
        }

        Assert.fail("Sequence did not contain hash from ReducedHashTree")
    }

    companion object {
        private var defaultMd: MessageDigest = MessageDigest.getInstance(DEFAULT_ALGO.javaName)

        @BeforeClass
        @Throws(NoSuchAlgorithmException::class)
        fun beforeClass() {
            defaultMd = MessageDigest.getInstance(DEFAULT_ALGO.javaName)

        }
    }

}
