package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.converter.X509CertificateConverter
import be.glever.xmlers.business.jpa.entity.TsaConfigEntity
import be.glever.xmlers.rest.api.generated.model.TsaConfig

class TsaConfigMapper {

    // Not sure if I'm keeping this: byte[] gets converted to x509cert when mapping to entity, then back to byte[] when persisting in db
    // same double mapping when returning tsaconfig from db
    // but I like the cert validation it brings.

    fun map(tsaConfig: TsaConfig): TsaConfigEntity = TsaConfigEntity(
            id = null,
            tsaUrl = tsaConfig.tsaUrl,
            tsaCert = X509CertificateConverter().convertToEntityAttribute(tsaConfig.tsaCert)
    )

    fun map(entity: TsaConfigEntity): TsaConfig =
            TsaConfig().apply {
                tsaUrl = entity.tsaUrl
                tsaCert = X509CertificateConverter().convertToDatabaseColumn(entity.tsaCert)
            }
}
