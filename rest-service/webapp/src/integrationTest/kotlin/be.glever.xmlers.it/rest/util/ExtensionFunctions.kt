/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.it.rest.util

import be.glever.xmlers.rest.api.generated.model.*
import java.util.*

/**
 * Calls each functions in [blocks] on [o1] and [o2] with `this` value as its receiver and returns the `==` comparison as a result.
 */
fun <T> _isEqual(o1: T?, o2: T?, vararg blocks: T.() -> Any?): Boolean =
        blocks.all { block ->
            when {
                o1 != null && o2 != null -> (block(o1) == block(o2))
                o1 != null || o2 != null -> false
                else -> true
            }
        }

fun C14nMethod._isEqual(other: C14nMethod?): Boolean =
        _isEqual(this, other, { uri })

fun DigestMethod._isEqual(other: DigestMethod?): Boolean =
        _isEqual(this, other, { javaName }, { oid }, { uri })

fun Period._isEqual(other: Period?): Boolean =
        when (other) {
            null -> (this.validFrom == null && this.validUntil == null)
            else -> _isEqual(this, other, { validFrom }, { validUntil })
        }

fun DigestMethodPolicy._isEqual(other: DigestMethodPolicy?): Boolean =
        when (other) {
            null -> false
            else -> _isEqual(this, other, { digestMethodOid })
                    && (this.period?._isEqual(other.period ?: null) ?: (this.period == other.period))
        }


fun ArchivalConfig._isEqual(other: ArchivalConfig?): Boolean =
        when (other) {
            null -> false
            else -> _isEqual(this, other, { uniqueId })
                    && this.c14nMethod._isEqual(other.c14nMethod)
                    && this.digestMethodPolicies.size == other.digestMethodPolicies.size
                    && this.tsaConfig._isEqual(other.tsaConfig)
                    && this.digestMethodPolicies.all { thisPolicy ->
                other.digestMethodPolicies?.all { otherPolicy -> thisPolicy._isEqual(otherPolicy) } ?: false
            }
        }

fun TsaConfig._isEqual(other: TsaConfig?): Boolean =
        when (other) {
            null -> false
            else -> _isEqual(this, other, { tsaUrl })
                    && Arrays.equals(this.tsaCert, other.tsaCert)
        }
