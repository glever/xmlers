/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix

import java.io.IOException
import java.net.URISyntaxException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.Base64

import org.bouncycastle.cert.X509CertificateHolder
import org.bouncycastle.tsp.TSPAlgorithms
import org.bouncycastle.tsp.TSPException
import org.bouncycastle.tsp.TimeStampResponse
import org.bouncycastle.tsp.TimeStampToken
import org.bouncycastle.tsp.TimeStampTokenInfo
import org.bouncycastle.util.Selector
import org.junit.Ignore
import org.junit.Test

import pkix.tsp.TimeStampCreator
import pkix.tsp.ValidatedTimestamp

class TimeStampCreatorTest {
    private val timestampResponseB64 = "MIISpTADAgEAMIISnAYJKoZIhvcNAQcCoIISjTCCEokCAQMxCzAJBgUrDgMCGgUAMIHgBgsqhkiG9w0BCRABBKCB0ASBzTCBygIBAQYFYDgJAwEwMTANBglghkgBZQMEAgEFAAQgeGdcwXYIE3LEOrqz6p+3DHQ4HrAtxuk/ttRNFh2m7rMCBzhBMDJBNDMYDzIwMTcwMjI3MjAxNTU1WgII0IC1ZlSQs5mgZ6RlMGMxCzAJBgNVBAYTAkJFMQ0wCwYDVQQFEwQyMDE0MSMwIQYDVQQKExpCZWxnaXVtIEZlZGVyYWwgR292ZXJubWVudDEgMB4GA1UEAxMXVGltZSBTdGFtcGluZyBBdXRob3JpdHmggg+CMIIDdzCCAl+gAwIBAgIEAgAAuTANBgkqhkiG9w0BAQUFADBaMQswCQYDVQQGEwJJRTESMBAGA1UEChMJQmFsdGltb3JlMRMwEQYDVQQLEwpDeWJlclRydXN0MSIwIAYDVQQDExlCYWx0aW1vcmUgQ3liZXJUcnVzdCBSb290MB4XDTAwMDUxMjE4NDYwMFoXDTI1MDUxMjIzNTkwMFowWjELMAkGA1UEBhMCSUUxEjAQBgNVBAoTCUJhbHRpbW9yZTETMBEGA1UECxMKQ3liZXJUcnVzdDEiMCAGA1UEAxMZQmFsdGltb3JlIEN5YmVyVHJ1c3QgUm9vdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKMEuyKrmD1X6CZymrV51Cni4eiVgLGw41uOKymaZN+hXe2wCQVt2yguzmKiYv60iNoS6zjrIZ3AQSsBUnuId9Mcj8e6uYi1agnnc+gRQKfRzMpijS3ljwumUNKoUMMo6vWrJYeKmpYcqWe4PwzV9/lSEy/CG9VwcPCPwBLKBsua4dnKM3p31vjsufFoREJIE9LAwqSuXmD+tqYF/LTdB1kC1FkYmGP1pWPgkAx9XbIGevOF6uvUA65ehD5f/xXtabz5OTZydc93Uk3zyZAsuT3lySNTPx8kmCFcB5kpvcY67Oduhjprl3RjM71oGDHweI12v/yejl0qhqdNkNwnGjkCAwEAAaNFMEMwHQYDVR0OBBYEFOWdWTCCR1jMrPoIVDaGezq1BE3wMBIGA1UdEwEB/wQIMAYBAf8CAQMwDgYDVR0PAQH/BAQDAgEGMA0GCSqGSIb3DQEBBQUAA4IBAQCFDF2O5G9RaEIFoN27TyclhAO992T9Ldcw46QQF+vaKSm2eT929hkTI7gQCvlYpNRhcL0EYWoSihfVCr3FvDB81ukMJY2GQE/szKN+OMY3EU/t3WgxjkzSswF07r51XgdIGn9w/xZchMB5hbgF/X++ZRGjD8ACtPhSNzkE1akxehi/oCr0Epn3o0WC4zxe9Z2etciefC7IpJ5OCBRLbf1wbWsaY71k5h+3zvDyny67G7fyUIhzksLi4xaNmjICq44Y3ekQEe5+NauQrz4wlHrQMz2nZQ/1/I6eYs9HRCwBXbsdtTLSR9I4LtD+gdwyah617jzV/OeBHRnDJELqYzmpMIID7jCCAtagAwIBAgILBAAAAAABQaHhNLowDQYJKoZIhvcNAQEFBQAwOzEYMBYGA1UEChMPQ3liZXJ0cnVzdCwgSW5jMR8wHQYDVQQDExZDeWJlcnRydXN0IEdsb2JhbCBSb290MB4XDTEzMTAxMDExMDAwMFoXDTI1MDUxMjIyNTkwMFowKDELMAkGA1UEBhMCQkUxGTAXBgNVBAMTEEJlbGdpdW0gUm9vdCBDQTIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGc0Iekv91D4u/dIanP+2yGC0tl/mp+5hKI9/VjeCMxjIcFi0HQtYttm4vp/X8xoWCXZXetSIAzKhTQJ+vCjWiesfhyvdgEGcOp1BzF1OVnyKfDF1vtkG8jOvaHUa9p5S/8xrL1PuNDh4zyJbX7IxT3pMeNB6KUHEmWLRdwoiJ2mDSiSE/3tcBGDZu4SxwAwRl75gGK10eYtxVa/5mZCFf+C/h15spr2/Nn6oMRtGIxZ2ilZ+sPxWu8mHf78NrmiLYLHH9WB3sAKQ4IJVax9U3Y7pPm6r+VkYuY9kLI9WK+rUj8IkboRQNJtpB8DiPMO0FJnnBS06huH5VUjppRoR1AgMBAAGjggEEMIIBADAOBgNVHQ8BAf8EBAMCAQYwEgYDVR0TAQH/BAgwBgEB/wIBATBQBgNVHSAESTBHMEUGCisGAQQBsT4BZAEwNzA1BggrBgEFBQcCARYpaHR0cDovL2N5YmVydHJ1c3Qub21uaXJvb3QuY29tL3JlcG9zaXRvcnkwHQYDVR0OBBYEFIWK6/TFu74OWQOU3taAARXjEJw5MDUGA1UdHwQuMCwwKqAooCaGJGh0dHA6Ly9jcmwub21uaXJvb3QuY29tL2N0Z2xvYmFsLmNybDARBglghkgBhvhCAQEEBAMCAAcwHwYDVR0jBBgwFoAUtgh7DXrMrCBMhlYyXs+rboUtcFcwDQYJKoZIhvcNAQEFBQADggEBALLLOUcpFHXrT8gK9htqXI8dV3LlSAooOqLkn+yRRxt/zS9Y0X0opocf56Kjdu+c2dgw6Ph3xE/ytMT5cu/60jT17BTk2MFkQhoAJbM/KIGmvu4ISDGdeobiBtSeiyzRb9JR6JSuuM3LvQp1n0fhsA5HlibT5rFrKi7Oi1luDbc4eAp09nPhAdcgUkRU9o/aAJLAJho3Zu9uSbw5yHW3PRGnmfSO67mwsnSDVswudPrZEkCnSHq/jwOBXAWCYVu5bru3rCdojd5qCTn/WyqbZdsgLAPR5Vmf/uG3d5HxTO1LLX1Zyp9iANuG32+nFusi89shA1GPDKWacEm0ASd8iaUwggQFMIIC7aADAgECAgsEAAAAAAFB5SqSbjANBgkqhkiG9w0BAQUFADAoMQswCQYDVQQGEwJCRTEZMBcGA1UEAxMQQmVsZ2l1bSBSb290IENBMjAeFw0xMzEwMjMxMTAwMDBaFw0xOTAxMjMxMTAwMDBaMGMxCzAJBgNVBAYTAkJFMQ0wCwYDVQQFEwQyMDE0MSMwIQYDVQQKExpCZWxnaXVtIEZlZGVyYWwgR292ZXJubWVudDEgMB4GA1UEAxMXVGltZSBTdGFtcGluZyBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC66vNxRn3GPQfGPynMs9J2h3FuX2FPOa9UWNXCQ+U78Ioi24y/mN1fvy+QnLEOgIjOVrJ0LyJfospW9spZLwMdThD29SD2OXI+sbekzz/u4woSJUmWjVDsb+PfJxUh8pbkmAsRSSQDumu2PG6ZpRo22hKgtdVsdT7iohUu4E6rt1jBk3Sh+1gzpxQSyWoOLw5IBlXI9i9Q0juSVxBGHbx264zJReE7O7id4HPj25IDlzecAgcnEkf7OwgQhn+u/b6iIWw3c1HLya7ywfA5HVt/zVI/vcLoayDHRyNvgloS8B0RdrI7HliMiSnyn2sXSwwduKZjLpeGDq4DM5uMWtC3AgMBAAGjgfQwgfEwDgYDVR0PAQH/BAQDAgbAMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMIMEMGA1UdIAQ8MDowOAYGYDgJAQEFMC4wLAYIKwYBBQUHAgEWIGh0dHA6Ly9yZXBvc2l0b3J5LnBraS5iZWxnaXVtLmJlMB0GA1UdDgQWBBSHL7GXyX9tS19ih+sUfE8jSEtEkjA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLnBraS5iZWxnaXVtLmJlL2JlbGdpdW0yLmNybDAJBgNVHRMEAjAAMB8GA1UdIwQYMBaAFIWK6/TFu74OWQOU3taAARXjEJw5MA0GCSqGSIb3DQEBBQUAA4IBAQAq2eD8EwnAniagM92HTAlsxwO6mDsKvc9uguo2JW0ke2iO2DJrn4sN8ikNoG3Tnh6z84pXd813jQtTV40GPR+VTRI2DHBGqpbAGN2kGP1cJmwuxOUKMsMbscmrq2jIB9diMcGPcqV1RBLaDjB/efF5CANstOzbwyGklY30UKTYMnrdo8RsBn8cDbQ08Bvzooz8uE7YCWQPCGk/nXSKHV0FaOLA1GuSubEidb48JCYEZHrSupGzl/ydqizJHxREPP/AeJDgCuf8etf8pVnYcwPym3Uhlj7ku4i4DZ+fSE0JDKsZmhrcc2ogg/nN9q/DROn6MMos01KyLkvwI/XbyWS6MIIECDCCAvCgAwIBAgIEByczJTANBgkqhkiG9w0BAQUFADBaMQswCQYDVQQGEwJJRTESMBAGA1UEChMJQmFsdGltb3JlMRMwEQYDVQQLEwpDeWJlclRydXN0MSIwIAYDVQQDExlCYWx0aW1vcmUgQ3liZXJUcnVzdCBSb290MB4XDTEwMDgxODE5MTE1MloXDTIwMDgxODE5MTEwNlowOzEYMBYGA1UEChMPQ3liZXJ0cnVzdCwgSW5jMR8wHQYDVQQDExZDeWJlcnRydXN0IEdsb2JhbCBSb290MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+Mi8vRRQZhP/8NN57CPytxrHjoXxEnOmGaoQ25yiZXRadz5RfVb23CO21O1fWLE3TdVJDm71aofW0ozSJ8bi/zafmGWgE07GKmSb1ZASzxQG9Dvj1Ci+6A74q05IlG2OlTEQXO2iLb3VOm2yHLtgwEZLAfVJrn5GitB0jaEMAs7u/OePuGtm839EAL9mJRQr3RAwHQeWP032a7iPt3sMpTjr3kfb1V05/Iin89cqdPHoWqI7n1C6poxFNcJQZZXcY4Lv3b93TZxiyWNzFtApD0mpSPCzqrdsxacwOUBdrsTiXSZT8M4cIwhhqJQZugRiQOwfOHB3EgZxpzAYXSUnpQIDAQABo4H0MIHxMBIGA1UdEwEB/wQIMAYBAf8CAQIwSgYDVR0gBEMwQTA/BgRVHSAAMDcwNQYIKwYBBQUHAgEWKWh0dHA6Ly9jeWJlcnRydXN0Lm9tbmlyb290LmNvbS9yZXBvc2l0b3J5MAsGA1UdDwQEAwIBBjAfBgNVHSMEGDAWgBTlnVkwgkdYzKz6CFQ2hns6tQRN8DBCBgNVHR8EOzA5MDegNaAzhjFodHRwOi8vY2RwMS5wdWJsaWMtdHJ1c3QuY29tL0NSTC9PbW5pcm9vdDIwMjUuY3JsMB0GA1UdDgQWBBS2CHsNesysIEyGVjJez6tuhS1wVzANBgkqhkiG9w0BAQUFAAOCAQEAE26SCV2/KTgw6nYB6hfb6KI2EKycTynAOao331vsgBs499ZmrTA8FCGqxfg7UdQ6RQnWCZ0ybp2lzq8NL4rmO/SNtKmSpjYNNfVERR5OeLnCOTmqpJQpOEivmYN9lPFdeIgmglCmGcHEqvWRlo8SV7uyPvii0MxHmcoBV5vdW8UqnOP85ryHb6ON/d3XeYrzTx9f7eLq8VWPH+b145oMTA6dO0vJOyHtB0i1xoU8hAsrTm6PbqjnuSmToOTuh0lx3ne3Xz1UpqoGzUvg2KvLsQ80qiTYdDnFwQTPX2TJkgrrqh0f5nF32+sVXvGYDnhZ72qv4U9/OAar5O9EvaUHvTGCAgwwggIIAgEBMDcwKDELMAkGA1UEBhMCQkUxGTAXBgNVBAMTEEJlbGdpdW0gUm9vdCBDQTICCwQAAAAAAUHlKpJuMAkGBSsOAwIaBQCggaswGgYJKoZIhvcNAQkDMQ0GCyqGSIb3DQEJEAEEMCMGCSqGSIb3DQEJBDEWBBSuX4QAC6tFBdD6toiPFG4S4aqZoDBoBgsqhkiG9w0BCRACDDFZMFcwVTBTBBSeKOWylXPYPT0KnCOyRzM4sjYXRjA7MCykKjAoMQswCQYDVQQGEwJCRTEZMBcGA1UEAxMQQmVsZ2l1bSBSb290IENBMgILBAAAAAABQeUqkm4wDQYJKoZIhvcNAQEBBQAEggEAHKwlyj4RepFMT/xOROtMepRJXSldHkNf3JtnD47PewXdegudQPMPkY/p+KtinNSO7uD2oFoNVQMS0c+kkCbvzyU4Q0kEG+v8bqHrM4IhEUNLV5nxxVLKn62uIa/KkFB3rYdBjJ+gjyKXMPsVmyAKLZPAGexLclG3RhwFDvwZwl2vL0ag7KcJYjlt7X3H5F+zR5XKzEaUy95ioDxNRfHt1/6sofrX+4f17BznsnAgMyDLrhBW0d2uEJKKZEGRzKlXkeFgRe2bYEdaQU+42hxwVSHDQc722FAtpDCY1LPqLMzAQn+326k0IAM0CZfNR+BZE53dHNeCva/lvxVbCsZraA=="

    @Test
    @Ignore
    @Throws(NoSuchAlgorithmException::class, TSPException::class, IOException::class, URISyntaxException::class)
    fun trustedCertPathShouldReturnValidatedTimestamp() {
        val md = MessageDigest.getInstance("SHA-256")
        val imprint = md.digest("joe".toByteArray())
        val creator = TimeStampCreator(null, null)
        val validatedTimestamp = creator.create(imprint, TSPAlgorithms.SHA256,
                "KAPOT http://tsa.belgium.be/connect")
        val encodedTimestampResponse = validatedTimestamp.timestampResponse.encoded
        println(Base64.getEncoder().encodeToString(encodedTimestampResponse))
    }

    @Test
    @Ignore
    @Throws(IOException::class, TSPException::class)
    fun playAroundWithGeneratedTimestamp() {
        val tsr = TimeStampResponse(Base64.getDecoder().decode(timestampResponseB64))
        sysout(tsr.toString())
        val timeStampToken = tsr.timeStampToken
        sysout(timeStampToken.crLs)
        sysout(timeStampToken.attributeCertificates)
        sysout(timeStampToken.certificates)
        val timeStampInfo = timeStampToken.timeStampInfo
        sysout(timeStampInfo.nonce)
        sysout(timeStampInfo.genTime)
        sysout(timeStampInfo.tsa)
        sysout("done")

        val allMatch = object : Selector<X509CertificateHolder> {
            override fun match(obj: X509CertificateHolder): Boolean {
                return true
            }

            override fun clone(): Any {
                return Any()
            }
        }
        for (ch in timeStampToken.certificates.getMatches(allMatch)) {
            val certH = ch as X509CertificateHolder
            sysout(Base64.getEncoder().encodeToString(certH.encoded))
        }

    }

    private fun sysout(s: Any) {
        println(s)
    }
}
