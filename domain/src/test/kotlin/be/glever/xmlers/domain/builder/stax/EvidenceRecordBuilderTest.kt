package be.glever.xmlers.domain.builder.stax

import be.glever.xmlers.AbstractPkiEnabledTest
import be.glever.xmlers.domain.HashTreeTestUtil
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.util.ArchiveTimestampBuilder
import be.glever.xmlers.xml.generation.stax.EvidenceRecordBuilder
import org.apache.xml.security.c14n.Canonicalizer
import org.junit.Assert.assertEquals
import org.junit.Test
import java.io.ByteArrayInputStream
import java.io.StringReader
import java.io.StringWriter
import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.xml.XMLConstants
import javax.xml.bind.JAXBException
import javax.xml.transform.OutputKeys
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import javax.xml.validation.Validator


class EvidenceRecordBuilderTest : AbstractPkiEnabledTest() {


    val digestMethod = DigestMethod.SHA_512
    val messageDigest = MessageDigest.getInstance(digestMethod.javaName)
    val validator: Validator;

    init {
        val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
        val schema = factory.newSchema(StreamSource(this::class.java.getResourceAsStream("/xsd/xmlers.xsd")))
        validator = schema.newValidator()
        org.apache.xml.security.Init.init()
    }

    @Test
    @Throws(JAXBException::class, NoSuchAlgorithmException::class)
//    @Ignore
    fun testEvidenceRecordBuilder() {
        val start = System.currentTimeMillis()

//        val digests = (1..10000000).map { toString() }.map {
        val digests = (1..2).map { toString() }.map {
            messageDigest.digest(it.toByteArray(Charset.defaultCharset()))
        }
        val ht = HashTreeTestUtil.generateHashTree(digestMethod = digestMethod, byteArrays = digests)
        val archiveTimestamp = ArchiveTimestampBuilder(
                timestamper = timestamper,
                digestMethod = digestMethod
        ).generateArchiveTimestamp(ht)


        println("time: ${System.currentTimeMillis() - start}")
        digests.forEach { digest ->
            //            if (index % 100000 == 0) println(index)
            val reducedHashTree = ht.reduce(digest)
            val xml = EvidenceRecordBuilder().fromInitialTimeStamp(reducedHashTree, archiveTimestamp, digestMethod)
//            validate(xml)

        }

    }

    @Test
    fun shouldCreateValidEvidenceRecord() {
        val archiveTimestamp = ArchiveTimestampBuilder(timestamper = timestamper, digestMethod = digestMethod)
                .generateArchiveTimestamp(digest = messageDigest.digest(UUID.randomUUID().toString().toByteArray()))

        val evidenceRecordXml = EvidenceRecordBuilder().fromInitialTimeStamp(archiveTimestamp = archiveTimestamp, digestMethodType = digestMethod, reducedHashTree = null)

        validate(evidenceRecordXml)
    }

    fun validate(xml: String) {
//        println(xml)

        this.validator.validate(StreamSource(ByteArrayInputStream(xml.toByteArray())))


        assertCanonicalized(xml)
    }

    /**
     * Will fail when multithreading
     */
    private fun assertCanonicalized(xml: String) {
        val canonicalized = Canonicalizer.getInstance(Canonicalizer.ALGO_ID_C14N_OMIT_COMMENTS).canonicalize(xml.toByteArray())
        assertEquals(xml, String(canonicalized))
    }

    fun pretty(xml: String): String {
        val t = TransformerFactory.newInstance().newTransformer()
        t.setOutputProperty(OutputKeys.INDENT, "yes")
        t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2")
        val out = StringWriter()
        t.transform(StreamSource(StringReader(xml)), StreamResult(out))
        return out.toString()
    }
}
