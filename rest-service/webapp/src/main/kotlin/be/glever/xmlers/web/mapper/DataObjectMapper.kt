/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.DataObjectEntity
import be.glever.xmlers.rest.api.generated.model.DataObject
import be.glever.xmlers.service.DigestMethodService

class DataObjectMapper(private val digestMethodService: DigestMethodService) {

	fun map(entity: DataObjectEntity): DataObject =
			DataObject().apply {
				this.uniqueId = entity.uniqueId
				this.digests = entity.digests.map { DataObjectDigestMapper(digestMethodService).map(it) }
			}

	fun map(dataObject: DataObject): DataObjectEntity =
			DataObjectEntity(
					id = null,
					digests = dataObject.digests?.map { DataObjectDigestMapper(digestMethodService).map(it) }?.toMutableSet()
							?: mutableSetOf(),
					uniqueId = dataObject.uniqueId,
					archiveObjectEntity = null
			).apply {
				digests.forEach { it.dataObject = this }
			}
}
