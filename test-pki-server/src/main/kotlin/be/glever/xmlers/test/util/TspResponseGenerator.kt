package be.glever.xmlers.test.util

import eu.europa.esig.dss.DSSRevocationUtils
import eu.europa.esig.dss.DigestAlgorithm
import eu.europa.esig.dss.EncryptionAlgorithm
import eu.europa.esig.dss.SignatureAlgorithm
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.cms.AttributeTable
import org.bouncycastle.asn1.cms.Time
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers
import org.bouncycastle.cert.jcajce.JcaCertStore
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder
import org.bouncycastle.cms.DefaultSignedAttributeTableGenerator
import org.bouncycastle.cms.SignerInfoGeneratorBuilder
import org.bouncycastle.cms.SimpleAttributeTableGenerator
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.ContentSigner
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.tsp.*
import java.math.BigInteger
import java.security.PrivateKey
import java.security.Security
import java.security.cert.X509Certificate
import java.util.*

class TspResponseGenerator(val privKey: PrivateKey, val privCert: X509Certificate) {

    companion object {
        init {
            Security.addProvider(BouncyCastleProvider());
        }
    }


    fun generateResponse(tsRequest: TimeStampRequest): TimeStampResponse {
        val signatureAlgo: String = getSignatureAlgorithm(privKey, tsRequest)
        val contentSigner: ContentSigner = JcaContentSignerBuilder(signatureAlgo).build(privKey)

        val signedAtts = AttributeTable(Hashtable<ASN1ObjectIdentifier, Any>())
                .add(PKCSObjectIdentifiers.pkcs_9_at_signingTime, Time(Date(), Locale.getDefault()))

        val signedAttGenerator = DefaultSignedAttributeTableGenerator(signedAtts)
        val unsignedAttGenerator = SimpleAttributeTableGenerator(AttributeTable(Hashtable<ASN1ObjectIdentifier, Any>()))

        val sig = SignerInfoGeneratorBuilder(BcDigestCalculatorProvider()).apply {
            setSignedAttributeGenerator(signedAttGenerator)
            setUnsignedAttributeGenerator(unsignedAttGenerator)
        }.build(contentSigner, JcaX509CertificateHolder(privCert))

        val sha1DigestCalculator = DSSRevocationUtils.getSHA1DigestCalculator()

        val dummyPolicy = ASN1ObjectIdentifier("1.2.3")
        val tokenGenerator = TimeStampTokenGenerator(sig, sha1DigestCalculator, dummyPolicy)
        tokenGenerator.addCertificates(JcaCertStore(Collections.singleton(privCert)))
        val generate = TimeStampResponseGenerator(tokenGenerator, TSPAlgorithms.ALLOWED)
                .generate(tsRequest, BigInteger.ONE, Date())
        return generate
    }

    private fun getSignatureAlgorithm(privKey: PrivateKey, tsRequest: TimeStampRequest): String =
            SignatureAlgorithm.getAlgorithm(
                    EncryptionAlgorithm.forName(privKey.algorithm),
                    DigestAlgorithm.forOID(tsRequest.messageImprintAlgOID.id))
                    .jceId


}

