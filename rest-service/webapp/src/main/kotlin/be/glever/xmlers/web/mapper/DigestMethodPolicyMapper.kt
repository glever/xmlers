/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.DigestMethodPolicyEntity
import be.glever.xmlers.rest.api.generated.model.DigestMethodPolicy
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException

class DigestMethodPolicyMapper(val digestMethodService: DigestMethodService) {

	fun map(entity: DigestMethodPolicyEntity): DigestMethodPolicy =
			DigestMethodPolicy().apply {
				this.digestMethodOid = entity.digestMethod.oid
				this.period = entity.period.let { PeriodEmbeddableEntityMapper().map(it) }
			}

	fun map(digestMethodPolicy: DigestMethodPolicy): DigestMethodPolicyEntity =
			DigestMethodPolicyEntity(
					digestMethod = digestMethodService.findByOid(digestMethodPolicy.digestMethodOid)
							?: throw XmlErsBusinessException("DigestMethod with oid ${digestMethodPolicy.digestMethodOid} not found"),
					period = PeriodEmbeddableEntityMapper().map(digestMethodPolicy.period)
			)
}
