/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain

import be.glever.xmlers.domain.model.*
import be.glever.xmlers.domain.HashTreeTestUtil.DEFAULT_ALGO
import be.glever.xmlers.domain.HashTreeTestUtil.generateArchiveObjects
import be.glever.xmlers.domain.HashTreeTestUtil.generateDefaultArchiveObjects
import be.glever.xmlers.domain.util.ByteArrayComparator
import be.glever.xmlers.domain.util.MessageDigestUtil
import org.junit.*
import org.junit.Assert.fail
import org.junit.rules.ExpectedException
import java.io.IOException
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.util.*
import javax.xml.bind.JAXBException

class HashTreeTest {

	private var expection = ExpectedException.none()
	@Rule
	fun rules() = expection


	companion object {

		private var defaultMd: MessageDigest = MessageDigest.getInstance(DEFAULT_ALGO.javaName)

		@BeforeClass
		@Throws(NoSuchAlgorithmException::class)
		fun beforeClass() {
			defaultMd = MessageDigest.getInstance(DEFAULT_ALGO.javaName)
		}
	}

	@Test
	fun arityLowerThan2ShouldThrowIllegalArgumentException() {
		try {
            HashTree(arity = 1, digestMethod = DigestMethod.SHA_512, archiveObjects = listOf())
		} catch (e: IllegalArgumentException) {
			Assert.assertEquals("Minimum tree arity is 2", e.message)
		}

	}

	@Test
	fun reducedTreeOfUnknownHashShouldYieldIllegalArgumentException() {
		val digestMethod = DEFAULT_ALGO
		val knownHash = ArchiveObject(digestMethod, DataObject(digestMethod, "known".toByteArray()))
		val unknownHash = ArchiveObject(digestMethod, DataObject(digestMethod, "unknown".toByteArray()))

		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = listOf(knownHash))
		ht.reduce(knownHash.getDigest())

		expection.expect(IllegalArgumentException::class.java)
		ht.reduce(unknownHash.getDigest())
	}

	/**
	 * https://tools.ietf.org/html/rfc6283#section-3.2.1 step 3
	 *
	 * @throws Throwable
	 */
	@Test
	@Throws(Throwable::class)
	fun parentHashShouldBeCalculatedOnBinarySortedChildren() {
		val mdi = DEFAULT_ALGO
		val ao1 = ArchiveObject(mdi, DataObject(mdi, Base64.getDecoder().decode(
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa==")))
		val ao2 = ArchiveObject(mdi, DataObject(mdi, Base64.getDecoder().decode(
                "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb==")))

		val md = MessageDigest.getInstance(DEFAULT_ALGO.javaName)
		md.update(ao1.getDigest())
		md.update(ao2.getDigest())
		val expectedDigest = md.digest()

		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = listOf(ao1, ao2))
		Assert.assertArrayEquals(expectedDigest, ht.getRootHash())

		val ht2 = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = listOf(ao2, ao1))
		Assert.assertArrayEquals(expectedDigest, ht2.getRootHash())
	}

	@Test
	@Throws(NoSuchAlgorithmException::class)
	fun emptyLeafHashesShouldThrowIllegalArgumentException() {
		expection.expect(IllegalArgumentException::class.java)
        HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = listOf())
	}

	@Test
	fun treeDepthShouldBeBasedOnFactorOfTreeArity() {
		Assert.assertEquals(2, HashTree.calcTreeDepth(1, 2).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(2, 2).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(3, 2).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(4, 3).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(5, 2).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(8, 2).toLong())
		Assert.assertEquals(5, HashTree.calcTreeDepth(9, 2).toLong())
		Assert.assertEquals(5, HashTree.calcTreeDepth(16, 2).toLong())
		Assert.assertEquals(6, HashTree.calcTreeDepth(17, 2).toLong())
		Assert.assertEquals(6, HashTree.calcTreeDepth(32, 2).toLong())
		Assert.assertEquals(7, HashTree.calcTreeDepth(33, 2).toLong())

		Assert.assertEquals(2, HashTree.calcTreeDepth(1, 3).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(2, 3).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(3, 3).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(4, 3).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(9, 3).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(10, 3).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(27, 3).toLong())
		Assert.assertEquals(5, HashTree.calcTreeDepth(28, 3).toLong())

		Assert.assertEquals(2, HashTree.calcTreeDepth(1, 4).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(4, 4).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(5, 4).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(16, 4).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(17, 4).toLong())

		Assert.assertEquals(2, HashTree.calcTreeDepth(1, 100).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(100, 100).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(101, 100).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10000, 100).toLong())
	}

	@Test // bugfix
	fun calculateTreeDepthShouldNotHaveInfiniteLoop() {
		Assert.assertEquals(5, HashTree.calcTreeDepth(10, 2).toLong())
		Assert.assertEquals(4, HashTree.calcTreeDepth(10, 3).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 4).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 5).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 6).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 7).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 8).toLong())
		Assert.assertEquals(3, HashTree.calcTreeDepth(10, 9).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(10, 10).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(10, 11).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(10, 15).toLong())
		Assert.assertEquals(2, HashTree.calcTreeDepth(10, 10000).toLong())
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class, JAXBException::class)
	fun parentNodesLengthShouldBeChildNodesLengthDividedByTreeArity() {
		val arity = 5.toShort()
		val archiveObjects = generateArchiveObjects(10, 3, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = arity, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		val hashes = ht.hashes

		for (i in 0..hashes.size - 1 - 1) {
			val curLen = hashes[i].size
			val nextLen = hashes[i + 1].size
			val expectedLen = curLen / arity + 1
			val expected = expectedLen >= nextLen
			Assert.assertTrue(expected)
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class)
	fun reducedHashTreeShouldHaveSameHashAlgoAsHashTree() {
		val mdi = DEFAULT_ALGO

		val md = MessageDigest.getInstance(mdi.javaName)
		val hash = md.digest("s".toByteArray())
		val ao = ArchiveObject(mdi, DataObject(mdi, hash))
		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = listOf(ao))
		val rht = ht.reduce(ao.getDigest())

		Assert.assertEquals(mdi, rht.digestMethod)
		Assert.assertEquals(ht.digestMethod, rht.digestMethod)
	}

	@Test
	fun validateListSizeForBalancedTreeCalculation() {
		Assert.assertEquals(2, HashTree.calculateBalancedTreeLeafSize(1, 2))
		Assert.assertEquals(2, HashTree.calculateBalancedTreeLeafSize(2, 2))
		Assert.assertEquals(4, HashTree.calculateBalancedTreeLeafSize(3, 4))
		Assert.assertEquals(4, HashTree.calculateBalancedTreeLeafSize(4, 4))
		Assert.assertEquals(8, HashTree.calculateBalancedTreeLeafSize(5, 8))
		Assert.assertEquals(8, HashTree.calculateBalancedTreeLeafSize(8, 8))
		Assert.assertEquals(16, HashTree.calculateBalancedTreeLeafSize(9, 16))
		Assert.assertEquals(16, HashTree.calculateBalancedTreeLeafSize(16, 16))

		Assert.assertEquals(3, HashTree.calculateBalancedTreeLeafSize(1, 3))
		Assert.assertEquals(3, HashTree.calculateBalancedTreeLeafSize(2, 3))
		Assert.assertEquals(3, HashTree.calculateBalancedTreeLeafSize(3, 3))
		Assert.assertEquals(9, HashTree.calculateBalancedTreeLeafSize(4, 3))
		Assert.assertEquals(9, HashTree.calculateBalancedTreeLeafSize(9, 3))

		Assert.assertEquals(7, HashTree.calculateBalancedTreeLeafSize(1, 7))
		Assert.assertEquals(7, HashTree.calculateBalancedTreeLeafSize(7, 7))
		Assert.assertEquals(49, HashTree.calculateBalancedTreeLeafSize(8, 7))
		Assert.assertEquals(49, HashTree.calculateBalancedTreeLeafSize(49, 7))
		Assert.assertEquals(343, HashTree.calculateBalancedTreeLeafSize(50, 7))
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun reducedTreeDepthShouldBeOneLessThanHashTreeLength() {
		val archiveObjects = generateDefaultArchiveObjects(10)
		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)
		val rht = ht.reduce(archiveObjects.get(0).getDigest())

		Assert.assertEquals((ht.hashes.size - 1).toLong(), rht.reducedHashes.size.toLong())
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun hashOfReducedTreeShouldEqualToHashOfFullTree() {
		val archiveObjects = generateArchiveObjects(10, Integer.MAX_VALUE, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			val reducedTree = ht.reduce(ao.getDigest())
			Assert.assertArrayEquals(ht.getRootHash(), reducedTree.rootHash)
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun anyArityIsSupported() {
		// fix infinite loop bug with arity > 3
		val hashes = generateArchiveObjects()

		for (arity in 4..4) {
			val ht = HashTree(arity = arity.toShort(), digestMethod = DEFAULT_ALGO, archiveObjects = hashes)

			for (hash in hashes) {
				val reducedTree = ht.reduce(hash.getDigest())
				Assert.assertArrayEquals(ht.getRootHash(), reducedTree.rootHash)
			}

		}
	}

	@Test
	@Throws(Exception::class)
	fun reducedTreeShouldContainHashOfLeafToReduce() {
		val arity = 2.toShort()
		val archiveObjects = HashTreeTestUtil.generateArchiveObjects(10, 3, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = arity, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			val reducedTree = ht.reduce(ao.getDigest())
			Assert.assertArrayEquals(ao.getDigest(), reducedTree.reducedHash)
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun reducedTreeShouldContainSiblings() {
		val arity = 2.toShort()
		val archiveObjects = HashTreeTestUtil.generateArchiveObjects(10, 3, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = arity, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			assertReducedTreeSiblingsAreSame(ht, ao)
		}
	}

	@Test
	@Throws(Exception::class)
	fun reducedTreeShouldNotContainNonSiblings() {
		val arity = 2.toShort()
		val archiveObjects = generateArchiveObjects(10, 3, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = arity, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			assertNonSiblingsAreNotPresentInReducedHashTree(ht, ao, archiveObjects)
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun reducedTreeShouldNotContainHashToReduce() {
		val archiveObjects = generateDefaultArchiveObjects(10)
		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			val rht = ht.reduce(ao.getDigest())

			for (sibling in rht.reducedHashes[0]) {
				Assert.assertFalse(Arrays.equals(ao.getDigest(), sibling))
			}
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun hashTreeShouldAlwaysBeBalanced() {
		for (arity in 2..9) {
			for (nrArchiveObjects in 1..19) {
				val ht = HashTree(arity = arity.toShort(), digestMethod = DEFAULT_ALGO, archiveObjects = generateDefaultArchiveObjects(nrArchiveObjects))

				// nr Leafs would be closest factor of arity (2,4,8,16,32,...)
				// (3,9,27,...)
				var arityFactor = arity
				while (arityFactor < nrArchiveObjects) {
					arityFactor *= arity
				}

				Assert.assertEquals(arityFactor.toLong(), ht.hashes[0].size.toLong())
			}
		}
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun oneLeafIsAlsoExpandedToBalancedTree() {
		val archiveObjects = generateDefaultArchiveObjects(1)
		val hashTree = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		val hashList = archiveObjects.map { ao -> ao.getDigest() }.toMutableList()
		HashTree.balanceAndSortInputList(DEFAULT_ALGO, 2, hashList)
		val expectedRootHash = MessageDigestUtil.generateDigest(DEFAULT_ALGO.javaName, hashList)

		Assert.assertArrayEquals(expectedRootHash, hashTree.getRootHash())
	}

	@Test
	@Throws(NoSuchAlgorithmException::class, IOException::class)
	fun hashTreeRootHashShouldBeDerivedFromChildren() {
		val treeArityLimit = 100
		for (curArity in 2..treeArityLimit - 1) {
			val nrArchiveObjects = 1 + SecureRandom().nextInt(treeArityLimit - 1)

			val archiveObjects = HashTreeTestUtil.generateArchiveObjects(nrArchiveObjects, Integer.MAX_VALUE, 1,
					DEFAULT_ALGO)
			val ht = HashTree(arity = curArity.toShort(), digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

			assertRootHashIsDerivedFromLeafHashes(ht)
		}
	}

	@Test
	@Throws(Throwable::class)
	fun recalculatedReducedTreeHashShouldEqualHashtreeRootHash() {
		val archiveObjects = HashTreeTestUtil.generateArchiveObjects(5, 3, 4, DEFAULT_ALGO)

		val ht = HashTree(arity = 2, digestMethod = DEFAULT_ALGO, archiveObjects = archiveObjects)

		for (ao in archiveObjects) {
			val rht = ht.reduce(ao.getDigest())
			val rhtRootHash = rht.rootHash
			val htRootHash = ht.getRootHash()
			Assert.assertArrayEquals(htRootHash, rhtRootHash)
		}
	}

	fun <T> time(body: () -> T, message: String): T {
		val start = System.currentTimeMillis()
		val result: T = body()
		println("Timing [${System.currentTimeMillis() - start}]ms, message:[$message]")
		return result
	}

	@Test
	@Ignore
	fun manualLoadTest() {
		val amountToGenerate = 10_000_000
		val nrLoops = 1

		val archiveObjectsSha256 = time({ generateArchiveObjects(nrLeaves = amountToGenerate, algo = DigestMethod.SHA_256) }, "$amountToGenerate sha256 archiveObjects")

		loadTest(2, DigestMethod.SHA_256, nrLoops, archiveObjectsSha256)
		loadTest(3, DigestMethod.SHA_256, nrLoops, archiveObjectsSha256)
		loadTest(4, DigestMethod.SHA_256, nrLoops, archiveObjectsSha256)
		loadTest(10, DigestMethod.SHA_256, nrLoops, archiveObjectsSha256)

		val archiveObjectsSha512 = time({ generateArchiveObjects(nrLeaves = amountToGenerate, algo = DigestMethod.SHA_512) }, "$amountToGenerate sha512 archiveObjects")

		loadTest(2, DigestMethod.SHA_512, nrLoops, archiveObjectsSha512)
		loadTest(3, DigestMethod.SHA_512, nrLoops, archiveObjectsSha512)
		loadTest(4, DigestMethod.SHA_512, nrLoops, archiveObjectsSha512)
		loadTest(10, DigestMethod.SHA_512, nrLoops, archiveObjectsSha512)
	}

	private fun loadTest(arity: Short, algo: DigestMethod, nrLoops: Int, archiveObjects: List<ArchiveObject>) {

		for (i in 0 until nrLoops) {

			val htInfo = "HashTree [arity: $arity, algo: ${algo.javaName}, nr archiveObjects: ${archiveObjects.size}]"
			val ht = time({ HashTree(arity = arity, digestMethod = algo, archiveObjects = archiveObjects) }, "Creation $htInfo")

			time({
				for (ao in archiveObjects) {
//					val reducedTree =
					ht.reduce(ao.getDigest())
//				assertReducedHashTreeConsistency(reducedTree, ht);
				}
			}, "Reduction $htInfo")
		}
	}

	private fun assertReducedHashTreeConsistency(reducedTree: ReducedHashTree) {
		var reducedHash = reducedTree.reducedHash
		val md = MessageDigest.getInstance(reducedTree.digestMethod.javaName)
		for (siblings in reducedTree.reducedHashes) {
			val allChildren = siblings.toMutableList()
			allChildren.add(reducedHash)
			allChildren.sortWith(ByteArrayComparator())
			allChildren.forEach { hash ->
				//				println("Sibling: ${Arrays.toString(hash)}}")
				md.update(hash)
			}
			reducedHash = md.digest()
//			println("Reduced hash: ${Arrays.toString(reducedHash)}")
//			println()
		}


//		if (!reducedTree.rootHash.equals(reducedHash)) {
//			println("Reduced tree doesnt equal roothash")
//			println("Reduced hash: ${Arrays.toString(reducedTree.reducedHash)}")
//			println("Root hash: ${Arrays.toString(reducedTree.rootHash)}")
//			println("HashTree:")
//			HashTreeTestUtil.printHashTree(ht)
//			println("ReducedTree:")
//			HashTreeTestUtil.printReducedTreeChildren(reducedTree)
//		}
		Assert.assertArrayEquals(reducedTree.rootHash, reducedHash)
	}


	@Throws(NoSuchAlgorithmException::class)
	private fun assertRootHashIsDerivedFromLeafHashes(ht: HashTree) {
		val rootHash = ht.getRootHash()
		val hashList = ht.hashes
		var leafs = hashList[0]

		do {
			val parentDigests = mutableListOf<ByteArray>()
			val tmpList = mutableListOf<ByteArray>()
			for (i in leafs.indices) {
				tmpList.add(leafs[i])

				// arity or end of leaf list reached?
				if (tmpList.size == ht.arity.toInt() || (!tmpList.isEmpty() && i == leafs.size - 1)) {
					tmpList.sortWith(ByteArrayComparator())
					val parentDigest = MessageDigestUtil.generateDigest(ht.digestMethod.javaName, tmpList)
					parentDigests.add(parentDigest)
					tmpList.clear()
				}
			}
			leafs = parentDigests
		} while (leafs.size > 1)

		val recalculatedRootHash = leafs[0]

		Assert.assertArrayEquals(recalculatedRootHash, rootHash)
	}


	@Throws(NoSuchAlgorithmException::class, IOException::class)
	private fun assertReducedTreeSiblingsAreSame(ht: HashTree, ao: ArchiveObject) {
		val aoDigest = ao.getDigest()
		val htSiblings = ht.getSiblings(aoDigest)
		val reducedSiblings = ht.reduce(ao.getDigest()).reducedHashes[0]

		Assert.assertTrue(htSiblings.size == reducedSiblings.size)

		for (htSibling in htSiblings) {
			var found = false
			for (reducedSibling in reducedSiblings) {
				if (Arrays.equals(htSibling, reducedSibling)) {
					found = true
					break
				}
			}
			if (!found) {
				fail("Reduced hasthree does not contain hash of siblings from full hashtree")
			}
		}
	}


	@Throws(NoSuchAlgorithmException::class)
	private fun generateHashes(amountToGenerate: Int, algo: String): List<ByteArray> {
		val ret = mutableListOf<ByteArray>()
		val md = MessageDigest.getInstance(algo)
		for (i in 0..amountToGenerate - 1) {
			ret.add(md.digest(("" + i).toByteArray()))
			md.reset()
		}
		return ret
	}

	@Throws(Exception::class)
	private fun assertNonSiblingsAreNotPresentInReducedHashTree(ht: HashTree, archiveObject: ArchiveObject,
                                                                allArchiveObjects: List<ArchiveObject>) {

		val reducedTree = ht.reduce(archiveObject.getDigest())
		val digestsThatShouldBePresent = reducedTree.reducedHashes[0]

		val digestsThatShouldNotBePresent = allArchiveObjects
				.map { ao -> ao.getDigest() }//
				.filter { digest -> !byteArrayListContainsByteAray(digestsThatShouldBePresent, digest) }//
				.toList()

		for (hash in digestsThatShouldNotBePresent) {
			for (hashThatShouldBePresent in digestsThatShouldBePresent) {
				if (Arrays.equals(hash, hashThatShouldBePresent)) {
					fail("found hash that should not be present")
				}
			}
		}
	}


	private fun byteArrayListContainsByteAray(digestsThatShouldBePresent: List<ByteArray>, digest: ByteArray): Boolean {
		for (bArr in digestsThatShouldBePresent) {
			if (Arrays.equals(bArr, digest)) {
				return true
			}
		}
		return false
	}

}
