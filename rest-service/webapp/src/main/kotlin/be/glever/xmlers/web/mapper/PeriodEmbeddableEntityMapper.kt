/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.mapper

import be.glever.xmlers.business.jpa.entity.PeriodEmbeddableEntity
import be.glever.xmlers.rest.api.generated.model.Period

class PeriodEmbeddableEntityMapper {

	fun map(entity: PeriodEmbeddableEntity): Period =
			Period().apply {
				this.validFrom = entity.validFrom
				this.validUntil = entity.validUntil
			}

	fun map(period: Period): PeriodEmbeddableEntity =
			PeriodEmbeddableEntity(
					validFrom = period.validFrom?.let { it }
							?: PeriodEmbeddableEntity.DEFAULT_VALID_FROM_DATE,
					validUntil = period.validUntil?.let { it }
							?: PeriodEmbeddableEntity.DEFAULT_VALID_UNTIL_DATE
			)
}
