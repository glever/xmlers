package be.glever.xmlers.test.util

import java.time.LocalDate
import java.time.ZoneId
import java.util.*

fun yearFromNow(): Date = LocalDate.now()
        .plusYears(1)
        .atStartOfDay()
        .atZone(ZoneId.systemDefault())
        .toInstant()
        .let { Date.from(it) }
