/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.business.jpa.entity

import javax.persistence.*

@Entity
@Table(name = "DIGEST_METHOD_POLICY")
class DigestMethodPolicyEntity(
	@ManyToOne
	@JoinColumn(name = "DIGEST_METHOD_ID", nullable = false)
	val digestMethod: DigestMethodEntity,

	val period: PeriodEmbeddableEntity

) : AbstractXmlErsEntity(){
	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as DigestMethodPolicyEntity

		if (digestMethod != other.digestMethod) return false
		if (period != other.period) return false

		return true
	}

	override fun hashCode(): Int {
		var result = digestMethod.hashCode()
		result = 31 * result + period.hashCode()
		return result
	}
}