/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.Attributes.Attribute
import ietf.params.xml.ns.ers.ObjectFactory

class AttributeBuilder {

	private val attribute: Attribute

	init {
		this.attribute = ObjectFactory().createAttributesAttribute()
	}

	fun withContent(content: Any?): AttributeBuilder {
		this.attribute.content.add(content)
		return this
	}

	fun withOrder(order: Int): AttributeBuilder {
		this.attribute.order = order
		return this
	}

	fun withType(type: String?): AttributeBuilder {
		this.attribute.type = type
		return this
	}

	fun build(): Attribute {
		return this.attribute
	}

}
