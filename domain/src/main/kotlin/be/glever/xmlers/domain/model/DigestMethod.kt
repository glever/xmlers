/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain.model

import java.util.*

class DigestMethod(val id: Long? = null, val javaName: String, val uri: String, val oid: String) {

    init {
        if (byJavaName[javaName] == null && byUri[uri] == null && byOid[oid] == null) {
            byJavaName.put(javaName, this)
            byOid.put(oid, this)
            byUri.put(uri, this)
            values.add(this)
        }
    }

    companion object {
        private val byJavaName = HashMap<String, DigestMethod>()
        private val byUri = HashMap<String, DigestMethod>()
        private val values = mutableListOf<DigestMethod>()

        private val byOid = HashMap<String, DigestMethod>()
        val MD5 = DigestMethod(javaName = "MD5",
                uri = "http://www.w3.org/2001/04/xmldsig-more#md5", oid = "1.2.840.113549.2.5")
        val SHA_1 = DigestMethod(javaName = "SHA-1",
                uri = "http://www.w3.org/2000/09/xmldsig#sha1", oid = "1.3.14.3.2.26")
        val SHA_256 = DigestMethod(javaName = "SHA-256",
                uri = "http://www.w3.org/2001/04/xmlenc#sha256", oid = "2.16.840.1.101.3.4.2.1")
        val SHA_384 = DigestMethod(javaName = "SHA-384",
                uri = "http://www.w3.org/2001/04/xmldsig-more#sha384", oid = "2.16.840.1.101.3.4.2.2")
        val SHA_512 = DigestMethod(javaName = "SHA-512",
                uri = "http://www.w3.org/2001/04/xmlenc#sha512", oid = "2.16.840.1.101.3.4.2.3")

        fun values(): List<DigestMethod> = values.toList()

        fun fromJavaName(javaName: String): DigestMethod? = values.find { it.javaName.equals(javaName) }
        fun fromOid(oid: String): DigestMethod? = values.find { it.oid.equals(oid) }
        fun fromUri(uri: String): DigestMethod? = values.find { it.uri.equals(uri) }
    }

    override fun toString(): String {
        return "DigestMethod(javaName='$javaName', uri='$uri', oid='$oid')"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DigestMethod

        if (javaName != other.javaName) return false
        if (uri != other.uri) return false
        if (oid != other.oid) return false

        return true
    }

    override fun hashCode(): Int {
        var result = javaName.hashCode()
        result = 31 * result + uri.hashCode()
        result = 31 * result + oid.hashCode()
        return result
    }


}
