/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.rest.client

import javax.ws.rs.client.Client
import javax.ws.rs.client.WebTarget

class XmlersRestClient(client: Client, uri: String) {

	private val webTarget: WebTarget = client.register(JacksonObjectMapper::class.java).target(uri)

	val archivalConfigRestClient: ArchivalConfigRestClient
	val digestMethodRestClient: DigestMethodRestClient
	val c14nMethodRestClient: C14nMethodRestClient
	val archiveObjectRestClient: ArchiveObjectRestClient
	val jobControlRestClient: JobControlRestClient
	val evidenceRecordRestClient: EvidenceRecordRestClient

	init {
		archivalConfigRestClient = ArchivalConfigRestClient(webTarget.path("archivalConfig"))
		archiveObjectRestClient = ArchiveObjectRestClient(webTarget.path("archiveObject"))
		digestMethodRestClient = DigestMethodRestClient(webTarget.path("digestMethod"))
		c14nMethodRestClient = C14nMethodRestClient(webTarget.path("c14nMethod"))
		jobControlRestClient = JobControlRestClient(webTarget.path("jobControl"))
		evidenceRecordRestClient = EvidenceRecordRestClient(webTarget.path("evidenceRecord"))
	}


}