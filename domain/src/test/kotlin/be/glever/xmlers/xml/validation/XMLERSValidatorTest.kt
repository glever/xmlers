package be.glever.xmlers.xml.validation

import be.glever.xmlers.AbstractPkiEnabledTest
import be.glever.xmlers.domain.model.ArchiveTimestamp
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.model.HashTree
import be.glever.xmlers.domain.util.ArchiveTimestampBuilder
import be.glever.xmlers.xml.generation.jaxb.EvidenceRecordBuilder
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import java.security.MessageDigest
import java.util.*


class XMLERSValidatorTest : AbstractPkiEnabledTest() {
    val digestMethod = DigestMethod.SHA_256
    val md = MessageDigest.getInstance(digestMethod.javaName)

    lateinit var digests: MutableList<ByteArray>
    lateinit var ht: HashTree
    lateinit var archiveTimestamp: ArchiveTimestamp

    @Before
    fun before() {
        digests = (1..2).map { md.digest(ByteArray(1) { it.toByte() }) }.toMutableList()
        ht = HashTree(arity = 2, digestMethod = digestMethod, digests = this.digests)
        archiveTimestamp = ArchiveTimestampBuilder(timestamper = timestamper, digestMethod = digestMethod).generateArchiveTimestamp(ht)
    }

    @Test
    fun shouldValidateToHashTreeRootHash() {
        val unknownDigest = UUID.randomUUID().toString().toByteArray().let { md.digest() }

        digests.forEach { digest ->
            val evidenceRecordType = EvidenceRecordBuilder().fromInitialTimestamp(reducedHashTree = ht.reduce(digest), archiveTimestamp = archiveTimestamp, digestMethodType = digestMethod)
            val validationResult = XMLERSValidator(evidenceRecordType = evidenceRecordType, digestToVerify = digest).validate()
            assertTrue(validationResult.chainsToRootHash)

            assertFalse(XMLERSValidator(evidenceRecordType = evidenceRecordType, digestToVerify = unknownDigest).validate().chainsToRootHash)
        }
    }

    @Test
    fun shouldValidateToDigestIfNoHashTree() {
        val digest = md.digest(UUID.randomUUID().toString().toByteArray())
        archiveTimestamp = ArchiveTimestampBuilder(timestamper = timestamper, digestMethod = digestMethod)
                .generateArchiveTimestamp(digest)
        val evidenceRecordType = EvidenceRecordBuilder().fromInitialTimestamp(digestMethodType = digestMethod, archiveTimestamp = archiveTimestamp)

        assertTrue(XMLERSValidator(evidenceRecordType = evidenceRecordType, digestToVerify = digest).validate().chainsToRootHash)

        val randomDigest = md.digest(UUID.randomUUID().toString().toByteArray())
        assertFalse(XMLERSValidator(evidenceRecordType = evidenceRecordType, digestToVerify = randomDigest).validate().chainsToRootHash)

    }
}
