swagger: "2.0"
info:
  version: 1.0.0
  title: XMLERS
  license:
    name: LGPL 3.0
schemes:
  - http
consumes:
  - application/json
produces:
  - application/json
paths:
  /jobControl/run/{archivalConfigUniqueId}:
    parameters:
      - name: archivalConfigUniqueId
        in: path
        description: id of archivalConfig for which to run the job
        required: true
        type: string
    post:
      tags:
        - JobControl
      summary: Launch archival job for specified archival configuration
      operationId: launchArchivalJob
      responses:
        "204":
          description: Job started successfully
        "404":
          $ref: '#/responses/NotFound'
        500: 
          $ref: "#/responses/UnexpectedError"
  /archivalConfig:
    put:
      tags:
        - ArchivalConfig
      summary: Create an archivalconfig
      operationId: createArchivalConfig
      parameters:
        - name: archivalConfig
          in: body
          description: ArchivalConfig to create
          required: true
          schema:
            $ref: ./ArchivalConfig.yml
      responses:
        201:
          description: Created succesfully
          schema:
            $ref: ./ArchivalConfig.yml
        400:
          $ref: '#/responses/BadRequest'
        500: 
          $ref: '#/responses/UnexpectedError'
    get:
      tags:
        - ArchivalConfig
      operationId: searchArchivalConfig
      responses:
        200:
          description: list of all archivalconfigs
          schema:
            type: array
            items:
              $ref: ./ArchivalConfig.yml
        500: 
          $ref: '#/responses/UnexpectedError'
  /archivalConfig/{archivalConfigUniqueId}:
    parameters:
      - name: archivalConfigUniqueId
        in: path
        required: true
        type: string
    get:
      tags:
        - ArchivalConfig
      summary: Retrieve the ArchivalConfig for given id
      operationId: getArchivalConfig
      responses:
        200:
          description: The ArchivalConfig
          schema:
            $ref: ./ArchivalConfig.yml
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'
    post:
      tags:
        - ArchivalConfig
      summary: Update an archivalconfig
      operationId: updateArchivalConfig
      parameters:
        - name: ArchivalConfig
          in: body
          description: ArchivalConfig to update
          required: true
          schema:
            $ref: ./ArchivalConfig.yml
      responses:
        "200":
          description: The resource was deleted successfully.
        "400":
          $ref: '#/responses/BadRequest'
        500: 
          $ref: '#/responses/UnexpectedError'
    delete:
      tags:
        - ArchivalConfig
      summary: Delete the given archival config
      operationId: deleteArchivalConfig
      responses:
        "204":
          description: ArchivalConfig deleted successfully
        "404":
          $ref: '#/responses/NotFound'
        500: 
          $ref: '#/responses/UnexpectedError'
  /archiveObject/{archivalConfigUniqueId}:
    parameters:
      - name: archivalConfigUniqueId
        in: path
        required: true
        type: string
    put:
      tags:
        - ArchiveObject
      summary:
        Adds the given ArchiveObject and attached dataObjects to a given ArchivalConfig.
        Note that the LTA process for a given ArchiveObject can only start once all its'
        DataObjects have a Digest in the ArchivalConfigs' current DigestMethod.
      operationId: addArchiveObject
      parameters:
        - name: archiveObject
          in: body
          description: the archiveObject to upload
          required: true
          schema:
            $ref: ./ArchiveObject.yml
      responses:
        "204":
          description: Successfully added ArchiveObject.
        "400":
          $ref: '#/responses/BadRequest'
        500:
          $ref: '#/responses/UnexpectedError'
  /archiveObject/{archivalConfigUniqueId}/{archiveObjectUniqueId}:
    parameters:
      - name: archivalConfigUniqueId
        in: path
        required: true
        description: id of archivalConfig to which to add add the ArchiveObject.
        type: string
      - name: archiveObjectUniqueId
        in: path
        required: true
        description: id of archivalConfig to which to add add the ArchiveObject.
        type: string
    get:
      tags:
        - ArchiveObject
      summary: returns ArchiveObject
      operationId: getArchiveObject
      responses:
        200:
          description: returns the archive object
          schema:
            $ref: ./ArchiveObject.yml
        500:
          $ref: '#/responses/UnexpectedError'
    post:
      parameters:
        - name: dataObjects
          in: body
          schema:
            $ref: ./DataObjectList.yml
          required: true
      tags:
        - ArchiveObject
      summary: Add new Digests for the DataObjects of an ArchiveObject.
      description:
        Note that the only allowed operation is specifying new digests for DataObjects for which no other Digest with the same DigestMethod exists. If colliding Digests (same DigestMethod, different Digest) are detected, a BadRequest will be generated. It is allowed to specify only the new Digests.
      operationId: updateDataObjects
      responses:
        204:
          description: Update ok
        400:
          $ref: '#/responses/BadRequest'
        500:
          $ref: '#/responses/UnexpectedError'
  /archiveObject/{archivalConfigUniqueId}/{archiveObjectUniqueId}/evidenceRecord:
    parameters:
      - name: archivalConfigUniqueId
        in: path
        required: true
        description: id of archivalConfig to which to add add the ArchiveObject.
        type: string
      - name: archiveObjectUniqueId
        in: path
        required: true
        description: id of archivalConfig to which to add add the ArchiveObject.
        type: string
    get:
      tags:
        - EvidenceRecord
      summary: Get the EvidenceRecord
      operationId: getEvidenceRecord
      produces:
        - application/json
      responses:
        200:
          description: If LTA process ran, return EvidenceRecord xml for this ArchiveObject
          schema:
            type: string
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'
  /c14nMethod:
    get:
      tags:
        - C14nMethod
      summary: Get all defined C14nMethods
      operationId: listC14nMethods
      responses:
        200:
          description: List of defined C14nMethods
          schema:
            type: array
            items:
              $ref: ./C14nMethod.yml
        500:
          $ref: '#/responses/UnexpectedError'
    put:
      tags:
        - C14nMethod
      summary: Add new C14nMethod
      operationId: addC14nMethod
      parameters:
        - name: c14nMethod
          in: body
          schema:
            $ref: ./C14nMethod.yml
          required: true
      responses:
        201:
          description: C14nMethod has been created.
        500:
          $ref: '#/responses/UnexpectedError'
  /c14nMethod/{c14nMethodUri}:
    parameters:
      - name: c14nMethodUri
        in: path
        required: true
        type: string
    get:
      tags:
        - C14nMethod
      summary: Get C14nMethod with given uri
      operationId: getC14nMethod
      responses:
        200:
          description: The C14nMethod with given uri
          schema:
            $ref: ./C14nMethod.yml
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'
    delete:
      tags:
        - C14nMethod
      summary: delete the given C14nMethod if possible (not yet in use)
      operationId:  deleteC14nMethod
      responses:
        204:
          description: Delete successful
        400:
          $ref: '#/responses/BadRequest'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'
  /digestMethod:
    put:
      tags:
        - DigestMethod
      summary: Add new DigestMethod
      operationId: addDigestMethod
      parameters:
        - name: digestMethod
          in: body
          required: true
          schema:
            $ref: ./DigestMethod.yml
      responses:
        201:
          description: created successfully
        400:
          $ref: '#/responses/BadRequest'
        500:
          $ref: '#/responses/UnexpectedError'
    get:
      tags:
        - DigestMethod
      summary: Retrieve all digestmethods matching any of the given parameters. If no parameters are given, returns all digestMethods.
      operationId: searchDigestMethods
      parameters:
        - name: oids
          type: array
          items:
            type: string
          required: false
          in: query
        - name: javaNames
          type: array
          items:
            type: string
          required: false
          in: query
        - name: uris
          type: array
          items:
            type: string
          required: false
          in: query
      responses:
        200:
          description: all DigestMethods
          schema:
            type: array
            items:
              $ref: ./DigestMethod.yml
  /digestMethod/{oid}:
    parameters:
      - name: oid
        type: string
        required: true
        in: path
    get:
      tags:
        - DigestMethod
      operationId: getDigestMethodByOid
      responses:
        200:
          description: The found digestmethod
          schema:
            $ref: ./DigestMethod.yml
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'
    delete:
      tags:
        - DigestMethod
      summary: Retrieve the digestMethod
      operationId: deleteDigestMethod
      responses:
        204:
          description: The DigestMethod has been deleted.
        400:
          $ref: '#/responses/BadRequest'
        404:
          $ref: '#/responses/NotFound'
        500:
          $ref: '#/responses/UnexpectedError'

responses:
  UnexpectedError:
    description: An unexpected error has occurred on the server.
    schema:
      $ref: '#/definitions/ErrorResponse'
  BadRequest:
    description: The input parameters were not correct
    schema:
      $ref: '#/definitions/ErrorResponse'
  NotFound:
    description: The specified resource was not found
    schema:
      $ref: "#/definitions/ErrorResponse"
  Unauthorized:
    description: Unauthorized
    schema:
      $ref: "#/definitions/ErrorResponse"
definitions:
  ErrorResponse:
    type: object
    properties:
      code:
        type: string
      message:
        type: string
    required:
      - code
      - message