/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package pkix.utils

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.NoSuchAlgorithmException
import java.time.Instant
import java.time.ZoneId
import java.util.Date

import javax.naming.ldap.LdapName

/**
 * Contains requirements to deviate from the default.
 *
 * @author glenv
 */
class CertificateSpec {
    var startDate = Date()
        private set
    var expiryDate = Date.from(Instant.ofEpochMilli(startDate.time).atZone(ZoneId.systemDefault())
            .toLocalDate().plusYears(10).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
        private set
    var dirName = "CN=Test Certificate Name"
        private set
    var kpAlgo = "RSA"
        private set
    var kpKeySize = 1024
        private set
    var signatureAlgo = "SHA256withRSA"
        private set

    /**
     * @return The keypair to be used to generate the certificate
     * @throws NoSuchAlgorithmException
     */
    val keyPair: KeyPair
        @Throws(NoSuchAlgorithmException::class)
        get() = KeyPairGenerator.getInstance("SHA256WithRsa").generateKeyPair()

    val subjectDN: LdapName?
        get() = null

    fun withStartDate(startDate: Date): CertificateSpec {
        this.startDate = startDate
        return this
    }

    fun withExpiryDate(expiryDate: Date): CertificateSpec {
        this.expiryDate = expiryDate
        return this
    }

    fun withDirName(dirName: String): CertificateSpec {
        this.dirName = dirName
        return this
    }

    fun withKPAlgo(KPAlgo: String): CertificateSpec {
        this.kpAlgo = KPAlgo
        return this
    }

    fun withKPKeySize(KPKeySize: Int): CertificateSpec {
        this.kpKeySize = KPKeySize
        return this
    }

    fun withSignatureAlgo(signatureAlgo: String): CertificateSpec {
        this.signatureAlgo = signatureAlgo
        return this
    }
}
