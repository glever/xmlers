package be.glever.xmlers.domain.util

import be.glever.xmlers.domain.model.ArchiveTimestamp
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.model.HashTree
import be.glever.xmlers.domain.model.Timestamp
import be.glever.xmlers.pkix.TimeStamper

class ArchiveTimestampBuilder(val timestamper: TimeStamper, val digestMethod: DigestMethod) {
    fun generateArchiveTimestamp(hashTree: HashTree) = generateArchiveTimestamp(hashTree, hashTree.getRootHash())
    fun generateArchiveTimestamp(digest: ByteArray) = generateArchiveTimestamp(null, digest)

    private fun generateArchiveTimestamp(hashTree: HashTree? = null, digest: ByteArray): ArchiveTimestamp {
        val timeStampToken = timestamper.timestamp(digestMethod, digest)
        return ArchiveTimestamp(hashTree, Timestamp(timeStampToken.encoded), digestMethod)
    }
}
