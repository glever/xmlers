/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.pkix

import org.apache.commons.codec.binary.Base64InputStream
import org.apache.commons.io.IOUtils
import org.bouncycastle.cert.X509CertificateHolder
import org.junit.Ignore
import org.junit.Test
import java.security.Security
import java.security.cert.*


@Ignore // ran manually
class CertChecker {
	@Test
	fun checkCertBouncy() {

		Security.setProperty("ocsp.enable", "true")
		System.setProperty("com.sun.net.ssl.checkRevocation", "true")
		System.setProperty("com.sun.security.enableCRLDP", "true")

		val classpath = "/tsa-chain"
		val endEntityCert = readCert("${classpath}/end-entity.pem")
		val inter1Cert = readCert("${classpath}/inter1.pem")
		val inter2Cert = readCert("${classpath}/inter2.pem")
		val rootCert = readCert("${classpath}/root.pem")

		println("$endEntityCert\n======================================")
		println("$inter1Cert\n======================================")
		println("$inter2Cert\n======================================")
		println("$rootCert\n======================================")

		val validator = CertPathValidator.getInstance("PKIX")
		val pkixParameters = PKIXParameters(mutableSetOf(TrustAnchor(rootCert, null)))
		val certPath = CertificateFactory.getInstance("X.509").generateCertPath(listOf(endEntityCert, inter1Cert, inter2Cert))


//		val rc = validator.getRevocationChecker() as PKIXRevocationChecker
//		rc.options = EnumSet.of<PKIXRevocationChecker.Option>(PKIXRevocationChecker.Option.SOFT_FAIL)
//		pkixParameters.addCertPathChecker(rc)

//		CertPathBuilder.getInstance("PKIX").
		val result = validator.validate(certPath, pkixParameters)



		println(result)
//		val verifier = JcaX509ContentVerifierProviderBuilder().setProvider(BouncyCastleProvider.PROVIDER_NAME)
//		val path = CertPath(arrayOf(endEntityCert, inter1Cert, inter2Cert))
//		path.validate(arrayOf(ParentCertIssuedValidation(verifier), BasicConstraintsValidation(), KeyUsageValidation()))
	}

	private fun readCert(classpathUri: String): X509Certificate =
			CertificateFactory.getInstance("X.509")
					.generateCertificate(Base64InputStream(CertChecker::class.java.getResourceAsStream(classpathUri), false)) as X509Certificate?
					?: throw IllegalArgumentException("Could not read certificate")

	private fun readCertHolder(classpathUri: String): X509CertificateHolder =
			X509CertificateHolder(IOUtils.toByteArray(Base64InputStream(CertChecker::class.java.getResourceAsStream(classpathUri), false)))
}