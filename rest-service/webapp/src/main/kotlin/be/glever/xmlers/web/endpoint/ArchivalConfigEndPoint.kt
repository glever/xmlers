/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.web.endpoint

import be.glever.xmlers.rest.api.generated.api.ArchivalConfigApi
import be.glever.xmlers.rest.api.generated.model.ArchivalConfig
import be.glever.xmlers.service.ArchivalConfigService
import be.glever.xmlers.service.ArchiveObjectService
import be.glever.xmlers.service.C14nMethodService
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.web.mapper.ArchivalConfigMapper
import be.glever.xmlers.web.mapper.ArchiveObjectMapper
import be.glever.xmlers.web.mapper.DataObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext

class ArchivalConfigEndPoint : ArchivalConfigApi {


	@Autowired
	private lateinit var archivalConfigService: ArchivalConfigService
	@Autowired
	private lateinit var digestMethodService: DigestMethodService
	@Autowired
	private lateinit var c14nMethodService: C14nMethodService
	@Autowired
	private lateinit var archiveObjectService: ArchiveObjectService

	private fun mapper(): ArchivalConfigMapper = ArchivalConfigMapper(digestMethodService, c14nMethodService)
	private fun archiveObjectMapper(): ArchiveObjectMapper = ArchiveObjectMapper(digestMethodService)
	private fun dataObjectMapper(): DataObjectMapper = DataObjectMapper(digestMethodService)

	override fun createArchivalConfig(archivalConfig: ArchivalConfig, securityContext: SecurityContext): Response {
		try {
			val createdArchivalConfig = archivalConfigService.create(mapper().map(archivalConfig))
			return Response.status(Response.Status.CREATED).entity(mapper().map(createdArchivalConfig)).build()
		} catch (e: DataIntegrityViolationException) {
			throw WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity("Cannot insert duplicate archivalConfig (uniqueId ${archivalConfig.uniqueId}").build())
		}
	}


	override fun getArchivalConfig(uniqueId: String, securityContext: SecurityContext): Response =
			try {
				archivalConfigService.findByUniqueId(uniqueId)
						.let { archivalConfig -> Response.status(Response.Status.OK).entity(mapper().map(archivalConfig)).build() }
			} catch (e: EmptyResultDataAccessException) {
				throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("ArchivalConfig with id [$uniqueId] not found.").build())
			}


	override fun updateArchivalConfig(uniqueId: String, archivalConfig: ArchivalConfig, securityContext: SecurityContext): Response {
		try {
			archivalConfig.uniqueId = uniqueId // force consistency of path param with given object

			val dbArchivalConfig = archivalConfigService.findByUniqueId(uniqueId)
			val newEntity = mapper().map(archivalConfig)
			val mergedEntity = dbArchivalConfig.copy(
					uniqueId = dbArchivalConfig.uniqueId
					, c14nMethod = newEntity.c14nMethod
					, tsaConfig = newEntity.tsaConfig
					, digestMethodPolicyEntities = dbArchivalConfig.digestMethodPolicyEntities.plus(newEntity.digestMethodPolicyEntities.filter { newPolicy -> !dbArchivalConfig.digestMethodPolicyEntities.contains(newPolicy) }))
			archivalConfigService.update(mergedEntity)
			return Response.status(Response.Status.OK).build()
		} catch (e: EmptyResultDataAccessException) {
			throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("ArchivalConfig with id [$archivalConfig.id] not found.").build())
		}
	}


	override fun searchArchivalConfig(securityContext: SecurityContext): Response =
			Response.status(Response.Status.OK).entity(archivalConfigService.findAll().map { entity -> mapper().map(entity) }.toMutableList()).build()


	override fun deleteArchivalConfig(uniqueId: String, securityContext: SecurityContext): Response =
			try {
				archivalConfigService.findByUniqueId(uniqueId)
						.let { archivalConfig ->
							archivalConfigService.delete(archivalConfig)
							Response.status(Response.Status.NO_CONTENT).build()
						}
			} catch (e: EmptyResultDataAccessException) {
				throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("ArchivalConfig with uniqueId [$uniqueId] not found.").build())
			}


}



