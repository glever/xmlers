package be.glever.xmlers.test.util

import java.security.cert.X509Certificate

class DefaultTestPkiServer(val server: TestPKIServer = TestPKIServer()) {

    fun stop() = this.server.stop()
    fun tsaUrl() = this.server.tsaUrl()
    fun tsaCert() = this.server.tsaCert
    fun tsaRootCert() = this.server.rootCert
    fun getChain(cert: X509Certificate) = this.server.getChain(cert)
}
