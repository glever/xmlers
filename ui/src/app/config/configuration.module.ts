import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ArchivalConfigListComponent } from '@xmlers/config/archival-config-list/archival-config-list.component';
import { ConfigurationComponent } from '@xmlers/config/configuration.component';
import { ArchivalconfigEffects } from '@xmlers/config/+state/archivalconfig.effects';
import { reducers } from '@xmlers/config/+state/reducers';
import { ConfigurationRoutingModule } from '@xmlers/config/configuration-routing.module';

@NgModule({
  declarations: [ArchivalConfigListComponent, ConfigurationComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('archivalConfigs', reducers),
    EffectsModule.forFeature([ArchivalconfigEffects]),
    ConfigurationRoutingModule,
  ],
  providers: []
})
export class ConfigurationModule {
}
