/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain.builder

import be.glever.xmlers.AbstractPkiEnabledTest
import be.glever.xmlers.domain.HashTreeTestUtil
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.util.ArchiveTimestampBuilder
import be.glever.xmlers.xml.generation.jaxb.EvidenceRecordBuilder
import be.glever.xmlers.xml.marshallToString
import junit.framework.Assert.assertNull
import org.junit.Test
import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.cert.CertPathValidatorException
import java.util.*
import javax.xml.bind.JAXBException


class EvidenceRecordBuilderTest : AbstractPkiEnabledTest() {
    val digestMethod = DigestMethod.SHA_256
    val messageDigest = MessageDigest.getInstance(digestMethod.javaName)

    @Test
    @Throws(JAXBException::class, NoSuchAlgorithmException::class)
    fun testEvidenceRecordBuilder() {
        val start = System.currentTimeMillis()

//        val digests = (1..10000000).map { toString() }.map {
        val digests = (1..2).map { toString() }.map {
            messageDigest.digest(it.toByteArray(Charset.defaultCharset()))
        }
        val ht = HashTreeTestUtil.generateHashTree(digestMethod = digestMethod, byteArrays = digests)
        val archiveTimestamp = ArchiveTimestampBuilder(
                timestamper = timestamper,
                digestMethod = digestMethod
        ).generateArchiveTimestamp(ht)

        println(System.currentTimeMillis() - start)
        digests.forEach { digest ->

            //            if (index % 100000 == 0) println(index)
            val evidenceRecordType = EvidenceRecordBuilder().fromInitialTimestamp(ht.reduce(digest), archiveTimestamp, digestMethod)
            marshallToString(evidenceRecordType)

//            assertEquals(BigDecimal.ONE, evidenceRecordType.version)

        }
    }

    @Test
    fun evidenceRecordFromTimestampShouldHaveNullHashTree() {
        val archiveTimestamp = ArchiveTimestampBuilder(timestamper = timestamper, digestMethod = digestMethod).generateArchiveTimestamp(digest = messageDigest.digest(UUID.randomUUID().toString().toByteArray()))
        val evidenceRecordType = EvidenceRecordBuilder().fromInitialTimestamp(archiveTimestamp = archiveTimestamp, digestMethodType = digestMethod)

        assertNull(evidenceRecordType.archiveTimeStampSequence.archiveTimeStampChain[0].archiveTimeStamp[0].hashTree)
    }

    @Test
    fun canGenerateEvidenceRecord() {
        val archiveTimestamp = ArchiveTimestampBuilder(timestamper = timestamper, digestMethod = digestMethod).generateArchiveTimestamp(digest = messageDigest.digest(UUID.randomUUID().toString().toByteArray()))
        EvidenceRecordBuilder().fromInitialTimestamp(archiveTimestamp = archiveTimestamp, digestMethodType = digestMethod)
    }

    @Test
    fun timestampingShouldPreferOcsp() {
        server.server.ocspActive = true
        server.server.crlActive = false
        canGenerateEvidenceRecord()
    }

    @Test
    fun timestampingShouldFallBackToCrl() {
        server.server.ocspActive = false
        canGenerateEvidenceRecord()
    }

    @Test(expected = CertPathValidatorException::class)
    fun shouldFailWhenNoRevocationAvailable() {
        server.server.ocspActive = false
        server.server.crlActive = false
        canGenerateEvidenceRecord()
    }
}
