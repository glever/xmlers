/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */



package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.CryptographicInformationType.CryptographicInformation
import ietf.params.xml.ns.ers.TimeStampType
import ietf.params.xml.ns.ers.TimeStampType.TimeStampToken

class TimeStampTypeBuilder {
	private val timeStampType: TimeStampType

	init {
		this.timeStampType = TimeStampType()

		this.timeStampType.cryptographicInformationList
		this.timeStampType.timeStampToken
	}

	fun withCryptographicInformation(cryptographicInformation: CryptographicInformation): TimeStampTypeBuilder {
		this.timeStampType
				.cryptographicInformationList
				.cryptographicInformation
				.add(cryptographicInformation)
		return this
	}

	fun withRFC3161TimeStampToken(timeStampToken: ByteArray?): TimeStampTypeBuilder {
		val token = TimeStampToken().apply{
			type = RFC3161_TYPE
			content.add(timeStampToken)
		}
		this.timeStampType.timeStampToken = token
		return this
	}

	fun build(): TimeStampType {
		return this.timeStampType
	}

	companion object {
		val RFC3161_TYPE = "RFC3161"
	}

}
