package be.glever.xmlers.rest.client

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule

import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider

@Provider
class JacksonObjectMapper : ContextResolver<ObjectMapper> {

	internal val defaultObjectMapper: ObjectMapper

	init {
		defaultObjectMapper = createDefaultMapper()
	}

	override fun getContext(type: Class<*>): ObjectMapper {
		return defaultObjectMapper
	}

	private fun createDefaultMapper(): ObjectMapper {
		val mapper = ObjectMapper()
		//    mapper.registerModule(new JavaTimeModule());
		mapper.findAndRegisterModules()
		return mapper
	}
}