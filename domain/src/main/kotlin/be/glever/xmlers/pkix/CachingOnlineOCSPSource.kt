/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.pkix

import eu.europa.esig.dss.client.ocsp.OnlineOCSPSource
import eu.europa.esig.dss.x509.CertificateToken
import eu.europa.esig.dss.x509.ocsp.ListOCSPSource
import eu.europa.esig.dss.x509.ocsp.OCSPSource
import eu.europa.esig.dss.x509.ocsp.OCSPToken

/**
 * OfflineOCSPSource that delegates to an OnlineOCSPSource and caches the result.
 * Used for rapid successive ingestion of data signed by same certificate.
 * NOT To be used for XMLERS validation.
 */
class CachingOnlineOCSPSource : OCSPSource {
	val onlineOcspSource = OnlineOCSPSource()

	val offlineOcspSource = ListOCSPSource()
	override fun getRevocationToken(certificateToken: CertificateToken?, issuerCertificateToken: CertificateToken?):  OCSPToken? =
			offlineOcspSource.getRevocationToken(certificateToken, issuerCertificateToken) ?: onlineOcspSource.getRevocationToken(certificateToken, issuerCertificateToken)

}
