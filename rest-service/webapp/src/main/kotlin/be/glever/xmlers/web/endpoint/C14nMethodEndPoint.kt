/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.endpoint

import be.glever.xmlers.rest.api.generated.api.C14nMethodApi
import be.glever.xmlers.rest.api.generated.model.C14nMethod
import be.glever.xmlers.service.C14nMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import be.glever.xmlers.web.mapper.C14nMethodMapper
import org.springframework.beans.factory.annotation.Autowired
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext

class C14nMethodEndPoint : C14nMethodApi {
	@Autowired
	lateinit var c14nMethodService: C14nMethodService

	override fun addC14nMethod(c14nMethod: C14nMethod, securityContext: SecurityContext): Response =
			if (c14nMethodService.findByUri(c14nMethod.uri) == null) {
				c14nMethodService.add(C14nMethodMapper().map(c14nMethod))
						.let { Response.status(Response.Status.CREATED).build() }
			} else {
				throw WebApplicationException(Response.status(Response.Status.CONFLICT).entity("C14nMethod with given uri already exists.").build())
			}


	override fun deleteC14nMethod(c14nMethodUri: String, securityContext: SecurityContext): Response =
			try {
				this.c14nMethodService.delete(uri = c14nMethodUri)
				Response.status(Response.Status.NO_CONTENT).build()
			} catch (e: XmlErsBusinessException) {
				throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("No C14nMethod found with uri $c14nMethodUri").build())
			}

	override fun getC14nMethod(c14nMethodId: String, securityContext: SecurityContext): Response =
			c14nMethodService.findByUri(c14nMethodId).let { c14nMethod ->
				when (c14nMethod) {
					null -> Response.status(Response.Status.NOT_FOUND).build()
					else -> Response.status(Response.Status.OK).entity(C14nMethodMapper().map(c14nMethod)).build()
				}
			}

	override fun listC14nMethods(securityContext: SecurityContext): Response =
			Response.status(Response.Status.OK)
					.entity(c14nMethodService.findAll().map { c14Method -> C14nMethodMapper().map(c14Method) })
					.build()


}
