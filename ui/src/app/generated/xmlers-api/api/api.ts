export * from './archivalConfig.service';
import { ArchivalConfigService } from './archivalConfig.service';
export * from './archivalConfig.serviceInterface'
export * from './archiveObject.service';
import { ArchiveObjectService } from './archiveObject.service';
export * from './archiveObject.serviceInterface'
export * from './c14nMethod.service';
import { C14nMethodService } from './c14nMethod.service';
export * from './c14nMethod.serviceInterface'
export * from './digestMethod.service';
import { DigestMethodService } from './digestMethod.service';
export * from './digestMethod.serviceInterface'
export * from './evidenceRecord.service';
import { EvidenceRecordService } from './evidenceRecord.service';
export * from './evidenceRecord.serviceInterface'
export * from './jobControl.service';
import { JobControlService } from './jobControl.service';
export * from './jobControl.serviceInterface'
export const APIS = [ArchivalConfigService, ArchiveObjectService, C14nMethodService, DigestMethodService, EvidenceRecordService, JobControlService];
