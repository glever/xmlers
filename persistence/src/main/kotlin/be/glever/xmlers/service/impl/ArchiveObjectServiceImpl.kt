/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.service.impl

import be.glever.xmlers.business.jpa.dao.ArchiveObjectDao
import be.glever.xmlers.business.jpa.dao.DataObjectDao
import be.glever.xmlers.business.jpa.dao.DigestMethodDao
import be.glever.xmlers.business.jpa.entity.ArchiveObjectEntity
import be.glever.xmlers.business.jpa.entity.DataObjectDigestEntity
import be.glever.xmlers.business.jpa.entity.DataObjectEntity
import be.glever.xmlers.service.ArchiveObjectService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Transactional
@Repository
class ArchiveObjectServiceImpl(val archiveObjectDao: ArchiveObjectDao, val dataObjectDao: DataObjectDao, val digestMethodDao: DigestMethodDao) : ArchiveObjectService {
	override fun insert(archiveObject: ArchiveObjectEntity): ArchiveObjectEntity {
		archiveObject.let {
			it.id = null
			it.dataObjects.forEach { it.id = null }
		}

		validate(archiveObject)
		return this.archiveObjectDao.save(archiveObject)
	}

	private fun validate(archiveObject: ArchiveObjectEntity) {
		archiveObject.dataObjects.forEach { firstDataObject ->
			if (!archiveObject.dataObjects.none { it != firstDataObject && it.uniqueId == firstDataObject.uniqueId }) {
				throw XmlErsBusinessException("DataObjects with duplicate uniqueId ${firstDataObject.uniqueId} given")
			}
		}

		archiveObject.dataObjects.forEach { dataObject ->
			dataObject.digests.forEach { firstDigest ->
				if (!dataObject.digests.none { it != firstDigest && it.digestMethod.oid == firstDigest.digestMethod.oid }) {
					throw XmlErsBusinessException("DataObject ${dataObject.uniqueId} would contain multiple digests with same digestmethod ${firstDigest.digestMethod.oid}")
				}
			}
		}
	}

	override fun findByArchivalConfigIdAndArchiveObjectId(archivalConfigId: Long, archiveObjectId: Long): ArchiveObjectEntity =
			this.archiveObjectDao.findByArchivalConfigIdAndId(archivalConfigId, archiveObjectId)

	override fun findByArchivalConfigUniqueIdAndArchiveObjectUniqueId(archivalConfigUniqueId: String, archiveObjectUniqueId: String): ArchiveObjectEntity =
			this.archiveObjectDao.findByArchivalConfigUniqueIdAndUniqueId(archivalConfigUniqueId, archiveObjectUniqueId)

	override fun findAllByArchivalConfigIdAndArchiveObjectIds(archivalConfigId: Long, archiveObjectIds: List<Long>): List<ArchiveObjectEntity> =
			if (archiveObjectIds.isEmpty()) this.archiveObjectDao.findByArchivalConfigId(archivalConfigId)
			else this.archiveObjectDao.findByArchivalConfigIdAndIdIn(archivalConfigId, archiveObjectIds)


	override fun updateArchiveObjectDigests(archivalConfigUniqueId: String, archiveObjectUniqueId: String, dataObjects: List<DataObjectEntity>) {
		val archiveObjectEntity = this.archiveObjectDao.findByArchivalConfigUniqueIdAndUniqueId(archivalConfigUniqueId, archiveObjectUniqueId)

		validateInput(archiveObjectEntity, dataObjects )
		mergeDigestsIntoEntity(archiveObjectEntity, dataObjects)

		validate(archiveObjectEntity)
		archiveObjectDao.save(archiveObjectEntity)
	}

	private fun mergeDigestsIntoEntity(dbAO: ArchiveObjectEntity, inDOs: List<DataObjectEntity>) {
		for (inDO in inDOs) {
			val dbDO = dbAO.dataObjects.first { it.uniqueId == inDO.uniqueId }
			dbDO.digests.addAll(inDO.digests.map {
				DataObjectDigestEntity(
						digestMethod = digestMethodDao.findByOid(it.digestMethod.oid)
								?: throw XmlErsBusinessException("DigestMethod with oid ${it.digestMethod.oid} not found"),
						digestValue = it.digestValue,
						dataObject = dbDO
				)
			})
		}
	}

	private fun validateInput(dbAO: ArchiveObjectEntity, inDOs: List<DataObjectEntity>) {
		assertAllDigestMethodsKnown( dbAO,inDOs)
		assertNoConflictingDigestsGiven(dbAO,inDOs)
	}

	private fun assertAllDigestMethodsKnown(dbAO: ArchiveObjectEntity, inDOs: List<DataObjectEntity>) {
		inDOs.forEach { inDO ->
			dbAO.dataObjects.firstOrNull { it.uniqueId == inDO.uniqueId }
					?: throw XmlErsBusinessException("DataObject with unique id ${inDO.uniqueId} not found in db.")
		}
	}

	private fun assertNoConflictingDigestsGiven(dbAO: ArchiveObjectEntity, inDOs: List<DataObjectEntity>) {
		inDOs.forEach { inDO ->
			val dbDO = dbAO.dataObjects.first { it.uniqueId == inDO.uniqueId }
			inDO.digests.forEach { inDigest ->
				if (null != dbDO.digests.firstOrNull { it.digestMethod.oid == inDigest.digestMethod.oid }) {
					throw XmlErsBusinessException("Digest with oid ${inDigest.digestMethod.oid} already exists for DataObject ${inDO.uniqueId}")
				}
			}
		}
	}

}