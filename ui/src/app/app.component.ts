import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <nav class="navbar">
      <span class="open-slide">
        <a href="#" (click)="openSideMenu()">
          <svg width="30" height="30">
            <path d="M0,5 30,5" stroke="#000" stroke-width="5"/>
            <path d="M0,14 30,14" stroke="#000" stroke-width="5"/>
            <path d="M0,23 30,23" stroke="#000" stroke-width="5"/>
          </svg>
        </a>
      </span>
      <ul class="navbar-nav">
        <li><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Service</a></li>
        <li><a href="#">Contact</a></li>
      </ul>

      <div #sideMenu class="side-nav">
        <a href="#" class="btn-close" (click)="closeSideMenu()">&times;</a>
        <a href="#" class="btn-close">Home</a>
        <a href="#" class="btn-close">About</a>
        <a href="#" class="btn-close">Services</a>
        <a href="#" class="btn-close">Contact</a>
      </div>

      <div id="main">
        <h1>Responsive Side Menu</h1>
      </div>

    </nav>
  `
})
export class AppComponent {
  constructor() {
    console.log('test');
  }

  @ViewChild('sideMenu', { static: false }) sideMenu: ElementRef;

  openSideMenu() {
    this.sideMenu
  }

  closeSideMenu() {
    console.log('close');
  }
}
