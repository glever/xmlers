/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.it.rest

import be.glever.xmlers.rest.api.generated.model.DigestMethod
import be.glever.xmlers.rest.client.XmlersRestClient
import be.glever.xmlers.rest.client.exception.XmlErsRestClientException
import org.junit.Assert
import org.junit.Test
import java.util.*
import javax.ws.rs.core.Response

class DigestMethodIT : AbstractRestIntegrationIT() {

	companion object {

		fun defaultDigestMethod(): DigestMethod = DigestMethod().apply {
			be.glever.xmlers.domain.model.DigestMethod.SHA_256.let {
				this.javaName = it.javaName
				this.uri = it.uri
				this.oid = it.oid
			}
		}

		fun randomDigestMethod(): DigestMethod = DigestMethod().apply {
			this.javaName = UUID.randomUUID().toString()
			this.uri = UUID.randomUUID().toString()
			this.oid = UUID.randomUUID().toString()
		}

		fun createDefaultDigestMethodIfNeeded(restClient: XmlersRestClient): DigestMethod =
				try {
					restClient.digestMethodRestClient.getByOid(defaultDigestMethod().oid)
				} catch (e: XmlErsRestClientException) {
					restClient.digestMethodRestClient.create(defaultDigestMethod())
				}

	}

	fun restClient() = xmlErsRestClient().digestMethodRestClient

	@Test
	fun findAll() {
		val initialSize = restClient().find().size
		val defaultDigestMethod = restClient().create(randomDigestMethod())
		val randomDigestMethod = restClient().create(randomDigestMethod())
		val result = restClient().find()
		Assert.assertEquals(initialSize + 2, result.size)
		Assert.assertEquals(1, result.filter { digestMethod -> digestMethod.javaName == defaultDigestMethod.javaName }.size)
		Assert.assertEquals(1, result.filter { digestMethod -> digestMethod.javaName == randomDigestMethod.javaName }.size)
	}

	@Test
	fun putDigestMethod() {
		val digestMethodToCreate = randomDigestMethod()
		restClient().create(digestMethodToCreate)
		try {
			restClient().create(digestMethodToCreate)
		} catch (exception: XmlErsRestClientException) {
			Assert.assertEquals(Response.Status.CONFLICT.statusCode, exception.statusCode)
        }
	}

	@Test
	fun deleteDigestMethod() {
		val initialSize = restClient().find().size
		val created = restClient().create(randomDigestMethod())

		val found = restClient().find()
		Assert.assertEquals(initialSize + 1, found.size)

		val toDelete = found.filter { it.javaName == created.javaName }.first()
		restClient().deleteDigestMethod(toDelete)
		Assert.assertEquals(initialSize, restClient().find().size)

		try {
			restClient().deleteDigestMethod(toDelete)
		} catch (e: XmlErsRestClientException) {
			Assert.assertEquals(Response.Status.NOT_FOUND.statusCode, e.statusCode)
		}
	}


	@Test
	fun getById() {
		val digestMethod = restClient().create(randomDigestMethod())
		val foundDigestMethod = restClient().getByOid(digestMethod.oid)

		Assert.assertEquals(digestMethod.uri, foundDigestMethod.uri)
		Assert.assertEquals(digestMethod.javaName, foundDigestMethod.javaName)
		Assert.assertEquals(digestMethod.oid, foundDigestMethod.oid)
	}

}
