/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain.util

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

object MessageDigestUtil {

    /**
     * Sorts and generates digest over array of hashes.
     *
     * @param hashAlgo
     * @param hashes
     * Array of byteArrays. Will not be modified.
     * @return
     * @throws NoSuchAlgorithmException
     */
    @Throws(NoSuchAlgorithmException::class)
    fun generateDigest(hashAlgo: String, hashes: List<ByteArray>): ByteArray {
        if (hashes.size == 0) throw IllegalArgumentException("Hashes cannot be empty")
        val md = MessageDigest.getInstance(hashAlgo)
        hashes.forEach { hash -> md.update(hash) }
        return md.digest()
    }

    fun toDigestBase64(algo: String, byteArray: ByteArray): String =
            Base64.getEncoder().encodeToString(MessageDigest.getInstance(algo).digest(byteArray))


    fun toDigestBase64(algo: String, string: String): String = toDigestBase64(algo, string.toByteArray())


}
