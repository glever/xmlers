/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.xml.generation.jaxb

import ietf.params.xml.ns.ers.ArchiveTimeStampSequenceType.ArchiveTimeStampChain
import ietf.params.xml.ns.ers.ArchiveTimeStampType
import ietf.params.xml.ns.ers.CanonicalizationMethodType
import ietf.params.xml.ns.ers.DigestMethodType
import ietf.params.xml.ns.ers.ObjectFactory

class ArchiveTimestampChainBuilder {
	private val archiveTimeStampChain: ArchiveTimeStampChain

	init {
		this.archiveTimeStampChain = ObjectFactory().createArchiveTimeStampSequenceTypeArchiveTimeStampChain()
	}

	fun withCanonicalizationMethod(
			canonicalizationMethodType: CanonicalizationMethodType): ArchiveTimestampChainBuilder {
		this.archiveTimeStampChain.canonicalizationMethod = canonicalizationMethodType
		return this
	}

	fun withDigestMethod(digestMethodType: DigestMethodType): ArchiveTimestampChainBuilder {
		this.archiveTimeStampChain.digestMethod = digestMethodType
		return this
	}

	fun withOrder(order: Int): ArchiveTimestampChainBuilder {
		this.archiveTimeStampChain.order = order
		return this
	}

	fun withArchiveTimestampType(archiveTimestampType: ArchiveTimeStampType): ArchiveTimestampChainBuilder {
		this.archiveTimeStampChain.archiveTimeStamp.add(archiveTimestampType)
		return this
	}

	fun build(): ArchiveTimeStampChain {
		return this.archiveTimeStampChain
	}
}
