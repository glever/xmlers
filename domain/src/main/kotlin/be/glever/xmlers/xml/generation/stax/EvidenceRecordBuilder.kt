package be.glever.xmlers.xml.generation.stax

import be.glever.xmlers.domain.model.ArchiveTimestamp
import be.glever.xmlers.domain.model.DigestMethod
import be.glever.xmlers.domain.model.ReducedHashTree
import org.apache.xml.security.c14n.Canonicalizer
import java.io.StringWriter
import javax.xml.stream.XMLOutputFactory
import javax.xml.stream.XMLStreamWriter

class EvidenceRecordBuilder {

    companion object {
        private val xmlOutputFactory = XMLOutputFactory.newInstance()
    }

    fun fromInitialTimeStamp(reducedHashTree: ReducedHashTree?, archiveTimestamp: ArchiveTimestamp, digestMethodType: DigestMethod): String {
        val stringWriter = StringWriter(2048)
        val streamWriter = xmlOutputFactory.createXMLStreamWriter(stringWriter)
        write(streamWriter, reducedHashTree, archiveTimestamp)
        return stringWriter.toString()
    }

    fun write(writer: XMLStreamWriter, reducedHashTree: ReducedHashTree?, archiveTimestamp: ArchiveTimestamp) {
        val ns = "urn:ietf:params:xml:ns:ers"
        writer.setDefaultNamespace(ns)
        writer.writeStartElement(ns, "EvidenceRecord")
        writer.writeAttribute("xmlns", ns)

        writer.writeAttribute("Version", "1.0")
        writeEncryptionInformation(writer)
        writeSupportingInformationList(writer)
        // TODO refactor params when implementing retimestamping
        writeArchiveTimeStampSequence(writer, reducedHashTree, archiveTimestamp)

        writer.writeEndElement()
        writer.writeEndDocument()
    }
}
