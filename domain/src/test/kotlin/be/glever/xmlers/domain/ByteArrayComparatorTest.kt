/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.domain

import be.glever.xmlers.domain.util.ByteArrayComparator
import org.junit.Assert.*
import org.junit.Test

class ByteArrayComparatorTest {
    @Test
    fun sameArraysShouldYieldZero(){
        val arr = byteArrayOf(1)
        assertEquals(0, ByteArrayComparator().compare(arr, arr))
    }

    @Test
    fun equalArrayShouldYieldZero(){
        val arr1 = byteArrayOf(1)
        val arr2 = byteArrayOf(1)
        assertEquals(0, ByteArrayComparator().compare(arr1, arr2))
    }

    @Test
    fun smallerArrayShouldYieldMinusOne(){
        val arr1 = byteArrayOf(1)
        val arr2 = byteArrayOf(2)
        assertEquals(-1, ByteArrayComparator().compare(arr1, arr2))
    }

    @Test
    fun biggerArrayShouldYieldOne() {
        val arr1 = byteArrayOf(1)
        val arr2 = byteArrayOf(0)
        assertEquals(1, ByteArrayComparator().compare(arr1, arr2).toLong())
    }

    @Test
    fun shorterButSameStartShouldYieldMinusOne() {
        val arr1 = byteArrayOf(1)
        val arr2 = byteArrayOf(1, 1)
        assertEquals(-1, ByteArrayComparator().compare(arr1, arr2).toLong())
    }

    @Test
    fun longerButSameStartShouldYieldOne() {
        val arr1 = byteArrayOf(1, 1)
        val arr2 = byteArrayOf(1)
        assertEquals(1, ByteArrayComparator().compare(arr1, arr2).toLong())
    }

}
