#!/usr/bin/env bash

cd "$(dirname "$0")"

SWAGGER_PATH="../../rest-service/api/swagger/swagger.yaml"
OUTPUT_DIR="../src/app/services/generated/"

if [ ! -f $SWAGGER_PATH ]; then
  echo "${SWAGGER_PATH} not found, terminating script."
  exit 0
fi

# download swagger codegen
CODEGEN_VERSION=2.4.5
CODEGEN_PREFIX=swagger-codegen-cli-
CODEGEN_JAR=${CODEGEN_PREFIX}${CODEGEN_VERSION}.jar
if [ ! -f ${CODEGEN_JAR} ]; then
  rm ${CODEGEN_PREFIX}*.jar
  mvn dependency:get -Dartifact=io.swagger:swagger-codegen-cli:$CODEGEN_VERSION -Ddest=${CODEGEN_JAR}
fi

# run codegen command
if [ -d ${OUTPUT_DIR} ]; then
  rm -rf ${OUTPUT_DIR}
fi
java -jar ${CODEGEN_JAR} generate -i ../../rest-service/api/swagger/swagger.yaml -o ${OUTPUT_DIR} -l typescript-angular -c config.json
