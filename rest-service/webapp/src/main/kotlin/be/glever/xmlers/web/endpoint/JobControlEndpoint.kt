/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.web.endpoint

import be.glever.xmlers.rest.api.generated.api.JobControlApi
import be.glever.xmlers.service.JobControlService
import org.springframework.beans.factory.annotation.Autowired
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext

class JobControlEndpoint: JobControlApi {
	@Autowired lateinit var jobControlService: JobControlService

	override fun launchArchivalJob(archivalConfigUniqueId: String, securityContext: SecurityContext?): Response {
		jobControlService.runJob(archivalConfigUniqueId)
		return Response.status(Response.Status.NO_CONTENT).build()
	}
}