package be.glever.xmlers.rest.client

import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.Response

class JobControlRestClient(val target: WebTarget) {

	fun launchArchivalJob(archivalConfigUniqueId: String) =
			target.path("run")
					.path(archivalConfigUniqueId)
					.request()
					.post(null)
					.assertStatus(Response.Status.NO_CONTENT)

}