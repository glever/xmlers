package be.glever.xmlers.xml.validation

data class XMLERSValidationResult(
        val chainsToRootHash: Boolean = false
) {
}
