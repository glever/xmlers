/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.business.jpa.entity

import java.time.OffsetDateTime
import java.time.ZoneOffset
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class PeriodEmbeddableEntity(
	@Column(name = "VALID_FROM")
	val validFrom: OffsetDateTime = DEFAULT_VALID_FROM_DATE,

	@Column(name = "VALID_UNTIL")
	val validUntil: OffsetDateTime = DEFAULT_VALID_UNTIL_DATE
) : XmlErsEntity {

	companion object {

		val DEFAULT_VALID_FROM_DATE: OffsetDateTime = OffsetDateTime.of(1900, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC)
		val DEFAULT_VALID_UNTIL_DATE: OffsetDateTime = OffsetDateTime.of(2500, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC)
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false

		other as PeriodEmbeddableEntity

		if (validFrom != other.validFrom) return false
		if (validUntil != other.validUntil) return false

		return true
	}

	override fun hashCode(): Int {
		var result = validFrom.hashCode()
		result = 31 * result + validUntil.hashCode()
		return result
	}


}