/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.service.impl

import be.glever.xmlers.business.jpa.dao.DigestMethodDao
import be.glever.xmlers.business.jpa.dao.HashTreeDao
import be.glever.xmlers.business.jpa.entity.HashTreeEntity
import be.glever.xmlers.service.HashTreeService
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Transactional
@Repository
class HashTreeServiceImpl(private val hashTreeDao: HashTreeDao, private val digestMethodDao: DigestMethodDao) : HashTreeService {

	override fun save(hashTree: HashTreeEntity): HashTreeEntity =
			hashTreeDao.save(hashTree)


}