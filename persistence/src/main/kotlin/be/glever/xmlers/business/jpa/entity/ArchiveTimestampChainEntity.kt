/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */


package be.glever.xmlers.business.jpa.entity

import javax.persistence.*

@Entity
@Table(name = "ARCHIVE_TSTP_CHAIN",
		uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("HASH_TREE_ID", "SORT_ORDER"))))
data class ArchiveTimestampChainEntity(
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	val id: Long? = null,

	@OneToOne
	@JoinColumn(name = "HASH_TREE_ID")
	val hashTree: HashTreeEntity,

	@ManyToOne
	@JoinColumn(name = "HASH_ALGO_ID")
	val digestMethod: DigestMethodEntity,

	@Column(name = "C14N_METHOD")
	val c14nMethod: String,

	@Column(name = "SORT_ORDER")
	val order: Int,

	@OneToMany
	val timeStamps: List<TimeStampEntity>) : XmlErsEntity