/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.web.endpoint

import be.glever.xmlers.rest.api.generated.api.ArchiveObjectApi
import be.glever.xmlers.rest.api.generated.model.ArchiveObject
import be.glever.xmlers.rest.api.generated.model.DataObjectList
import be.glever.xmlers.service.ArchivalConfigService
import be.glever.xmlers.service.ArchiveObjectService
import be.glever.xmlers.service.C14nMethodService
import be.glever.xmlers.service.DigestMethodService
import be.glever.xmlers.service.exception.XmlErsBusinessException
import be.glever.xmlers.web.mapper.ArchivalConfigMapper
import be.glever.xmlers.web.mapper.ArchiveObjectMapper
import be.glever.xmlers.web.mapper.DataObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response
import javax.ws.rs.core.SecurityContext

class ArchiveObjectEndPoint : ArchiveObjectApi {


	@Autowired
	private lateinit var archivalConfigService: ArchivalConfigService
	@Autowired
	private lateinit var digestMethodService: DigestMethodService
	@Autowired
	private lateinit var c14nMethodService: C14nMethodService
	@Autowired
	private lateinit var archiveObjectService: ArchiveObjectService

	private fun mapper(): ArchivalConfigMapper = ArchivalConfigMapper(digestMethodService, c14nMethodService)
	private fun archiveObjectMapper(): ArchiveObjectMapper = ArchiveObjectMapper(digestMethodService)
	private fun dataObjectMapper(): DataObjectMapper = DataObjectMapper(digestMethodService)

	override fun addArchiveObject(archivalConfigUniqueId: String, archiveObject: ArchiveObject, securityContext: SecurityContext): Response {
		validateNewArchiveObject(archiveObject)

		return archiveObjectService.insert(archiveObjectMapper().map(archiveObject, archivalConfigService.findByUniqueId(archivalConfigUniqueId)))
				.let { Response.status(Response.Status.CREATED).build() }
	}


	private fun validateNewArchiveObject(archiveObject: ArchiveObject) {
		archiveObject.dataObjects.forEach { dataObject ->
			if (!archiveObject.dataObjects.none { dataObject != it && dataObject.uniqueId == it.uniqueId }) {
				throw XmlErsBusinessException("Multiple DataObjects with uniqueId [${dataObject.uniqueId}}] present")
			}

			dataObject.digests.forEach { digest ->
				if (!dataObject.digests.none { digest != it && digest.digestMethodOid.equals(it.digestMethodOid) }) {
					throw XmlErsBusinessException("DataObject with uniqueId [${dataObject.uniqueId}] contains multiple digests for digestMethodOID [${digest.digestMethodOid}]")
				}
			}
		}

	}


	override fun getArchiveObject(archivalConfigUniqueId: String, archiveObjectUniqueId: String, securityContext: SecurityContext): Response =
			archiveObjectService.findByArchivalConfigUniqueIdAndArchiveObjectUniqueId(archivalConfigUniqueId, archiveObjectUniqueId)
					.let {
						Response.status(Response.Status.OK)
								.entity(archiveObjectMapper().map(it))
								.build()
					}
					?: throw WebApplicationException(Response.status(Response.Status.NOT_FOUND).entity("No ArchiveObject found for archivalConfigUniqueId $archivalConfigUniqueId and archiveObjectUniqueId $archiveObjectUniqueId").build())


	override fun getEvidenceRecord(archivalConfigId: String?, archiveObjectId: String?, securityContext: SecurityContext?): Response {
		TODO("not implemented")
	}

	override fun updateDataObjects(archivalConfigUniqueId: String, archiveObjectUniqueId: String, dataObjects: DataObjectList, securityContext: SecurityContext): Response {
		val mappedDataObjects = dataObjects.dataObjects.map { dataObject -> DataObjectMapper(digestMethodService).map(dataObject) }
		archiveObjectService.updateArchiveObjectDigests(archivalConfigUniqueId, archiveObjectUniqueId, mappedDataObjects)
		return Response.status(Response.Status.NO_CONTENT).build()
	}
}



