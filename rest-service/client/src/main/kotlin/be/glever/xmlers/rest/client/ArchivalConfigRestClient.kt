/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2017.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */

package be.glever.xmlers.rest.client

import be.glever.xmlers.rest.api.generated.model.ArchivalConfig
import be.glever.xmlers.rest.api.generated.model.ArchiveObject
import be.glever.xmlers.rest.api.generated.model.DataObject
import java.math.BigDecimal
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

class ArchivalConfigRestClient(private val target: WebTarget) {

	private fun archivalConfigPath(archivalConfigUniqueId: String): WebTarget = target.path(archivalConfigUniqueId)
	private fun archiveObjectsPath(archivalConfigUniqueId: String): WebTarget = archivalConfigPath(archivalConfigUniqueId).path("archiveObject")
	private fun archiveObjectPath(archivalConfigUniqueId: String, archiveObjectUniqueId: String): WebTarget = archiveObjectsPath(archivalConfigUniqueId).path(archiveObjectUniqueId)


	fun createArchivalConfig(archivalConfig: ArchivalConfig): ArchivalConfig =
			target.request(MediaType.APPLICATION_JSON_TYPE)
					.put(Entity.entity(archivalConfig, MediaType.APPLICATION_JSON_TYPE))
					.readResponse(expectedType = object : GenericType<ArchivalConfig>() {}, expectedStatus = Response.Status.CREATED)


	fun getByUniqueId(archivalConfigUniqueId: String): ArchivalConfig =
			archivalConfigPath(archivalConfigUniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.get()
					.readResponse(expectedStatus = Response.Status.OK, expectedType = object : GenericType<ArchivalConfig>() {})

	fun updateDataObjects(archivalConfig: ArchivalConfig) =
			archivalConfigPath(archivalConfig.uniqueId)
					.request(MediaType.APPLICATION_JSON_TYPE)
					.post(Entity.entity(archivalConfig, MediaType.APPLICATION_JSON_TYPE))
					.assertStatus(expectedStatus = Response.Status.OK)

	fun deleteArchivalConfig(archivalConfigUniqueId: String) =
			archivalConfigPath(archivalConfigUniqueId)
					.request()
					.delete()
					.assertStatus(Response.Status.NO_CONTENT)


	fun findAll(): List<ArchivalConfig> =
			target.request(MediaType.APPLICATION_JSON_TYPE).get()
					.readResponse(expectedStatus = Response.Status.OK, expectedType = object : GenericType<List<ArchivalConfig>>() {})

}