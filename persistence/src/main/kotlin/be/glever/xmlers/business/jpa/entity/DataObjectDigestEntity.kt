/*
 * Copyright Glen Vermeylen (glen.vermeylen@gmail.com), 2018.
 *
 * This file is part of XMLERS.
 *
 * XMLERS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * XMLERS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with XMLERS.  If not, see <http://www.gnu.org/licenses/>.
 */
package be.glever.xmlers.business.jpa.entity

import java.util.*
import javax.persistence.*

@Table(name = "DATA_OBJECT_DIGEST")
@Entity
class DataObjectDigestEntity(

	id: Long? = null,

	@ManyToOne
	@JoinColumn(name = "DATA_OBJECT_ID", nullable = false)
	var dataObject: DataObjectEntity?,

	@ManyToOne
	@JoinColumn(name = "DIGEST_METHOD_ID", nullable = false)
	val digestMethod: DigestMethodEntity,

	@Column(name = "DIGEST_VALUE", nullable = false)
	val digestValue: ByteArray

) : AbstractXmlErsEntity(id)